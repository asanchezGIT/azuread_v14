package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ProducerOrganizationContainer")
@Node(jcrType = "santanderbrxm:ProducerOrganizationContainer")
public class ProducerOrganizationContainer extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:idIL")
    public String getIdIL() {
        return getSingleProperty("santanderbrxm:idIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:idCMS")
    public String getIdCMS() {
        return getSingleProperty("santanderbrxm:idCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleCMS")
    public String getTitleCMS() {
        return getSingleProperty("santanderbrxm:titleCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:deprecated")
    public String getDeprecated() {
        return getSingleProperty("santanderbrxm:deprecated");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleIL")
    public String getTitleIL() {
        return getSingleProperty("santanderbrxm:titleIL");
    }
}
