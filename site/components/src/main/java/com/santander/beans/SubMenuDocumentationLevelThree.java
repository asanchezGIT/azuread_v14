package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

import java.util.List;

@HippoEssentialsGenerated(internalName = "santanderbrxm:SubMenuDocumentationLevelThree")
@Node(jcrType = "santanderbrxm:SubMenuDocumentationLevelThree")
public class SubMenuDocumentationLevelThree extends BaseDocument {
	
	
	@HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }
	@HippoEssentialsGenerated(internalName = "santanderbrxm:description")
	    public HippoHtml getDescription() {
	        return getHippoHtml("santanderbrxm:description");
	}
	
	  @HippoEssentialsGenerated(internalName = "santanderbrxm:documents")
	    public List<HippoBean> getDocuments() {
	          return getLinkedBeans("santanderbrxm:documents", HippoBean.class);
	    }
	
    
    
    
}
