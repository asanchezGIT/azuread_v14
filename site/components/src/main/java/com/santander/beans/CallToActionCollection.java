package com.santander.beans;

import java.util.List;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:CallToActionCollection")
@Node(jcrType = "santanderbrxm:CallToActionCollection")
public class CallToActionCollection extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:lookandfeel")
    public String getLookandfeel() {
        return getSingleProperty("santanderbrxm:lookandfeel");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:CallToActionItem")
    public List<CallToActionItem> getCallToActionItem() {
        return getChildBeansByName("santanderbrxm:CallToActionItem", CallToActionItem.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:callToActionButton")
    public CallToActionButton getCallToActionButton() {
        return getBean("santanderbrxm:callToActionButton", CallToActionButton.class);
    }
}
