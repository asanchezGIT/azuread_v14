package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.List;
import com.santander.beans.CardStatusItem;

@HippoEssentialsGenerated(internalName = "santanderbrxm:CardStatusCollection")
@Node(jcrType = "santanderbrxm:CardStatusCollection")
public class CardStatusCollection extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:CardStatusItem")
    public List<CardStatusItem> getCardStatusItem() {
        return getChildBeansByName("santanderbrxm:CardStatusItem",
                CardStatusItem.class);
    }
}
