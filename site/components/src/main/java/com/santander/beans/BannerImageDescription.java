package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

@HippoEssentialsGenerated(internalName = "santanderbrxm:BannerImageDescription")
@Node(jcrType = "santanderbrxm:BannerImageDescription")
public class BannerImageDescription extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:lookandfeel")
    public String getLookandfeel() {
        return getSingleProperty("santanderbrxm:lookandfeel");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:imageBackground")
    public HippoGalleryImageSet getImageBackground() {
        return getLinkedBean("santanderbrxm:imageBackground",
                HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:callToActionButton")
    public CallToActionButton getCallToActionButton() {
        return getBean("santanderbrxm:callToActionButton", CallToActionButton.class);
    }
}
