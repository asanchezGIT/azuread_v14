package com.santander.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:bannerdocument")
@Node(jcrType = "santanderbrxm:bannerdocument")
public class BannerDocument extends BaseDocument {
	
	  @HippoEssentialsGenerated(internalName = "santanderbrxm:logo")
	    public HippoGalleryImageSet getLogo() {
	        return getLinkedBean("santanderbrxm:logo", HippoGalleryImageSet.class);
	    }
	
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:subtitle")
    public HippoHtml getSubtitle() {
        return getHippoHtml("santanderbrxm:subtitle");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }
  
    @HippoEssentialsGenerated(internalName = "santanderbrxm:imageBackground")
    public HippoGalleryImageSet getImageBackground() {
        return getLinkedBean("santanderbrxm:imageBackground",
                HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:scale")
    public String getScale() {
        return getSingleProperty("santanderbrxm:scale");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:callToActionButton")
    public CallToActionButton getCallToActionButton() {
        return getBean("santanderbrxm:callToActionButton",
                CallToActionButton.class);
    }
}
