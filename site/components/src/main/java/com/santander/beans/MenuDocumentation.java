package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.List;


@HippoEssentialsGenerated(internalName = "santanderbrxm:MenuDocumentation")
@Node(jcrType = "santanderbrxm:MenuDocumentation")
public class MenuDocumentation extends BaseDocument {
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentsGroups")
    public List<SubMenuDocumentationLevelOne> getDocumentsGroups() {
        return getChildBeansByName("santanderbrxm:documentsGroups",
        		SubMenuDocumentationLevelOne.class);
    }
}
