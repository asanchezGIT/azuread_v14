package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

@HippoEssentialsGenerated(internalName = "santanderbrxm:Security")
@Node(jcrType = "santanderbrxm:Security")
public class Security extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:html")
    public HippoHtml getHtml() {
        return getHippoHtml("santanderbrxm:html");
    }
}
