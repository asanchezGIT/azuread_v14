package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.onehippo.forge.beans.RelatedDocsBean;
import java.util.List;
import java.util.Calendar;

@HippoEssentialsGenerated(internalName = "santanderbrxm:NewsAndArticles")
@Node(jcrType = "santanderbrxm:NewsAndArticles")
public class NewsAndArticles extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:author")
    public String getAuthor() {
        return getSingleProperty("santanderbrxm:author");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:profile")
    public String getProfile() {
        return getSingleProperty("santanderbrxm:profile");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:image")
    public HippoGalleryImageSet getImage() {
        return getLinkedBean("santanderbrxm:image", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "hippostd:tags")
    public String[] getTags() {
        return getMultipleProperty("hippostd:tags");
    }

    @HippoEssentialsGenerated(internalName = "relateddocs:docs")
    public List<HippoBean> getRelatedDocuments() {
        final RelatedDocsBean documents = getBean("relateddocs:docs");
        return documents.getDocs();
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:date")
    public Calendar getDate() {
        return getSingleProperty("santanderbrxm:date");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:type")
    public String getType() {
        return getSingleProperty("santanderbrxm:type");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:iconAuthor")
    public HippoGalleryImageSet getIconAuthor() {
        return getLinkedBean("santanderbrxm:iconAuthor",
                HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:shortDescription")
    public HippoHtml getShortDescription() {
        return getHippoHtml("santanderbrxm:shortDescription");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:littleIcon")
    public HippoGalleryImageSet getLittleIcon() {
        return getLinkedBean("santanderbrxm:littleIcon",
                HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:video")
    public HippoBean getVideo() {
        return getLinkedBean("santanderbrxm:video", HippoBean.class);
    }
    @HippoEssentialsGenerated(internalName = "santanderbrxm:alias")
    public String getAlias() {
        return getSingleProperty("santanderbrxm:alias");
    }
}
