package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;

import java.util.List;
import com.santander.beans.CardItem;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ProductList")
@Node(jcrType = "santanderbrxm:ProductList")
public class ProductList extends BaseDocument {
   
    @HippoEssentialsGenerated(internalName = "santanderbrxm:productos")
    public List<HippoBean> getProductos() {
        return getLinkedBeans("santanderbrxm:productos", HippoBean.class);
    }
}
