package com.santander.beans;

import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.List;
import com.santander.beans.SectionLevelOne;

@HippoEssentialsGenerated(internalName = "santanderbrxm:documentation")
@Node(jcrType = "santanderbrxm:documentation")
public class Documentation extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:page")
    public List<SectionLevelOne> getPage() {
        return getChildBeansByName("santanderbrxm:page", SectionLevelOne.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:links")
    public List<LinksAndDownloads> getLinks() {
        return getChildBeansByName("santanderbrxm:links", LinksAndDownloads.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:type")
    public String getType() {
        return getSingleProperty("santanderbrxm:type");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alias")
    public String getAlias() {
        return getSingleProperty("santanderbrxm:alias");
    }
}
