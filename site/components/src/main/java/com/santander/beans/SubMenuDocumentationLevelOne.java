package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

import java.util.List;

@HippoEssentialsGenerated(internalName = "SubMenuDocumentationLevelOne")
@Node(jcrType = "santanderbrxm:SubMenuDocumentationLevelOne")
public class SubMenuDocumentationLevelOne extends BaseDocument {
	
	
	@HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }
	@HippoEssentialsGenerated(internalName = "santanderbrxm:description")
	    public HippoHtml getDescription() {
	        return getHippoHtml("santanderbrxm:description");
	}
	
	  @HippoEssentialsGenerated(internalName = "santanderbrxm:documents")
	    public List<HippoBean> getDocuments() {
	          return getLinkedBeans("santanderbrxm:documents", HippoBean.class);
	    }
	
 @HippoEssentialsGenerated(internalName = "santanderbrxm:documentsGroups")
	    public List<SubMenuDocumentationLevelTwo> getDocumentsGroups() {
	        return getChildBeansByName("santanderbrxm:documentsGroups",
	        		SubMenuDocumentationLevelTwo.class);
	    }
    
 
    
    
    
}
