package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import java.util.List;
import com.santander.beans.SectionLevelFour;

@HippoEssentialsGenerated(internalName = "santanderbrxm:sectionLevelThree")
@Node(jcrType = "santanderbrxm:sectionLevelThree")
public class SectionLevelThree extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:text")
    public String getText() {
        return getSingleProperty("santanderbrxm:text");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:html")
    public HippoHtml getHtml() {
        return getHippoHtml("santanderbrxm:html");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:sections")
    public List<SectionLevelFour> getSections() {
        return getChildBeansByName("santanderbrxm:sections",
                SectionLevelFour.class);
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentation")
    public List<HippoBean> getDocumentation() {
        return getLinkedBeans("santanderbrxm:documentation", HippoBean.class);
    }
    
}
