package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.List;


@HippoEssentialsGenerated(internalName = "santanderbrxm:SubMenuDocumentation")
@Node(jcrType = "santanderbrxm:SubMenuDocumentation")
public class SubMenuDocumentation extends BaseDocument {
	
	@HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentationsGroups")
    public List<DocumentsGroup> getDocumentationsGroups() {
        return getChildBeansByName("santanderbrxm:documentationsGroups",
        		DocumentsGroup.class);
    }
}
