package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

import java.util.List;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ListDescription")
@Node(jcrType = "santanderbrxm:ListDescription")
public class ListDescription extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }
    
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleList1")
    public String getTitleList1() {
        return getSingleProperty("santanderbrxm:titleList1");
    }
   
    @HippoEssentialsGenerated(internalName = "santanderbrxm:elementsList1")
    public List<HippoBean> getElementsList1() {
        return getLinkedBeans("santanderbrxm:elementsList1", HippoBean.class);
    }
    
    
       
    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleList2")
    public String getTitleList2() {
        return getSingleProperty("santanderbrxm:titleList2");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:elementsList2")
    public List<HippoBean> getElementsList2() {
        return getLinkedBeans("santanderbrxm:elementsList2", HippoBean.class);
    }
   
    
   
}
