package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;

import java.util.List;
import com.santander.beans.CardItem;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ProductListCompound")
@Node(jcrType = "santanderbrxm:ProductListCompound")
public class ProductListCompound extends BaseDocument {
	
	
	 @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
	    public String getTitle() {
	        return getSingleProperty("santanderbrxm:title");
	    }
   
	 @HippoEssentialsGenerated(internalName = "santanderbrxm:products")
	    public List<HippoBean> getProducts() {
	        return getLinkedBeans("santanderbrxm:products", HippoBean.class);
	    }
}
