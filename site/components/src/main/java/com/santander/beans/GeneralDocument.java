/*
 * Copyright 2014-2019 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.santander.beans;
import java.util.Calendar;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:GeneralDocument")
@Node(jcrType = "santanderbrxm:GeneralDocument")
public class GeneralDocument extends BaseDocument {
	
	 @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
	    public String getTitle() {
	        return getSingleProperty("santanderbrxm:title");
	    }
	
    @HippoEssentialsGenerated(internalName = "santanderbrxm:introduction")
    public HippoHtml getIntroduction() {
    	return getHippoHtml("santanderbrxm:introduction");
    }

   

    @HippoEssentialsGenerated(internalName = "santanderbrxm:content")
    public HippoHtml getContent() {
        return getHippoHtml("santanderbrxm:content");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:lastUpdated")
    public Calendar getPublicationDate() {
        return getSingleProperty("santanderbrxm:lastUpdated");
    }
}
