package com.santander.beans;

import java.util.List;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:IntegrationProduct")
@Node(jcrType = "santanderbrxm:IntegrationProduct")
public class IntegrationProduct extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }
 
    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:items")
    public List<CallToActionItemSimple> getItems() {
        return getChildBeansByName("santanderbrxm:items",
        		CallToActionItemSimple.class);
    }
   
}
