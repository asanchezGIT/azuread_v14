package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;

@HippoEssentialsGenerated(internalName = "santanderbrxm:productsApi")
@Node(jcrType = "santanderbrxm:productsApi")
public class ProductsApi extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:id")
    public String getId() {
        return getSingleProperty("santanderbrxm:id");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:exposure")
    public String getExposure() {
        return getSingleProperty("santanderbrxm:exposure");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:type")
    public String getType() {
        return getSingleProperty("santanderbrxm:type");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:mode")
    public String getMode() {
        return getSingleProperty("santanderbrxm:mode");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:deprecated")
    public String getDeprecated() {
        return getSingleProperty("santanderbrxm:deprecated");
        
        
    }
    
}
