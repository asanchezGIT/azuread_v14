package com.santander.utils;

import static com.santander.utils.Constants.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

import com.santander.beans.ApiItem;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.content.beans.standard.HippoDocumentBean;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.util.PathUtils;
import org.hippoecm.repository.HippoStdNodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.santander.beans.Documentation;
import com.santander.beans.GettingStartingPage;
import com.santander.beans.MenuDocumentation;
import com.santander.beans.ProductContainerItem;
import com.santander.beans.SubMenuDocumentationLevelOne;
import com.santander.beans.SubMenuDocumentationLevelThree;
import com.santander.beans.SubMenuDocumentationLevelTwo;
import com.santander.dto.DocumentationLinkDTO;
import com.santander.dto.DocumentationProductDTO;
import com.santander.dto.GenericDAOResource;
import com.santander.dto.MenuDocumentationDTO;
import com.santander.dto.ProductContainerExpandDTO;
import com.santander.dto.ProductContainerInfoBasicDTO;
import com.santander.dto.SubMenuDocumentionLevelOneDTO;
import com.santander.dto.SubMenuDocumentionLevelThreeDTO;
import com.santander.dto.SubMenuDocumentionLevelTwoDTO;

public class InternalLinkProcessor {

  private static final String HTTP = "http://";
private static final String HTTPS = "https://";
private static Logger logger = LoggerFactory.getLogger(InternalLinkProcessor.class);
  HstRequestContext hstRequestContext = RequestContextProvider.get();

  private final String[] PROTOCOLOS_HTTP = {HTTP, HTTPS};
  //private final String[] searchList = {HTTPS, HTTP, "/site/", "customapi/", ".html", "resourceapi/", "santanderdeveloper"};
  //private final String[] replacementList = {"", "", "", "", "", "", "developer"};
  private final String[] searchList = {HTTPS, HTTP, "site/", "customapi/", ".html", "resourceapi/"};
  private final String[] replacementList = {"", "", "", "", "", "" };


  /**
   * Converts internal CMS link to FrontEnd link.
   *
   * E.g "/site/brasil/en_GB/content/products/product1.html" to "/en/brazil/productDetail/[id]"
   *
   * @param url
   * @param documentType
   * @param uuid
   * @return
   */
  public String generateInternalLink(String url, String documentType, String uuid) throws QueryException {  String locale = "";
    String channelBRX;
    String type = "";
    String alias=uuid;
   
    // TODO To refactor into optimum way
    if (StringUtils.startsWithAny(url, PROTOCOLOS_HTTP)) {
      logger.debug("*******HTTP/HTTPS");
      url = StringUtils.substringAfter(StringUtils.replaceEach(url, searchList, replacementList),"/");
    } else {
      logger.debug("*******NO HTTP/HTTPS");
      // Manage standard URL
      url = StringUtils.replaceEach(url, searchList, replacementList);
    }

    //logger.info("*******url: {}" ,url);

    // get the locale
    locale = getLocale(url, locale);

    //get the channel of bloomreach
    channelBRX = getChannelBRXM(url);
    
    //logger.info("*****documentType:"+ documentType);

    // get the type
    if (documentType.equals(DOC_TYPE_PRODUCT)) {
      type = PRODUCT_DETAIL;
      alias = getAliasByUuid(uuid);
      
    } 
    else if (documentType.equals(DOC_TYPE_DOCUMENTATION)) {
      alias = getAliasByUuid(uuid);
      //Si es documentType=documentation y su campo type es GetStarted o Guide en la url tiene que poner getstarted o guide y en el caso de tiene que devolverse la url con el id del Producto al que pertenece
      type=  getTypeByUuid(uuid).toLowerCase();
      
      if (type.equals("getstarted")) type=GETSTARTED;
      if (type.equals("guide")) type=GUIDE;
      if (type.equalsIgnoreCase(DOCUMENTATION)){
    	  logger.debug("El documento es un documentation");
    	  logger.debug("uuid documento:"+uuid);
    	  String parentProductId = getProductAsignedDocument(uuid, hstRequestContext, locale,channelBRX);
	      if (parentProductId.isEmpty()) {
	        logger.debug("documento NO esta asociado a Producto");
	        return StringUtils.EMPTY;
	      } 
	      else {
	    	  logger.debug("documento SI está asociado a Producto");	  
	    	  return LINK_BLOOMREACH+"/"+channelBRX+"/"+locale+"/"+PRODUCT_DETAIL+"/"+parentProductId+"/"+DOCUMENTATION+"/"+alias;
	      }
      
      }
    
    } else if (documentType.equals(DOC_TYPE_GETTINGSTARTED)) {
      alias = getAliasByUuid(uuid);
      type = PRODUCT_DOCUMENTATION;
    } else if (documentType.equals(DOC_TYPE_NEWS_AND_ARTICLES)) {
      alias = getAliasByUuid(uuid);	//añadido 100521
      type = getTypeFromArticleNode(uuid);
    } else if (documentType.equals(DOC_PRODUCT_CONTAINER_ITEM)) {
      alias = getAliasByUuid(uuid);
      type = PRODUCT_DETAIL;
      return LINK_BLOOMREACH+"/"+channelBRX+"/"+locale+"/"+type+"/"+alias;
    } else if (documentType.equals(DOC_TYPE_API_CONTAINER_ITEM)) {
      alias = getAliasByUuid(uuid);
      type = API;
      return LINK_BLOOMREACH+"/"+channelBRX+"/"+locale+"/"+type+"/"+alias;
    } else if (documentType.equals(DOC_TYPE_API_ITEM)) {
	      type = API;
	      String version;
	      version = getVersion(uuid,hstRequestContext);
	      String parentProductId = HippoUtils.getApiItemRelatedDocuments(uuid, hstRequestContext);
	      alias = getAliasByUuid(parentProductId);
	      if (parentProductId.isEmpty()) {
	        logger.warn("no parent productContainer found. Using empty string.");
	        return StringUtils.EMPTY;
	      } else {
	        logger.debug("returning  api item url : /{}/{}/{}/{}/{}/{}",locale,channelBRX,type,parentProductId,uuid,version);
	      }
	      return LINK_BLOOMREACH+"/"+channelBRX+"/"+locale+"/"+type+"/"+alias+"/"+version;
    }else {
      logger.debug("type no valid, returning empty url");
      return StringUtils.EMPTY;
    }

    logger.info("returning: generateInternalLink:"+ LINK_BLOOMREACH+"/"+channelBRX+"/"+locale+"/"+type+"/"+alias);
    return LINK_BLOOMREACH+"/"+channelBRX+"/"+locale+"/"+type+"/"+alias;
    
    
  }

private String getTypeFromArticleNode(String uuid) {
	String type;
	type = URL_PATH_ARTICLE_DETAIL_OLD;
      try {
    	logger.debug("INICIO getTypeFromArticleNode. uuid:"+uuid);
        Node articleNode = hstRequestContext.getSession().getNodeByIdentifier(uuid);
        String articleType = articleNode.getProperty(PROPERTY_SANTANDERBRXM_TYPE).getString();
        logger.debug("articleType:"+articleType);
        type = getType(type, articleType);
        logger.debug("type:"+type);
      } catch (RepositoryException e) {
        logger.trace("No NewAndArticle type could be retrieve", e);
      }
	return type;
}

private String getChannelBRXM(String url) {
  String channelBRXM;
  
  if (url.startsWith("/")) {
	  url=url.substring(1);
  	
  }
  channelBRXM = StringUtils.substringBefore(url, "/");
  logger.debug("*******channelBRXM: {}" ,channelBRXM);

  return channelBRXM;
}

public static String getVersion(String uuid, HstRequestContext requestContext){

  String version = null;
  final HippoDocumentBean hippoDocumentBean = requestContext.getSiteContentBaseBean().getBeanByUUID(uuid, ApiItem.class);
  if(Objects.nonNull(hippoDocumentBean)) {
    ApiItem apiItemBean = (ApiItem) hippoDocumentBean;
    version = Objects.nonNull(apiItemBean.getVersionIL()) && !apiItemBean.getVersionIL().isEmpty()
            ? apiItemBean.getVersionIL() : apiItemBean.getVersionCMS();
  }
  return version;
}

private String getLocale(String url, String locale) {
	Pattern p = Pattern.compile("[a-z][a-z]_[A-Z][A-Z]");
    Matcher m = p.matcher(url);
    if (m.find()) {
      locale = StringUtils.substringBefore(m.group(0), "_");
      logger.debug("localeProcessed: {}" , locale);
    }
	return locale;
}

private String getType(String type, String articleType) {
	if (StringUtils.equals(PROPERTY_SANTANDERBRXM_TYPE_ARTICLE, articleType)) {
	  type = URL_PATH_ARTICLES_DETAIL;
	} else if (StringUtils.equals(PROPERTY_SANTANDERBRXM_TYPE_NEWS, articleType)) {
	  type = URL_PATH_NEWS_DETAIL;
	} else if (StringUtils.equals(PROPERTY_SANTANDERBRXM_TYPE_WEBINAR, articleType)) {
	  type = URL_PATH_WEBINARS_DETAIL;
	} else
	{
		//Do nothing
	}
	return type;
}

  /**
   * Translate an internal link to the content path.
   *
   * @param url
   * @return
   */
  public String translateInternalLinkURLToContentPath(String url) {
	    String translatedURL = StringUtils.replaceEach(url, searchList, replacementList);
	    logger.debug("translatedURL:"+translatedURL);
	    if (translatedURL.startsWith("/")) {
	    	translatedURL=translatedURL.substring(1);
	    }
	    translatedURL = processLocaleInternalLinkURL(translatedURL);
	    return PROJECT_ROOT_PATH.concat(translatedURL);
  }

  /**
   * Returns the UUID of a content based on its path.
   *
   * @param path
   * @return
   */
  public String getInternalLinkUUID(String path) {
	String uuid = "";
    try {
      Node publishedVariant = getPublishedNodeVariant(hstRequestContext.getSession().getNode(path));
      if (publishedVariant!=null)
      {
    	  logger.debug("Published Node UUID: {}" , publishedVariant.getIdentifier());
    	  uuid = publishedVariant.getIdentifier();
      }
    } catch (RepositoryException re) {
      logger.error("Exception thrown when trying to get content node:"+path);
    }
    return uuid;
  }

  /**
   * Gets the published variant of the node.
   *
   * @param node
   * @return
   */
  private Node getPublishedNodeVariant(Node node) {
    Node documentVariant = null;
    try {
      for (NodeIterator variantsIterator = node.getNodes(node.getName()); variantsIterator
              .hasNext();) {
        documentVariant = variantsIterator.nextNode();
        String state = documentVariant.getProperty(HippoStdNodeType.HIPPOSTD_STATE).getString();
        if (HippoStdNodeType.PUBLISHED.equals(state)) {
          logger.debug("Published state found for node {}" , node.getIdentifier());
          return documentVariant;
        }
      }
      if (documentVariant!=null)
      {
    	  logger.debug("Published state node for node handler {} is {}" , node.getIdentifier()
    		  , documentVariant.getIdentifier());
      }
    } catch (PathNotFoundException pnfe) {
      logger.error("Error getting published node variant. ", pnfe);
    } catch (RepositoryException re) {
      logger.error("Error getting published node variant " , re);
    }
    return documentVariant;
  }

  /**
   * Returns the document type based on its uuid.
   *
   * @param uuid
   * @return
   */
  public String getInternalLinkDocumentType(String uuid) {
    String documentType = "";
    try {
      Node contentNode = hstRequestContext.getSession().getNodeByIdentifier(uuid);
      documentType = contentNode.getPrimaryNodeType().getName();
      logger.debug("Document Type is: {}" , documentType);
    } catch (RepositoryException re) {
      logger.error("Exception thrown when trying to get content node: {}", re.getMessage());
    }
    return documentType;
  }

  /**
   * Searches for locale pattern and converts to lower case if it exists.
   *
   * @param url
   * @return
   */
  private String processLocaleInternalLinkURL(String url) {
	  
	  logger.debug("URL {}" , url);  
    Pattern p = Pattern.compile("[a-z][a-z]_[A-Z][A-Z]");
    Matcher m = p.matcher(url);
    String locale = null;
    if (m.find()) {
      logger.debug("Locale found in URL {}" , url);
      locale = m.group(0);
    }
    
    if (locale==null)
    {	
     	return "";
    }
    
    logger.debug("locale {}" , locale);
    return url.replace(locale, locale.toLowerCase());
  }

  
  public static String getAliasByUuid(String uuid)
  {
	  logger.debug("getting alias for uuid {}",uuid);
	  HstRequestContext requestContext = RequestContextProvider.get();
      Node mountContentNode = null;
      HstQueryManager hstQueryManager = null;
      GenericDAOResource genericDAOResource = new GenericDAOResource();

      try {
          hstQueryManager = requestContext.getQueryManager();
          String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
          mountContentNode = requestContext.getSession().getRootNode()
                  .getNode(PathUtils.normalizePath(mountContentPath));
      } catch (PathNotFoundException e) {
          logger.error(e.getMessage(),e);
          
      } catch (LoginException e) {
          logger.error(e.getMessage(),e);
      } catch (RepositoryException e) {
          logger.error(e.getMessage(),e);
      }
      HstQueryResult result = null;

      try {
          HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode);
          if (Objects.nonNull(uuid) && !uuid.isEmpty()) {
            Filter filter = hstQuery.createFilter();
          	filter.addEqualTo("jcr:uuid", uuid);
            hstQuery.setFilter(filter);
          }
          result = hstQuery.execute();
      } catch (QueryException e) {
          logger.error(e.getMessage(),e);
      }
	  
      if (result!=null && result.getSize()>0)
      {
    	  HippoBean document=result.getHippoBeans().nextHippoBean();
    	  String alias =null;
    	  try
    	  {
    		  alias =document.getNode().getProperty("santanderbrxm:alias").getString();
    	  } catch (RepositoryException e)
    	  {
    		  logger.debug("no alias retrieved ");
    		  return uuid;
    	  }
    	  if (alias !=null && !alias.isEmpty())
    	  {
    		  logger.debug("alias for uuid {} - {}",alias, uuid);
    		  return alias;
    	  }
      }
      
	  logger.debug("no alias found");
	  return uuid;
  }
  
  public static String getTypeByUuid(String uuid)
  {
	  logger.debug("getting type documentation for uuid {}",uuid);
	  HstRequestContext requestContext = RequestContextProvider.get();
      Node mountContentNode = null;
      HstQueryManager hstQueryManager = null;
      GenericDAOResource genericDAOResource = new GenericDAOResource();

      try {
          hstQueryManager = requestContext.getQueryManager();
          String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
          mountContentNode = requestContext.getSession().getRootNode()
                  .getNode(PathUtils.normalizePath(mountContentPath));
      } catch (PathNotFoundException e) {
          logger.error(e.getMessage(),e);
          
      } catch (LoginException e) {
          logger.error(e.getMessage(),e);
      } catch (RepositoryException e) {
          logger.error(e.getMessage(),e);
      }
      HstQueryResult result = null;

      try {
          HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode);
          if (Objects.nonNull(uuid) && !uuid.isEmpty()) {
            Filter filter = hstQuery.createFilter();
          	filter.addEqualTo("jcr:uuid", uuid);
            hstQuery.setFilter(filter);
          }
          result = hstQuery.execute();
      } catch (QueryException e) {
          logger.error(e.getMessage(),e);
      }
      
      String type =DOCUMENTATION;
      if (result!=null && result.getSize()>0)
      {
    	  HippoBean document=result.getHippoBeans().nextHippoBean();
    	 
    	  try
    	  {
    		  type =document.getNode().getProperty("santanderbrxm:type").getString();
    		  //logger.info("Type documentation:"+type);
    		  
    		  if (type==null || type.isEmpty())
        	  {
    			  type=DOCUMENTATION;
        	  }
    	  } catch (RepositoryException e)
    	  {
    		  logger.debug("no type retrieved ");
    		  return type;
    	  }
    	 
      }
      
	
	  return type;
  }
  
  public String processExternalLink(String content, String html) {
    if(content.contains("?open=")) {
      String externalLink = "";
      String errorLink = "";
      logger.debug("External URL found. Going to process it.");
      Pattern p = Pattern.compile(PATTERN_EXTERNAL_LINK);
      Matcher m = p.matcher(content);
      while (m.find()) {
    	//this variable should contain the link URL
        externalLink = m.group(1); 
        logger.debug("Extracted URL is: {}" , externalLink);
        Pattern p2 = Pattern.compile(PATTERN_ERROR_LINK);
        Matcher m2 = p2.matcher(html);
        if (m2.find()) {
        	//this variable should contain the link URL
          errorLink = m2.group(0); 
          logger.debug("Extracted URL is: {}" , errorLink);
        }
        logger.debug("Replacing {} with {}" , errorLink , externalLink);
        html = html.replaceFirst(errorLink, "href=" + externalLink);
      }
    }
    return html;
  }
  
  public static String getProductAsignedDocument(String uuidDoc, HstRequestContext hstRequestContext, String locale, String channel) {
      
	  //Obtiene el Producto al que esta asociado un documento de tipo Documentation
      String productId="";
      String menuId="";
      
      
      try {
      
    	//logger.info("uuid documento:"+uuidDoc);
    	
  	    String mountContentPath = hstRequestContext.getResolvedMount().getMount().getContentPath();
  	    Node mountContentNode = hstRequestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(mountContentPath));
  	    HstQueryManager hstQueryManager = hstRequestContext.getQueryManager();
  	    
  	    
  	    //Recupero los MenuDocumentation y compruebo cual de ellos contiene el documento
  	    HstQuery hstQueryDocuments = hstQueryManager.createQuery(mountContentNode, MenuDocumentation.class);
  	 
  	     	
  	    HstQueryResult resultMenus = hstQueryDocuments.execute();
  	    
  	    
  	    HippoBeanIterator iterator = resultMenus.getHippoBeans();
  	    logger.debug("NUMERO DE MENUS: "+iterator.getSize());
  	  
  	 	   
  	    boolean encontradoMenu=false;
  	   
  	    while (iterator.hasNext() && !encontradoMenu) {
  	    	
  	    	MenuDocumentation menuDocumentation=(MenuDocumentation) iterator.nextHippoBean();
  	    	
  	    	//recupero sus documentos y si alguno de ellos coincide con el uuidDoc 
  	    	boolean isDocumentInMenu=isDocumentInMenu(uuidDoc, menuDocumentation,hstRequestContext);
  	 	    if (isDocumentInMenu) {
  	 	    	encontradoMenu=true;
  	 	    	menuId=menuDocumentation.getCanonicalUUID();
  	 	    }     
  	 	    
  	    }

  	    //Si el documento pertenece a algún Menú, se busca el producto al que está asociado ese Menu
  	    if (menuId!=null && !menuId.isEmpty()) {
  	    	
  	    	//Busco los productContainer de ese Site en ese idioma y se alguno de ellos tiene adociado ese menuId
   	    	productId=getProductMenuDocumentation(menuId,hstRequestContext,hstQueryManager,channel);
   	    }
      }
      
      
      catch (Exception e) {
    	  logger.error("error:"+e.getMessage());
    	  return 	productId;
      }
      
      
      return 	productId;
  }
      
  
 
  public static String getProductMenuDocumentation(String uuidMenu, HstRequestContext hstRequestContext,HstQueryManager hstQueryManager, String channel) {
  
  	String idProduct="";
  	logger.debug("uuid Menu:"+uuidMenu);
  	try {
  	
	  //Recupera el id del producto al que está asociado el MenuDocumentation
	  String mountContentPath = hstRequestContext.getResolvedMount().getMount().getContentPath();
      Node mountContentNode = hstRequestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(mountContentPath));
      HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode, ProductContainerItem.class);

      Filter filter = hstQuery.createFilter();
      filter.addEqualTo("santanderbrxm:deprecated", "No");
      hstQuery.setFilter(filter);
 
      HstQueryResult result = hstQuery.execute();
      
      HippoBeanIterator iterator = result.getHippoBeans();
      //logger.info("---NUMERO DE Productos: "+iterator.getSize());
      
      //ProductContainerExpandDTO productContainerExpandDTO=null;
      boolean encontradoProduct=false;
      
      while (iterator.hasNext() && !encontradoProduct) {
    	  ProductContainerItem productContainer = (ProductContainerItem) iterator.nextHippoBean();
    	  
                   
          if (Objects.nonNull(productContainer.getMenuDocumentation())) {
        	
        	  String idMenuProducto=productContainer.getMenuDocumentation().getCanonicalUUID();
	          if (idMenuProducto.equals(uuidMenu)) {
	        	  encontradoProduct=true;
	        	  idProduct=getAliasByUuid(productContainer.getCanonicalUUID());
	          }
	          
	       
          }
          else {
        	  logger.debug("---Este Producto NO TIENE MENU DOC ASOCIADO");
        	  
          }
          
          
          
      }
  }
  catch(Exception e) {
	  logger.error("error:"+e.getMessage());
  	  return idProduct;
  }
  
  return idProduct;
  
 
  
  } 
  
  



  
  public static boolean isDocumentInMenu(String uuidDoc, MenuDocumentation menuDocumentation, HstRequestContext requestContext) {
  	
	boolean  isDocumentInMenu=false;
	 	
	
	if (Objects.nonNull(menuDocumentation.getDocumentsGroups())) {
		    	
	    for (SubMenuDocumentationLevelOne level1 : menuDocumentation.getDocumentsGroups()) {
	    	    
	    	isDocumentInMenu=isDocumentInMenuLevelOne(uuidDoc,level1,requestContext);
	    	if (isDocumentInMenu) {
	    		break;
	    	}
	    }
	    	    

	}
		
	return isDocumentInMenu;		
		
	}
  
  public static boolean isDocumentInMenuLevelOne(String uuidDoc, SubMenuDocumentationLevelOne level1,HstRequestContext requestContext) {
	  	
		boolean  isDocumentInMenu=false;
		if (Objects.nonNull(level1.getDocuments())) {
			isDocumentInMenu=isDocumentInList(uuidDoc,level1.getDocuments(),requestContext);
		  	   //se busca en los documentos
			
		}
		
		if (!isDocumentInMenu) {
			//Si no se encuentra en los documentos, busco en documentsGroups
			 if (Objects.nonNull(level1.getDocumentsGroups())) {
					    	
				   
				    for (SubMenuDocumentationLevelTwo level2 : level1.getDocumentsGroups()) {
				    	    
				    	isDocumentInMenu=isDocumentInMenuLevelTwo(uuidDoc,level2, requestContext);
				    	if (isDocumentInMenu) break;
				    }
				    	    
		    
				}
		}	 
				
		return isDocumentInMenu;		
			
	}
  
  public static boolean isDocumentInMenuLevelTwo(String uuidDoc, SubMenuDocumentationLevelTwo level2,HstRequestContext requestContext) {
	  	
		boolean  isDocumentInMenu=false;
		if (Objects.nonNull(level2.getDocuments())) {
			isDocumentInMenu=isDocumentInList(uuidDoc,level2.getDocuments(),requestContext);
			
		}
		
		if (!isDocumentInMenu) {
			//Si no se encuentra en los documentos, busco en documentsGroups
			 if (Objects.nonNull(level2.getDocumentsGroups())) {
					    	
				   
				    for (SubMenuDocumentationLevelThree level3 : level2.getDocumentsGroups()) {
				    	    
				    	isDocumentInMenu=isDocumentInMenuLevelThree(uuidDoc,level3,requestContext);
				    	if (isDocumentInMenu) break;
				    }
				    	    
		    
				}
		}	 
				
		return isDocumentInMenu;		
			
	}
  
  public static boolean isDocumentInMenuLevelThree(String uuidDoc, SubMenuDocumentationLevelThree level3,HstRequestContext requestContext) {
	  	
		boolean  isDocumentInMenu=false;
		if (Objects.nonNull(level3.getDocuments())) {
			isDocumentInMenu=isDocumentInList(uuidDoc,level3.getDocuments(),requestContext);
			
		}
		
				
		return isDocumentInMenu;		
			
	}
  
  public static boolean isDocumentInList(String uuidDoc, List<HippoBean> documents, HstRequestContext requestContext) {
	  	
		boolean  isDocumentInList=false;
		if (!documents.isEmpty()) {
			try{
				for (HippoBean document : documents) {
		        	String uuid=HippoUtils.getNodeId(document.getNode().getPath(), requestContext);
		   			if (uuidDoc.equals(uuid)){
		   				isDocumentInList=true;
						break;
		   			}
		   				
		   		}
			}
			catch (Exception e) {
				return isDocumentInList;	
			}
		}
		
				
		return isDocumentInList;		
			
	}
  
  
  
  
  /*
  
  private void processMenuDocumentationBean(HstRequestContext requestContext,
			MenuDocumentationDTO menuDocumentationDTO, HippoBean menuDocumentationBean, Mount mountPoint) {
  	
  	
  	logger.info("menuDocumentationBean path: "+menuDocumentationBean.getPath());
		MenuDocumentation menuDocumentation = (MenuDocumentation) menuDocumentationBean;
			
		
		 
		
		//SubMenusDocumentationLevelOne
		if (Objects.nonNull(menuDocumentation.getDocumentsGroups())) {
			    	
		    List<SubMenuDocumentionLevelOneDTO> subMenusDocumentationLevelOne = new ArrayList<>();
		    for (SubMenuDocumentationLevelOne level1 : menuDocumentation.getDocumentsGroups()) {
		    	    
		        processMenuLevelOne(requestContext, subMenusDocumentationLevelOne, level1, mountPoint);
		        
		    }
		    	    
		    
		    menuDocumentationDTO.setDocumentsGroups(subMenusDocumentationLevelOne);
		    menuDocumentationDTO.setId(menuDocumentation.getCanonicalUUID());
		    
		}
		
		
	}
  
  
  */
  
      
 
  
  
  
}
