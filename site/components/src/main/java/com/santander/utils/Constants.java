package com.santander.utils;

public class Constants {

  private Constants() {}

  public static final String BRX_HOSTNAME = System.getenv("BRX_HOSTNAME");
  public static final String ASSET_HOSTNAME = System.getenv("ASSET_HOSTNAME");

  public static final String BEARER_AUTH_HEADER_PROPERTY = "Bearer ";

  public static final String HTTP_PROTOCOL = "http://";
  public static final String HTTPS_PROTOCOL = "https://";

  public static final String PARAMETER_LIMIT = "_limit=";
  public static final String PARAMETER_OFFSET = "&_offset=";
  public static final String PARAMETER_SITE = "&site=";


  public static final String CONTENT_ROOT_PATH = "/content/documents/santander";
  public static final String PROJECT_ROOT_PATH = "/content/documents/santander/santander-";
  public static final String BINARIES_SITE_CONTENT_PATH = "/site/binaries/content/";
  public static final String CONTENT_CONFIGURATION_PATH = "/content/documents/santander/configurations";

  //public static final String URL_PATH_WEBINARS_DETAIL = "webinars";
  //public static final String URL_PATH_ARTICLES_DETAIL = "articles";
  //public static final String URL_PATH_NEWS_DETAIL = "news";
    
  
  public static final String URL_PATH_WEBINARS_DETAIL = "resources";
  public static final String URL_PATH_ARTICLES_DETAIL = "resources";
  public static final String URL_PATH_NEWS_DETAIL = "resources";
    
    

  public static final String PROPERTY_SANTANDERBRXM_TYPE_WEBINAR = "webinar";
  public static final String PROPERTY_SANTANDERBRXM_TYPE_NEWS = "news";
  public static final String PROPERTY_SANTANDERBRXM_TYPE_ARTICLE = "article";
  public static final String PROPERTY_SANTANDERBRXM_TYPE = "santanderbrxm:type";
  public static final String PROPERTY_SANTANDERBRXM_DATE = "santanderbrxm:date";
  public static final String PROPERTY_SANTANDERBRXM_DEPRECATED = "santanderbrxm:deprecated";


  public static final String PROPERTY_PREVIOUS = "Previous";
  public static final String FACE_TITLE_2 = "_x002f_";
  public static final String FACE_TITLE = "x002f";

  public static final String URL_PATH_ARTICLE_DETAIL_OLD = "articleDetail";
  public static final String BINARIES = "binaries";
  public static final String RESOURCEAPI = "/resourceapi/";
  public static final String RESOURCEAPI_NAME = "resourceapi";
  
  public static final String CUSTOMAPI_NAME= "customapi";
  public static final String CUSTOMAPI = "/customapi/";
  public static final String SITE_CUSTOMAPI = "/site/customapi";
  
  public static final String LINK_BLOOMREACH = "link-bloomreach";

  public static final String LOCALE = "locale";
  public static final String CHANNEL = "channel";

  public static final String DOC_TYPE_PRODUCT = "santanderbrxm:Product";
  public static final String DOC_TYPE_API_ITEM = "santanderbrxm:ApiItem";
  public static final String DOC_TYPE_API_CONTAINER_ITEM = "santanderbrxm:ApiContainerItem";
  public static final String DOC_TYPE_DOCUMENTATION = "santanderbrxm:documentation";
  public static final String DOC_TYPE_GETTINGSTARTED = "santanderbrxm:GettingStartingPage";
  public static final String DOC_TYPE_NEWS_AND_ARTICLES = "santanderbrxm:NewsAndArticles";
  public static final String DOC_PRODUCT_CONTAINER_ITEM = "santanderbrxm:ProductContainerItem";

  public static final String SANTANDERBX_ALIAS = "santanderbrxm:alias";
  public static final String SANTANDERBX_TITLE = "santanderbrxm:title";
  //public static final String PRODUCT_DETAIL = "productDetail";
  public static final String PRODUCT_DETAIL = "productsApis";
  public static final String DOCUMENTATION = "documentation";
  public static final String GETSTARTED = "getStarted";
  public static final String GUIDE = "guide";
  public static final String DOCUMENTATION_TYPE="Documentation";
  public static final String GUIDE_TYPE="Guide";
  public static final String GETSTARTED_TYPE = "GetStarted";
  public static final String API = "api";
  public static final String PRODUCT_DOCUMENTATION = "productDocumentation";
 

  public static final String PATTERN_EXTERNAL_LINK = "href=\"(\\?.*)\"";
  public static final String PATTERN_INTERNAL_LINK = "<a\\s+(?:[^>]*?\\s+)?href=\"([^\"]*)\"";
  public static final String PATTERN_ERROR_LINK = "href=\"(.*pagenotfound?.*)\"";

  public static final String COMPONENT_CTA_BUTTON = "callToActionButton";
  public static final String COMPONENT_LINK_DOWNLOADS = "links";

  public static final String COMPONENT_DOCUMENT_ITEM = "documentItem";

  public static final String HIPPOTRANSLATION_LOCALE_PROPERTY = "hippotranslation:locale";

  public static final String HIPPOTAXONOMY_KEYS = "hippotaxonomy:keys";



}
