package com.santander.utils;

import org.hippoecm.hst.core.request.HstRequestContext;

import lombok.Data;

@Data
public class Locale {

	private String locale;
	
	public Locale() {
		// Auto-generated constructor stub
	}
	
    
    
    
    public String getLocaleContext(HstRequestContext requestContext, String split) {
	   	
    	String requestpath = requestContext.getBaseURL().getRequestPath();
    	String[] splitSite = requestpath.split("/"+split);
        String locale = setLocaleValue(splitSite);
        
        return locale;
           	
    }
    
    
    private String setLocaleValue(String[] splitSite) {
  		String locale = "";

  		if (splitSite.length >= 1) {
  		    String beforeSite = splitSite[0];
  		    locale = beforeSite.substring(beforeSite.lastIndexOf('/') + 1);// COGE DESDE LA ULTIMA BARRA
  		}
  		return locale;
  	}
}