package com.santander.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.util.PathUtils;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.beans.NewsAndArticles;
import com.santander.dto.HALLinkDTO;
import com.santander.dto.InformationDTO;
import com.santander.dto.InformationsDTO;
import com.santander.utils.Constants;
import com.santander.utils.GlobalConfiguration;
import com.santander.utils.HippoUtils;
import com.santander.utils.Locale;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

import static com.santander.utils.Constants.*;


@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM,
        MediaType.APPLICATION_FORM_URLENCODED})
@Path("/informations")
@Api(value = "News & Articles Page", description = "News And Articles page REST service")
public class NewsAndArticlesPageResource extends BaseRestResource {

    private static final int THREE = 3;
	private static Logger logger = LoggerFactory.getLogger(NewsAndArticlesPageResource.class);

    @ApiOperation(value = "Returns the list of content of the information page.",
            response = InformationDTO.class, authorizations = @Authorization(value = "clientCredentials",
            scopes = @AuthorizationScope(scope = "read_informations", description = "")))
    @ApiResponses({@ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = InformationDTO.class),

    })
    @GET
    public Map<String, InformationsDTO> getInformations(@Context HttpServletRequest servletRequest,
                                                        @Context HttpServletResponse servletResponse, 
                                                        @Context UriInfo uriInfo) {

        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        Map<String, InformationsDTO> content = new HashMap<>();

        String type = setTypes(queryParams);
        int limit = setLimit(queryParams);
        int offset = setOffset(queryParams);
        String site = setSite(queryParams);

        logger.debug("Invoking /informations with type: {}", type);
        logger.debug("Invoking /informations with limit: {}", limit);
        logger.debug("Invoking /informations with offset: {}", offset);
        logger.debug("Invoking /informations with site: {}", site);

        try {

            NewsAndArticles informationPage = null;
            HstRequestContext requestContext = RequestContextProvider.get();

            String contextPath = requestContext.getBaseURL().getContextPath();
            String requestpath = requestContext.getBaseURL().getRequestPath();
          

            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);

            // IDIOMA
            String[] splitSite = requestpath.split("/informations");
            logger.debug("splitSite0 : {}", splitSite[0]);
            logger.debug("splitSiteLenght : {}", splitSite.length);
            String locale = setLocale(splitSite);



            boolean isMarketplace = mountContentNode(site, requestContext, scope);
            HstQuery hstQuery = hstQueryManager.createQuery(scope, NewsAndArticles.class, true);
            Filter filter = hstQuery.createFilter();
            filter.addEqualTo(HIPPOTRANSLATION_LOCALE_PROPERTY, locale);// PARA FILTRAR POR IDIOMA
            hstQuery.setFilter(filter);

            // count
            HstQueryResult result = hstQuery.execute();
            // hstQuery.getQueryAsString(true); //PARA SACAR LA QUERY
            int count = result.getTotalSize();
            logger.debug("count: {}", count);

            if (limit >= 1 && offset >= 0) {
                hstQuery.setLimit(limit);
                hstQuery.setOffset(offset);
                result = hstQuery.execute();
            }

            // articlesCount
            HstQuery hstQueryArticles = hstQueryManager.createQuery(scope, NewsAndArticles.class, true);
            Filter filterArticles = hstQueryArticles.createFilter();
            filterArticles.addEqualTo(HIPPOTRANSLATION_LOCALE_PROPERTY, locale);
            filterArticles.addEqualTo(PROPERTY_SANTANDERBRXM_TYPE, PROPERTY_SANTANDERBRXM_TYPE_ARTICLE);
            hstQueryArticles.setFilter(filterArticles);
            hstQueryArticles.addOrderByDescending(PROPERTY_SANTANDERBRXM_DATE);

            HstQueryResult resultArticles = hstQueryArticles.execute();
            int articlesCount = resultArticles.getSize();
            logger.debug("articlesCount: {}", articlesCount);


            resultArticles = setResultArticles(type, limit, offset, hstQueryArticles, resultArticles);

            // newsCount
            HstQuery hstQueryNews = hstQueryManager.createQuery(scope, NewsAndArticles.class, true);
            Filter filterNews = hstQueryNews.createFilter();
            filterNews.addEqualTo(HIPPOTRANSLATION_LOCALE_PROPERTY, locale);
            filterNews.addEqualTo(PROPERTY_SANTANDERBRXM_TYPE, "news");
            hstQueryNews.setFilter(filterNews);
            hstQueryNews.addOrderByDescending(PROPERTY_SANTANDERBRXM_DATE);

            HstQueryResult resultNews = hstQueryNews.execute();
            int newsCount = resultNews.getSize();
            logger.debug("newsCount: {}", newsCount);

            resultNews = setResultNews(type, limit, offset, hstQueryNews, resultNews);


            // webinarsCount
            HstQuery hstQueryWebinars = hstQueryManager.createQuery(scope, NewsAndArticles.class, true);
            Filter filterWebinars = hstQueryWebinars.createFilter();
            filterWebinars.addEqualTo(HIPPOTRANSLATION_LOCALE_PROPERTY, locale);
            filterWebinars.addEqualTo(PROPERTY_SANTANDERBRXM_TYPE, PROPERTY_SANTANDERBRXM_TYPE_WEBINAR);
            hstQueryWebinars.setFilter(filterWebinars);
            hstQueryWebinars.addOrderByDescending(PROPERTY_SANTANDERBRXM_DATE);

            HstQueryResult resultWebinars = hstQueryWebinars.execute();
            int webinarsCount = resultWebinars.getSize();
            logger.debug("webinarsCount: {}", webinarsCount);

            resultWebinars = setResultWebinars(type, limit, offset, hstQueryWebinars, resultWebinars);


            // if a type of content is asked
            result = setResultByType(type, result, resultArticles, resultNews, resultWebinars);
            HippoBeanIterator iterator = result.getHippoBeans();
            List<InformationDTO> informations = new ArrayList<>();
            while (iterator.hasNext()) {
                informationPage = (NewsAndArticles) iterator.nextHippoBean();
                informations.add(new InformationDTO(informationPage));
            }

            List<HALLinkDTO> links = new ArrayList<>();

            if (offset >= 0 && offset <= result.getTotalSize() && limit > 0) {


                String requestURL =
                        getRequestURL(requestContext.getServletRequest().getRequestURL().toString());
                String localPath = setLocalPath(type, contextPath, requestpath, requestURL);

                // First
                firstLinks(limit, site, isMarketplace, links, localPath);


                // Previous
                previousLinks(limit, offset, site, isMarketplace, links, localPath);

                // Current
                currentLinks(limit, offset, site, isMarketplace, links, localPath);

                // Next
                nextLinks(limit, offset, site, isMarketplace, result, links, localPath);

                // Last
                lastLinks(limit, site, isMarketplace, result, links, localPath);
            }
            content.put("content",
                    InformationsDTO.builder().informations(informations).articlesCount(articlesCount)
                            .newsCount(newsCount).webinarsCount(webinarsCount).count(count).links(links).build());
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        return content;
    }

	private String setLocalPath(String type, String contextPath, String requestpath, String requestURL) {
		String localPath = requestURL + contextPath + requestpath;


		if (Objects.nonNull(type) && !type.isEmpty()
		        && ("article".equals(type) || "news".equals(type) || "webinar".equals(type))) {
		    localPath = localPath + "?type=" + type + "&";
		} else {
		    localPath = localPath + "?";
		}
		return localPath;
	}

	private HstQueryResult setResultNews(String type, int limit, int offset, HstQuery hstQueryNews,
			HstQueryResult resultNews) throws QueryException {
		if (Objects.nonNull(type) && !type.isEmpty() && "news".equals(type) && limit >= 1 && offset >= 0) {
		    hstQueryNews.setLimit(limit);
		    hstQueryNews.setOffset(offset);
		    resultNews = hstQueryNews.execute();
		}
		return resultNews;
	}

	private static HstQueryResult setResultByType(String type, HstQueryResult result, HstQueryResult resultArticles,
			HstQueryResult resultNews, HstQueryResult resultWebinars) {
		if (Objects.nonNull(type) && !type.isEmpty()) {
		    if ("article".equals(type)) {
		        result = resultArticles;
		    } else if ("news".equals(type)) {
		        result = resultNews;
		    } else if ("webinar".equals(type)) {
		        result = resultWebinars;
		    } else
		    {
		    	//nothing
		    }

		}
		return result;
	}

	private HstQueryResult setResultWebinars(String type, int limit, int offset, HstQuery hstQueryWebinars,
			HstQueryResult resultWebinars) throws QueryException {
		if (Objects.nonNull(type) && !type.isEmpty() && PROPERTY_SANTANDERBRXM_TYPE_WEBINAR.equals(type) 
				&& limit >= 1 && offset >= 0) {
		    hstQueryWebinars.setLimit(limit);
		    hstQueryWebinars.setOffset(offset);
		    resultWebinars = hstQueryWebinars.execute();
		}
		return resultWebinars;
	}

	private HstQueryResult setResultArticles(String type, int limit, int offset, HstQuery hstQueryArticles,
			HstQueryResult resultArticles) throws QueryException {
		if (Objects.nonNull(type) && !type.isEmpty() 
				&& PROPERTY_SANTANDERBRXM_TYPE_ARTICLE.equals(type) && limit >= 1 && offset >= 0) {
		    hstQueryArticles.setLimit(limit);
		    hstQueryArticles.setOffset(offset);
		    resultArticles = hstQueryArticles.execute();
		}
		return resultArticles;
	}

	private boolean mountContentNode(String site, HstRequestContext requestContext, HippoBean scope)
			throws PathNotFoundException, RepositoryException, LoginException {
		String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
		boolean isMarketplace = mountContentPath.contains("marketplace");
		logger.debug("isMarketpolace: {}", isMarketplace);

		// If is marketplace and when we recived the parameter site
		if (isMarketplace && Objects.nonNull(site) && !site.isEmpty() && "*".equals(site)) {
		    logger.debug("mountContentPath del marketplace: {}", mountContentPath);
		    mountContentPath = "/content/documents/santander";
		}

		Node mountContentNode = requestContext.getSession().getRootNode()
		        .getNode(PathUtils.normalizePath(mountContentPath));

		scope.setNode(mountContentNode);
		return isMarketplace;
	}

	private String setLocale(String[] splitSite) {
		String locale = "";

		if (splitSite.length >= 1) {
		    String beforeSite = splitSite[0];
		    locale = beforeSite.substring(beforeSite.lastIndexOf('/') + 1);// COGE DESDE LA ULTIMA BARRA
		    // HASTA EL FINAL
		    locale = locale.toLowerCase().replace("_", "-");
		    logger.debug("locale : {}", locale);
		}
		return locale;
	}

	private String setSite(MultivaluedMap<String, String> queryParams) {
		List<String> sites = queryParams.get("site");
        String site = Objects.nonNull(sites) && !sites.isEmpty() ? sites.get(0) : null;
		return site;
	}

	private int setOffset(MultivaluedMap<String, String> queryParams) {
		List<String> offsets = queryParams.get("_offset");
        int offset =
                Objects.nonNull(offsets) && !offsets.isEmpty() ? Integer.parseInt(offsets.get(0)) : -1;
		return offset;
	}

	private int setLimit(MultivaluedMap<String, String> queryParams) {
		List<String> limits = queryParams.get("_limit");
        int limit = Objects.nonNull(limits) && !limits.isEmpty() ? Integer.parseInt(limits.get(0)) : 0;
		return limit;
	}

	private String setTypes(MultivaluedMap<String, String> queryParams) {
		List<String> types = queryParams.get("type");
        String type = Objects.nonNull(types) && !types.isEmpty() ? types.get(0) : null;
		return type;
	}

	private void firstLinks(int limit, String site, boolean isMarketplace, List<HALLinkDTO> links, String localPath) {
		if (isMarketplace && Objects.nonNull(site) && !site.isEmpty() && "*".equals(site)) {
		    links.add(HALLinkDTO.builder().rel("First")
		            .href(localPath + PARAMETER_LIMIT + limit + "&_offset=0&site=*").build());
		} else {
		    links.add(HALLinkDTO.builder().rel("First")
		            .href(localPath + PARAMETER_LIMIT + limit + "&_offset=0").build());
		}
	}

	private void lastLinks(int limit, String site, boolean isMarketplace, HstQueryResult result, List<HALLinkDTO> links,
			String localPath) {
		if (isMarketplace && Objects.nonNull(site) && !site.isEmpty() && "*".equals(site)) {
		    links.add(HALLinkDTO.builder().rel("Last").href(
		            localPath + PARAMETER_LIMIT + limit + "&_offset=" + (result.getTotalSize() - 1) + "&site=*")
		            .build());
		} else {
		    links.add(HALLinkDTO.builder().rel("Last")
		            .href(localPath + PARAMETER_LIMIT + limit + "&_offset=" + (result.getTotalSize() - 1))
		            .build());
		}
	}

	private void nextLinks(int limit, int offset, String site, boolean isMarketplace, HstQueryResult result,
			List<HALLinkDTO> links, String localPath) {
		if (offset + limit < result.getTotalSize()) {
		    if (isMarketplace && Objects.nonNull(site) && !site.isEmpty() && "*".equals(site)) {
		        links.add(HALLinkDTO.builder().rel("Next")
		                .href(localPath + PARAMETER_LIMIT + limit + PARAMETER_OFFSET 
		                		+ (offset + limit) + PARAMETER_SITE + "*")
		                .build());
		    } else {
		        links.add(HALLinkDTO.builder().rel("Next")
		                .href(localPath + PARAMETER_LIMIT + limit + PARAMETER_OFFSET + (offset + limit)).build());
		    }

		}
	}

	private void currentLinks(int limit, int offset, String site, boolean isMarketplace, List<HALLinkDTO> links,
			String localPath) {
		if (isMarketplace && Objects.nonNull(site) && !site.isEmpty() && "*".equals(site)) {
		    links.add(HALLinkDTO.builder().rel("Current")
		            .href(localPath + PARAMETER_LIMIT + limit + PARAMETER_OFFSET + offset + PARAMETER_SITE + "*").build());
		} else {
		    links.add(HALLinkDTO.builder().rel("Current")
		            .href(localPath + PARAMETER_LIMIT + limit + PARAMETER_OFFSET + offset).build());
		}
	}

	private void previousLinks(int limit, int offset, String site, boolean isMarketplace, List<HALLinkDTO> links,
			String localPath) {
		if (offset != 0 && (offset - limit) >= 0) {
		    addLinks(limit, offset, site, isMarketplace, links, localPath);

		} else if (offset - limit < 0 && offset != 0) {
		    if (isMarketplace && Objects.nonNull(site) && !site.isEmpty() && "*".equals(site)) {
		        links.add(HALLinkDTO.builder().rel(PROPERTY_PREVIOUS)
		                .href(localPath + PARAMETER_LIMIT + offset + PARAMETER_OFFSET + "0" + PARAMETER_SITE + "*").build());
		    } else {
		        links.add(HALLinkDTO.builder().rel(PROPERTY_PREVIOUS)
		                .href(localPath + PARAMETER_LIMIT + offset + PARAMETER_OFFSET + "0").build());
		    }

		} else
		{
			//nothing
		}
	}

	private void addLinks(int limit, int offset, String site, boolean isMarketplace, List<HALLinkDTO> links,
			String localPath) {
		if (isMarketplace && Objects.nonNull(site) && !site.isEmpty() && "*".equals(site)) {
		    links.add(HALLinkDTO.builder().rel(PROPERTY_PREVIOUS)
		            .href(localPath + PARAMETER_LIMIT + limit + PARAMETER_OFFSET 
		            		+ (offset - limit) + PARAMETER_SITE + "*")
		            .build());
		} else {
		    links.add(HALLinkDTO.builder().rel(PROPERTY_PREVIOUS)
		            .href(localPath + PARAMETER_LIMIT + limit + PARAMETER_OFFSET + (offset - limit)).build());
		}
	}

    private String getRequestURL(String url) {
        String aux = url;
        int index = 0;
        int cont = 0;
        for (int i = 0; i < THREE; i++) {
            index = aux.indexOf('/');
            aux = aux.substring(index + 1, aux.length());
            cont = cont + index + 1;
        }
        return url.substring(0, cont - 1);
    }

    @ApiOperation(value = "Returns the detail of the content of the informations page.",
            response = InformationDTO.class, authorizations = @Authorization(value = "clientCredentials",
            scopes = @AuthorizationScope(scope = "read_informations", description = "")))
    @ApiResponses({@ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = InformationDTO.class),

    })
    @GET
    @Path("/{uuid}/")
    public Map<String, InformationDTO> getInformationsByID(@Context HttpServletRequest servletRequest,
                                                           @Context HttpServletResponse servletResponse, 
                                                           @Context UriInfo uriInfo,
                                                           @PathParam("uuid") String uuid) {

        logger.debug("Invoking /informations/{uuid} : {}", uuid);


        Map<String, InformationDTO> content = new HashMap<>();

        try {
            NewsAndArticles informationPage = null;
            InformationDTO information = new InformationDTO();
            HstRequestContext requestContext = RequestContextProvider.get();
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);
            HstQuery hstQuery = hstQueryManager.createQuery(scope, NewsAndArticles.class, true);
            
            
            //Nombre del Site
			String scopePath=scope.getPath();
            String[] splitSite =scopePath.split(Constants.PROJECT_ROOT_PATH);
			
			String siteName="";
            if (splitSite.length > 1) {
            	siteName=splitSite[1].substring(0,splitSite[1].indexOf("/"));
            	logger.debug("*****siteName:"+siteName);
            }
            

            if (Objects.nonNull(uuid) && !uuid.isEmpty()) {
                 
                Filter filter = hstQuery.createFilter();
                Filter filterAlias = hstQuery.createFilter(); 
            	Filter filterUuid = hstQuery.createFilter();
            	filterAlias.addEqualTo("santanderbrxm:alias", uuid);
            	filterUuid.addEqualTo("jcr:uuid", uuid);
            	filter.addOrFilter(filterAlias);
            	filter.addOrFilter(filterUuid);
                hstQuery.setFilter(filter);
                hstQuery.addOrderByDescending("santanderbrxm:date");
   
                HstQueryResult result = hstQuery.execute();
	            HippoBeanIterator iterator = result.getHippoBeans();
	           
	            
	            if (iterator.getSize()>0) {
	            	logger.debug("*****SI encontrado News&articles en ese scope");
	                informationPage = (NewsAndArticles) iterator.nextHippoBean();
	                information = new InformationDTO(informationPage);
	            }
	            
	            else { //No se ha encontrado en ese idioma. Se busca el de por defecto. 
	            	//si el idioma por defecto coincide con el del scope, pues no hay contenido a devolver
	            	//Si no coincide->se devuelve el contenido del idioma por defecto
	            	logger.debug("*****NO encontrado News&articles en ese scope");
	            	
	            	//locale
	            	Locale locale=new Locale();
	            	String localeContext=locale.getLocaleContext(requestContext,"informations");
	                logger.debug("*****localeContext:"+localeContext);
	                
	                //Recupero el contenido SantanderGlobalConfiguration
	                GlobalConfiguration globalConfiguration=new GlobalConfiguration();
	             
	                String defaultLocale=globalConfiguration.getConfigurationById("DEFAULT_LANGUAGE_"+siteName,requestContext);
	               
	                
	                if (!localeContext.equals(defaultLocale)) {
		            	logger.debug("*****EL LOCALE DEL CONTEXT NO ES EL MISMO QUE EL DE POR DEFECTO. BUSCO EL DE POR DEFECTO");  
		                	  
		                	  
		            	HippoBeanIterator informationsIterator = HippoUtils.getObjectTypeDefaultLanguaje(requestContext,"NewsAndArticles",defaultLocale,uuid,hstQueryManager);	  
	     	            if (informationsIterator!=null && informationsIterator.getSize()>0) { 
	     	            	informationPage = (NewsAndArticles) informationsIterator.nextHippoBean();
	     	                information = new InformationDTO(informationPage);
	         	        }
	         	            
	         	        else {
	         	            	logger.debug("*****NO encontrado information en el idioma por defecto");
	         	        }
	                }
	            } 
            }
	            
            content.put("content", information);
        }
        catch (Exception e) {
        	logger.error("ERROR News&ArticlesPage:"+e.getMessage());
        	e.printStackTrace();
            throw new WebApplicationException(e);
        }
        logger.debug("antes return");

        return content;
    }   

    
}
