package com.santander.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.util.PathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.beans.GettingStartingPage;
import com.santander.beans.SectionLevelOne;
import com.santander.beans.SectionLevelThree;
import com.santander.beans.SectionLevelTwo;
import com.santander.dto.DescriptionDTO;
import com.santander.dto.DocumentationDTO;
import com.santander.dto.DocumentationLinkDTO;
import com.santander.dto.GenericDAOResource;
import com.santander.dto.GetStartedDTO;
import com.santander.dto.GetStartedListDTO;
import com.santander.dto.GetStartedSectionDTO;
import com.santander.dto.GetStartedsDTO;
import com.santander.dto.HALLinkDTO;
import com.santander.utils.Constants;
import com.santander.utils.GlobalConfiguration;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import com.santander.utils.Locale;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@Path("/getstarted")
@Api(value = "GetStarted", description = "getStarted Page REST service")
public class GettingStartingPageResource extends org.hippoecm.hst.jaxrs.services.AbstractResource {

    private static final int INDEX = 3;
	private static final String OFFSET = "&_offset=";
	private static final String LIMIT = "?_limit=";
	private static Logger logger = LoggerFactory.getLogger(GettingStartingPageResource.class);

    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
    @ApiOperation(value = "Returns the content of GetStarted Page.", response = GetStartedDTO.class,
            authorizations = @Authorization(value = "clientCredentials",
                    scopes = @AuthorizationScope(scope = "read_getstarted", description = "")))
    @ApiResponses({@ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = GetStartedDTO.class),

    })
    @GET
    @Path("/{uuid}/")
    public Map<String, GetStartedDTO> getStartedPage(@Context HttpServletRequest servletRequest,
                                                     @Context HttpServletResponse servletResponse, 
                                                     @Context UriInfo uriInfo,
                                                     @NotNull @PathParam("uuid") String uuid) {
        logger.debug("****************************** Invoke /getstarted/{uuid}/");
        Map<String, GetStartedDTO> content = new HashMap<>();
       
        try {
	        HstRequestContext requestContext = RequestContextProvider.get();
	        HstQueryManager hstQueryManager = getHstQueryManager(requestContext.getSession(), requestContext);
	        String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
	        Node mountContentNode = requestContext.getSession().getRootNode()
                    .getNode(PathUtils.normalizePath(mountContentPath));
	        
	        
	       //Nombre del Site
	        HippoBean scope = getMountContentBaseBean(requestContext);
			String scopePath=scope.getPath();
            String[] splitSite =scopePath.split(Constants.PROJECT_ROOT_PATH);
			
			String siteName="";
            if (splitSite.length > 1) {
            	siteName=splitSite[1].substring(0,splitSite[1].indexOf("/"));
            	logger.debug("*****siteName:"+siteName);
            }
            
	        
	       
	      
	        GenericDAOResource genericDAOResource = new GenericDAOResource();
	        GetStartedDTO doc = new GetStartedDTO();

	        if (!uuid.isEmpty()) {
	        	HstQueryResult result = null;
	        	result = getQueryResult(uuid, mountContentNode, hstQueryManager, result);
      
	        	HippoBeanIterator iterator = result.getHippoBeans();
		
		       
		        if (iterator.getSize()>0) { //encontrado en ese idioma
		
		        	setSections(requestContext, genericDAOResource, iterator, doc);
		        	
		        }
		        
		        else {
		        	//locale
	            	Locale locale=new Locale();
	            	String localeContext=locale.getLocaleContext(requestContext,"getstarted");
	                
	                
	                //Recupero el contenido SantanderGlobalConfiguration
	                GlobalConfiguration globalConfiguration=new GlobalConfiguration();
	                String defaultLocale=globalConfiguration.getConfigurationById("DEFAULT_LANGUAGE_"+siteName,requestContext);
	               
	                
	               
		            if (!localeContext.equals(defaultLocale)) {
		            	HippoBeanIterator getStartedIterator = HippoUtils.getObjectTypeDefaultLanguaje(requestContext,"GettingStartingPage",defaultLocale,uuid,hstQueryManager);	  
         	            if (getStartedIterator!=null && getStartedIterator.getSize()>0) { //encontrado en ese idioma
         	            	setSections(requestContext, genericDAOResource, getStartedIterator, doc);
	         	
	         	        }
	         	            
	         	        else {
	         	            	logger.debug("*****NO encontrado getStarted en el idioma por defecto");
	         	        }
	               }
		        	
		        	
		        }
	        }       

	        content.put("content", doc);
	        return content;
        }    
        catch (Exception e) {
        	logger.error("ERROR GettingStartingPage:"+e.getMessage());
        	e.printStackTrace();
            throw new WebApplicationException(e);
        }
       

      
    }

	private void setSections(HstRequestContext requestContext, GenericDAOResource genericDAOResource,
			HippoBeanIterator iterator, GetStartedDTO doc) 
	{
		if (iterator.hasNext()) 
		{
			
			//Step 1. Retrieve base level information
            GettingStartingPage hippoBean = (GettingStartingPage) iterator.nextHippoBean();
            doc.setTitle(hippoBean.getTitle());
            try {
            	doc.setDescription(Objects.nonNull(hippoBean.getDescription())
                    ? genericDAOResource.rewriteHstRichContent(hippoBean.getDescription().getContent(),
                    hippoBean.getDescription().getNode(), requestContext.getResolvedMount().getMount())
                    : StringUtils.EMPTY);
            
            }
            catch(Exception e) {
            	
            	doc.setDescription(StringUtils.EMPTY);
            }
            doc.setLinks(genericDAOResource.processlinksAndDownloads(hippoBean.getLinks()));
            
            
            //Step 2. Iterates and populates sections
            List<GetStartedSectionDTO> pages = new ArrayList<>();
            
            //Compruebo di el contenido que se está pidiendo es el del lenguaje del contenido o el del lenguaje por defecto
            Map<String, String> urlAttrs =
                    HippoUtils.getDocumentAttrsByURLCustom(requestContext, hippoBean);
            
            
            Mount mountPoint=requestContext.getResolvedMount().getMount();
            try {
   		        
   		        String localeContent=hippoBean.getLocaleString().replace("-", "_");
   		        logger.debug("*****locale Contenido:"+localeContent);
   		       
   		        String mountRequestPath = requestContext.getResolvedMount().getMount().getContentPath();
   		        String localeMountRequestPath=mountRequestPath.substring(mountRequestPath.lastIndexOf("/")+1);
   		        logger.debug("*****locale request:"+localeMountRequestPath);
   		         
   		       
   		        if (!localeContent.equals(localeMountRequestPath)){ //Si el lenguaje del contenido no se corresponde con el de la petición, busco el Mount del contenido
   		          	 mountPoint=requestContext.getMount(urlAttrs.get("channel")+"-"+localeContent); //coge el Mount con el atributo alias=superdiginal-es_es
   		         	logger.debug("*****mountPoint:"+mountPoint);
   		         	 
   		         }
   		         
   		        
            }
            catch(Exception e) {
            	 logger.error("ERROR GetStartedService:"+e.getMessage());
            }
            
            
            
             for (SectionLevelOne level1 : hippoBean.getSections()) 
            {
                logger.debug("Level one");
                GetStartedSectionDTO page = new GetStartedSectionDTO();
                page.setTitle(level1.getTitle());
                try {
	                page.setDescription(DescriptionDTO.builder()
	                        .content(Objects.nonNull(level1.getDescription())
	                                ? genericDAOResource.rewriteHstRichContent(level1.getDescription().getContent(),
	                                level1.getDescription().getNode(), mountPoint)
	                                : StringUtils.EMPTY)
	                        .build());
                }
                catch (Exception e) {
                	page.setDescription(DescriptionDTO.builder()
	                        .content(StringUtils.EMPTY).build());
                	
                }
                
                setSectionDocuments(page,level1.getDocumentation(),requestContext);
                List<GetStartedSectionDTO> sections = new ArrayList<>();
                for (SectionLevelTwo level2 : level1.getSections()) 
                {
                    logger.debug("Level two");
                    GetStartedSectionDTO section = new GetStartedSectionDTO();
                    section.setTitle(level2.getTitle());
                    try {
                    section.setDescription(
                                    DescriptionDTO.builder().content(Objects.nonNull(level2.getDescription())
                                            ? genericDAOResource.rewriteHstRichContent(
                                            level2.getDescription().getContent(), level2.getDescription().getNode(),
                                            mountPoint)
                                            : StringUtils.EMPTY).build());
                    
                    }
                    catch (Exception e) {
                    	 section.setDescription(
                                 DescriptionDTO.builder().content(StringUtils.EMPTY).build());
                 
                    }
                    setSectionDocuments(section,level2.getDocumentation(),requestContext);
                    List<GetStartedSectionDTO> sections2 = new ArrayList<>();
                    addSections2(requestContext, genericDAOResource, level2, sections2, mountPoint);
                    section.setSections(sections2);
                    sections.add(section);
                }
                page.setSections(sections);
                pages.add(page);

            }

            doc.setSections(pages);
        }
	}


	private void addSections2(HstRequestContext requestContext, GenericDAOResource genericDAOResource,
			SectionLevelTwo level2, List<GetStartedSectionDTO> sections2, Mount mountPoint) {
		for (SectionLevelThree level3 : level2.getSections()) {
		    logger.debug("Level three");
		    GetStartedSectionDTO section2 = new GetStartedSectionDTO();
		    section2.setTitle(level3.getText());
		    try {
		    section2.setDescription(DescriptionDTO.builder()
		            .content(Objects.nonNull(level3.getHtml())
		                    ? genericDAOResource.rewriteHstRichContent(level3.getHtml().getContent(),
		                    level3.getHtml().getNode(), mountPoint)
		                    : StringUtils.EMPTY)
		            .build());
		    }
		    catch(Exception e) {
		    	 section2.setDescription(DescriptionDTO.builder()
				            .content(StringUtils.EMPTY)
				            .build());
		    }
		    setSectionDocuments(section2,level3.getDocumentation(),requestContext);
		    sections2.add(section2);
		}
	}


	private HstQueryResult getQueryResult(String uuid, Node mountContentNode, HstQueryManager hstQueryManager,
			HstQueryResult result) {
		try {
            HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode, GettingStartingPage.class);
            if (Objects.nonNull(uuid) && !uuid.isEmpty()) {
                Filter filter = hstQuery.createFilter();
                Filter filterAlias = hstQuery.createFilter(); 
            	Filter filterUuid = hstQuery.createFilter();
            	filterAlias.addEqualTo("santanderbrxm:alias", uuid);
            	filterUuid.addEqualTo("jcr:uuid", uuid);
            	filter.addOrFilter(filterAlias);
            	filter.addOrFilter(filterUuid);
                hstQuery.setFilter(filter);
            }
            result = hstQuery.execute();
        } catch (QueryException e) {
            logger.error(e.getMessage(),e);
        }
		return result;
	}


    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
    @ApiOperation(value = "Returns the list of content of the GettingStarted.",
            response = GetStartedListDTO.class, authorizations = @Authorization(value = "clientCredentials",
            scopes = @AuthorizationScope(scope = "read_getstarted", description = "")))
    @ApiResponses({@ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = GetStartedListDTO.class),

    })
    @GET
    public Map<String, GetStartedsDTO> getGettingStartedList(@Context HttpServletRequest servletRequest,
                                                             @Context HttpServletResponse servletResponse, 
                                                             @Context UriInfo uriInfo) {

        //Devuelve una lista de GettingStarted
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        Map<String, GetStartedsDTO> content = new HashMap<>();

        List<String> limits = queryParams.get("_limit");
        int limit = Objects.nonNull(limits) && !limits.isEmpty() ? Integer.parseInt(limits.get(0)) : 0;

        List<String> offsets = queryParams.get("_offset");
        int offset =
                Objects.nonNull(offsets) && !offsets.isEmpty() ? Integer.parseInt(offsets.get(0)) : -1;

        List<String> sites = queryParams.get("site");
        String site = Objects.nonNull(sites) && !sites.isEmpty() ? sites.get(0) : null;

        logger.debug("Invoking /getstarted with limit: {}" , limit);
        logger.debug("Invoking /getstarted with offset: {}" , offset);
        logger.debug("Invoking /getstarted with site: {}" , site);

        try {

            HstRequestContext requestContext = RequestContextProvider.get();

            String contextPath = requestContext.getBaseURL().getContextPath();
            String requestpath = requestContext.getBaseURL().getRequestPath();
            logger.debug("Invoking /getstarted contextPath: {}" , contextPath);
            logger.debug("Invoking /getstarted requestpath: {}" , requestpath);
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);

            logger.debug("scope path: {}" , scope.getPath());

            HstQuery hstQuery=hstQueryManager.createQuery(scope, GettingStartingPage.class,true);

            //Número total
            HstQueryResult result = hstQuery.execute();

            int count = result.getTotalSize();
            logger.debug("count: {}" , count);


            if (limit >= 1 && offset >= 0) { //si hay que filtrar por páginacion, se ejecuta de nuevo la query
                hstQuery.setLimit(limit);
                hstQuery.setOffset(offset);
                result = hstQuery.execute();
            }

            HippoBeanIterator iterator = result.getHippoBeans();
            List<GetStartedListDTO> getStarteds = new ArrayList<>();
            GettingStartingPage gettingStartedPage = null;
            while (iterator.hasNext()) {
               
                gettingStartedPage = (GettingStartingPage) iterator.nextHippoBean();
                getStarteds.add(new GetStartedListDTO(gettingStartedPage));

            }

            List<HALLinkDTO> links = new ArrayList<>();


            processLinks(limit, offset, requestContext, contextPath, requestpath, result, links);

            content.put("content",GetStartedsDTO.builder()._embedded(getStarteds)._count(count)._links(links).build());
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        return content;
    }


	private void processLinks(int limit, int offset, HstRequestContext requestContext, String contextPath,
			String requestpath, HstQueryResult result, List<HALLinkDTO> links) {
		if (offset >= 0 && offset <= result.getTotalSize() && limit > 0) {
		    String requestURL =
		            getRequestURL(requestContext.getServletRequest().getRequestURL().toString());
		    String localPath = requestURL + contextPath + requestpath;
		    // First
		    links.add(HALLinkDTO.builder().rel("First")
		            .href(localPath + LIMIT + limit + "&_offset=0").build());
		    // Previous
		    if (offset != 0 && (offset - limit) >= 0) {
		        links.add(HALLinkDTO.builder().rel("Previous")
		                .href(localPath + LIMIT + limit + OFFSET + (offset - limit)).build());
		    } else if (offset - limit < 0 && offset != 0) {
		        links.add(HALLinkDTO.builder().rel("Previous")
		                .href(localPath + LIMIT + offset + "&_offset=0").build());
		    } else
		    {
		    	// do nothing
		    }

		    // Current
		    links.add(HALLinkDTO.builder().rel("Current")
		            .href(localPath + LIMIT + limit + OFFSET + offset).build());
		    // Next
		    if (offset + limit < result.getTotalSize()) {
		        links.add(HALLinkDTO.builder().rel("Next")
		                .href(localPath + LIMIT + limit + OFFSET + (offset + limit)).build());
		    }
		    // Last
		    links.add(HALLinkDTO.builder().rel("Last")
		            .href(localPath + LIMIT + limit + OFFSET + (result.getTotalSize() - 1))
		            .build());

		}
	}

	
	private void setSectionDocuments (GetStartedSectionDTO section,List<HippoBean> documents,HstRequestContext requestContext)
	{
		if (documents==null || documents.isEmpty())
		{
			return;
		}
		
		logger.debug("MACF Setting {} documents to section {}",documents.size(),section.getTitle());
		
		
		try
		{
			List<DocumentationLinkDTO> documentsRelation = new ArrayList<DocumentationLinkDTO>();
			InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
			for (HippoBean document : documents)
			{ 
				String title=null;
				if (document.getNode()!=null && document.getNode().getProperty("santanderbrxm:title")!=null)
				{
					title=document.getNode().getProperty("santanderbrxm:title").getString();
				
				} else if (document.getNode()!=null && document.getNode().getProperty("santanderbrxm:tutle")!=null) 
				{
					title=document.getNode().getProperty("santanderbrxm:tutle").getString();
				} else
				{
					//nothing
				}
				
				
				String url =requestContext.getHstLinkCreator().create(document, requestContext)
		                .toUrlForm(requestContext, true);
				String uuid=HippoUtils.getNodeId(document.getNode().getPath(), requestContext);
				
				String type = document.getNode().getPrimaryNodeType().getName();

				DocumentationLinkDTO documentationLinkDTO = new DocumentationLinkDTO();
				documentationLinkDTO.setTitle(title);
				documentationLinkDTO.setInternalLink(internalLinkProcessor.generateInternalLink(url, type, uuid));

				documentsRelation.add(documentationLinkDTO);
			}
			
			section.setDocumentsRelation(documentsRelation);
		} catch (Exception e)
		{
			logger.error("Exception retrieving documents",e);
		}
	}
	
    private String getRequestURL(String url) {
        String aux = url;
        int index = 0;
        int cont = 0;
        for (int i = 0; i < INDEX; i++) {
            index = aux.indexOf('/');
            aux = aux.substring(index + 1, aux.length());
            cont = cont + index + 1;
        }
        return url.substring(0, cont - 1);
    }

    public String generateHstLink(HippoBean bean) {
        return RequestContextProvider.get().getHstLinkCreator()
                .create(bean, RequestContextProvider.get()).toUrlForm(RequestContextProvider.get(), true);
    }
}
