package com.santander.rest;

import com.santander.dto.FacetsDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

import org.hippoecm.hst.site.HstServices;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;
import org.onehippo.taxonomy.api.Taxonomy;
import org.onehippo.taxonomy.api.TaxonomyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;




@Path("/facets")
@Api(value = "Facets", description = "Facets REST service")
public class FacetResource extends BaseRestResource {

    private static Logger logger = LoggerFactory.getLogger(FacetResource.class);
    
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})

    @ApiOperation(value = "Returns a internal menu for a site", response = FacetsDTO.class, authorizations = {
            @Authorization(value = "clientCredentials"
            		, scopes = @AuthorizationScope(scope = "read_facets", description = ""))
    })
    @ApiResponses({
            @ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500, message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value", response = FacetsDTO.class),

    })

    @GET
    @Path("/")
    public Map<String, FacetsDTO> getFacet(@Context HttpServletRequest request,
                                        @Context HttpServletResponse response) {
      
      logger.debug("*******Inicio FacetResource********");  
      Map<String, FacetsDTO> content = new HashMap<>();
        FacetsDTO facets=null;
        try {
            TaxonomyManager taxonomyManager =HstServices.getComponentManager()
            		.getComponent(TaxonomyManager.class.getName());
            Taxonomy taxonomy = taxonomyManager.getTaxonomies().getTaxonomy("Facet");
            facets=(new FacetsDTO(taxonomy));
            content.put("content",facets);

        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        return content;
    }
}
