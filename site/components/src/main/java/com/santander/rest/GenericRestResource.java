package com.santander.rest;

import com.santander.dto.ChannelsListDTO;
import com.santander.utils.Locale;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

import static com.santander.utils.Constants.CONTENT_ROOT_PATH;

@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
public class GenericRestResource extends BaseRestResource {

	private static Logger logger = LoggerFactory.getLogger(GenericRestResource.class);


	@ApiOperation(value = "Returns all available channels and their languages", response = ChannelsListDTO.class, authorizations = @Authorization(value = "clientCredentials", scopes = @AuthorizationScope(scope = "read_channels", description = "")))
	@ApiResponses({
			@ApiResponse(code = 401, message = "Client could not be authenticated"),
			@ApiResponse(code = 403, message = "Client is not authorized to make this request."),
			@ApiResponse(code = 404, message = "The specified resource could not be found."),
			@ApiResponse(code = 500, message = "Unable to complete the request because an unexpected error occurred."),
			@ApiResponse(code = 200, message = "Successful retrieval of param value", response = ChannelsListDTO.class),

	})
	@GET
	@Path("channels")
	public List<ChannelsListDTO> getChannels(@Context HttpServletRequest request, @Context HttpServletResponse response) {
		logger.debug("/channels end point called");
		List<ChannelsListDTO> channelsListDTOList = new ArrayList<>();
		try {
			HstRequestContext requestContext = RequestContextProvider.get();
			Session session = requestContext.getSession();
			Node contentRootNode = session.getNode(CONTENT_ROOT_PATH);
			NodeIterator channels = contentRootNode.getNodes();
			while (channels.hasNext()) {
				Node n = channels.nextNode();
				if(n.getName().contains("santander")) {
					NodeIterator n2 = n.getNodes("*_*");
					if(n2.getSize() != 0) {
						ChannelsListDTO channelListDTO = new ChannelsListDTO();
						List<Locale> locales = new ArrayList<>();
						channelListDTO.setChannel(StringUtils.substringAfter(n.getName(), "santander-"));
						while (n2.hasNext()) {
							Node localeNode = n2.nextNode();
							logger.debug("Adding locale " + localeNode.getName() + " for channel " + n.getName());
							Locale locale = new Locale();
							locale.setLocale(localeNode.getName());
							locales.add(locale);
						}
						channelListDTO.setLanguages(locales);
						channelsListDTOList.add(channelListDTO);
					}
				}
			}
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
		return channelsListDTOList;
	}

}