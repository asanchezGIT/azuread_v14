package com.santander.components;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Strings;
import com.santander.beans.CardCollection;
import com.santander.beans.CardItem;
import com.santander.dto.GenericDAOResource;

@ParametersInfo(type = CardCollectionComponentInfo.class)
public class CardCollectionComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(CardCollectionComponent.class);

  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    try {
      super.doBeforeRender(request, response);
      CardCollectionComponentInfo cardCollectionComponentInfo = getComponentParametersInfo(request);
      String cardCollection = cardCollectionComponentInfo.getCardCollection();
      if (!Strings.isNullOrEmpty(cardCollection)) {
        CardCollection object1 = (CardCollection) request.getRequestContext().getObjectBeanManager()
        		.getObject(cardCollection);
        
        //necesario para preview
        request.setModel("document", object1);
        
        request.setModel("cardCollection", object1);
        int cont = 0;
        for (CardItem item : object1.getCardItem()) {
          //OLD
        /*  String fixDescription = (new GenericDAOResource()).rewriteHstRichContent(item.getDescription().getContent()
        		  , item.getDescription().getNode(), RequestContextProvider.get()
        		  .getResolvedMount().getMount());
                */
          cont++;
        }
      }
    } catch (ObjectBeanManagerException e) {
      logger.error("exception cardcollection {}", e.getMessage());
    }
  }

}
