package com.santander.components;

import com.google.common.base.Strings;
import com.santander.beans.SEO;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@ParametersInfo(type = SEOComponentInfo.class)
public class SEOComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(SEOComponent.class);


    @Override
    public void doBeforeRender(HstRequest request, HstResponse response){
         try{
            super.doBeforeRender(request, response);
            SEOComponentInfo seoComponentInfo = getComponentParametersInfo(request);
            String seoComponentInfoDocument = seoComponentInfo.getSEO();
            if (!Strings.isNullOrEmpty(seoComponentInfoDocument)){
                SEO seo = (SEO) request.getRequestContext().getObjectBeanManager()
                        .getObject(seoComponentInfoDocument);
                request.setModel("document", seo);

                request.setModel("titleSEO", seo.getTitle());
            }
        }catch (ObjectBeanManagerException obme){
            logger.error("Error: ", obme);
        }
    }
}
