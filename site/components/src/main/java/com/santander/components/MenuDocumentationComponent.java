package com.santander.components;


import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import com.google.common.base.Strings;
import com.santander.beans.MenuDocumentation;
import com.santander.components.model.MenuDocumentationDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = MenuDocumentationComponentInfo.class)
public class MenuDocumentationComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(MenuDocumentationComponent.class);

  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    try {
      super.doBeforeRender(request, response);
      MenuDocumentationComponentInfo menuDocumentationComponentInfo = getComponentParametersInfo(request);
      String menuDocumentationString = menuDocumentationComponentInfo.getMenuDocumentation();
      
      if (!Strings.isNullOrEmpty(menuDocumentationString)) {
    	  MenuDocumentation menuDocumentation = (MenuDocumentation) request.getRequestContext()
        		.getObjectBeanManager().getObject(menuDocumentationString);
    	  
    	  
    	//MenuDocumentationDTO menuDocumentationDTO= new MenuDocumentationDTO(menuDocumentation);
    	//request.setModel("MenuDocumentation", menuDocumentationDTO);
        request.setModel("document", menuDocumentation);
        
      }
    } catch (ObjectBeanManagerException e) {
      logger.error("exception {}", e.getMessage());
    }
  }

}
