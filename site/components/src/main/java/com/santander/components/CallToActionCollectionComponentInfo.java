package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface CallToActionCollectionComponentInfo {

    @Parameter(name = "callToActionCollection", displayName = "Call to Action Collection")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:CallToActionCollection"})
    String getCallToActionCollection();
}

