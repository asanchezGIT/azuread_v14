package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;
@FunctionalInterface
public interface SEOComponentInfo {

    @Parameter(name = "seoComponentDocument", displayName = "SEO Component Document")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:SEO"})
    String getSEO();
}
