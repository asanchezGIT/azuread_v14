package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface ListDescriptionComponentInfo {

  @Parameter(name = "document", displayName = "ListDescription")
  @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:ListDescription"})
  String getListDescription();

}
