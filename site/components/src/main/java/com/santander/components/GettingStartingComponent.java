package com.santander.components;

import com.google.common.base.Strings;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = GettingStartingComponentInfo.class)
public class GettingStartingComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(GettingStartingComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {
        try {
            super.doBeforeRender(request, response);
            GettingStartingComponentInfo gettingStartingComponentInfo = getComponentParametersInfo(request);
            String getStarted1path = gettingStartingComponentInfo.getStarted1();
            String getStarted2path = gettingStartingComponentInfo.getStarted2();
            String getStarted3path = gettingStartingComponentInfo.getStarted3();
            if (!Strings.isNullOrEmpty(getStarted1path)) {
                Object object1 = request.getRequestContext().getObjectBeanManager().getObject(getStarted1path);
                request.setModel("document1", object1);
            }
            if (!Strings.isNullOrEmpty(getStarted2path)) {
                Object object2 = request.getRequestContext().getObjectBeanManager().getObject(getStarted2path);
                request.setModel("document2", object2);
            }
            if (!Strings.isNullOrEmpty(getStarted3path)) {
                Object object3 = request.getRequestContext().getObjectBeanManager().getObject(getStarted3path);
                request.setModel("document3", object3);
            }
        } catch (ObjectBeanManagerException e) {
            logger.error("Error: {}", e.getMessage());
        }
    }

}