package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

public interface GettingStartingComponentInfo {
	
@Parameter(name = "gettingstarted1", displayName = "Getting Started 1")
@JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:gettingStartedItem"}, 
pickerInitialPath = "/content/documents/santander/santander-spain/en/content/pages/homepage/getstarteditems")
String getStarted1();

@Parameter(name = "gettingstarted2", displayName = "Getting Started 2")
@JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:gettingStartedItem"})
String getStarted2();

@Parameter(name = "gettingstarted3", displayName = "Getting Started 3")
@JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:gettingStartedItem"})
String getStarted3();
	
}
