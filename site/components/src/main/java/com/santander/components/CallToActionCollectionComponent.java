package com.santander.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.hippoecm.repository.util.JcrUtils;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Strings;
import com.santander.beans.CallToActionButton;
import com.santander.beans.CallToActionCollection;
import com.santander.beans.CallToActionItem;
import com.santander.components.model.CallToActionButtonDTO;
import com.santander.components.model.CallToActionItemDTO;
import com.santander.dto.GenericDAOResource;
import com.santander.utils.Constants;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;

import static com.santander.utils.Constants.COMPONENT_CTA_BUTTON;

@ParametersInfo(type = CallToActionCollectionComponentInfo.class)
public class CallToActionCollectionComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(CallToActionCollectionComponent.class);
  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    try {

      logger.debug("****************************** Invoke CallToActionCollectionComponent.java");

      super.doBeforeRender(request, response);
      InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
      GenericDAOResource genericDAOResource = new GenericDAOResource();

      CallToActionCollectionComponentInfo callToActionCollectionComponentInfo =
          getComponentParametersInfo(request);

      String callToActionCollection =
          callToActionCollectionComponentInfo.getCallToActionCollection();

      if (!Strings.isNullOrEmpty(callToActionCollection)) {

        CallToActionCollection object1 = (CallToActionCollection) request.getRequestContext()
            .getObjectBeanManager().getObject(callToActionCollection);

        //Necesario para preview
        request.setModel("document", object1);
        request.setModel("icon", object1.getIcon());
        request.setModel("title", object1.getTitle());
        request.setModel("lookAndFeel", object1.getLookandfeel());
        /*request.setModel("description",
            (new GenericDAOResource()).rewriteHstRichContent(object1.getDescription().getContent(),
                object1.getDescription().getNode(),
                RequestContextProvider.get().getResolvedMount().getMount()));*/

        CallToActionButton ctaButton = object1.getCallToActionButton();
        generateComponent(request, ctaButton,  internalLinkProcessor, genericDAOResource);

        //updated 14/04/2021
        List<CallToActionItemDTO> CallToAtionItemList= new ArrayList<CallToActionItemDTO>();

        for (CallToActionItem item : object1.getCallToActionItem()) {
          processCallToActionItem(request, CallToAtionItemList, item,  internalLinkProcessor, genericDAOResource);

        }
        request.setModel("documentItemList",CallToAtionItemList);

      }

    } catch (ObjectBeanManagerException e) {
    	logger.error("",e);
    }
  }
  private void generateComponent(HstRequest request, CallToActionButton ctaButton, InternalLinkProcessor internalLinkProcessor,GenericDAOResource genericDAOResource) {

    try {
      if (ctaButton != null && ctaButton.getInternalLink() != null) {
        Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURL(request, ctaButton.getInternalLink());
        NodeType primaryType = JcrUtils.getPrimaryNodeType(ctaButton.getInternalLink().getNode());
        String parentProductId = "";
        String version = "";
        if (primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)) {
          parentProductId = getParentProductId(request, ctaButton, parentProductId);
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());

        } else if(primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)){
          parentProductId = getApiContainerId(request, ctaButton, parentProductId);
          version = InternalLinkProcessor.getVersion(ctaButton.getInternalLink().getCanonicalUUID(), request.getRequestContext());
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .version(version).channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());
        } else {
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());
        }
      } else {
        if(ctaButton != null) {
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder()
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .externalLink(ctaButton.getExternalLink())
                          .tab(ctaButton.getTab())
                          .build());
        }
      }
    } catch (RepositoryException | QueryException e) {
      logger.error("Error: ", e);
    }
  }

  private void processCallToActionItem(HstRequest request, List<CallToActionItemDTO> CallToAtionItemList,
                                       CallToActionItem item, InternalLinkProcessor internalLinkProcessor,GenericDAOResource genericDAOResource) {
    try {
    	//logger.info("******processCallToActionItem******");
    	//logger.info("item.getInternalLink():"+item.getInternalLink());
     

      if (item.getInternalLink() != null) {

        Map<String, String> urlAttrs =
                HippoUtils.getDocumentAttrsByURL(request, item.getInternalLink());

        NodeType primaryType = JcrUtils.getPrimaryNodeType(item.getInternalLink().getNode());

        // NUEVO.APIS
        String parentProductId = "";
        String version = "";
        if (primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)) {

          parentProductId = getParentProductIdCTAItem(request, item, parentProductId);

          CallToAtionItemList.add(
                  CallToActionItemDTO.builder()
                          .description((new GenericDAOResource()).rewriteHstRichContent(
                                 item.getDescription().getContent(), item.getDescription().getNode(),
                                  request.getRequestContext().getResolvedMount().getMount()))
                          .title(item.getTitle()).icon(item.getIcon()).name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .tab(item.getTab())
                          .author(item.getAuthor())
                          .iconAuthor(item.getIconAuthor())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                          .build());

        } else if(primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)) {

          parentProductId = getApiContainerIdCTAItem(request, item, parentProductId);
          version = InternalLinkProcessor.getVersion(item.getInternalLink().getCanonicalUUID(), request.getRequestContext());
          CallToAtionItemList.add(
                  CallToActionItemDTO.builder()
                          .description((new GenericDAOResource()).rewriteHstRichContent(
                                 item.getDescription().getContent(), item.getDescription().getNode(),
                                 request.getRequestContext().getResolvedMount().getMount()))
                          .title(item.getTitle()).icon(item.getIcon()).name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .version(version)
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .tab(item.getTab())
                          .author(item.getAuthor())
                          .iconAuthor(item.getIconAuthor())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                          .build());

        }else{
        	 //logger.info("item.getInternalLink()" , item.getInternalLink());
        	 //logger.info("item.getCanonicalUUID()" , item.getCanonicalUUID());
        	 //logger.info("generateHstLink(item.getInternalLink())", genericDAOResource.generateHstLink(item.getInternalLink()));
        	        	
          CallToAtionItemList.add(
                  CallToActionItemDTO.builder()
                          .description((new GenericDAOResource()).rewriteHstRichContent(
                               item.getDescription().getContent(), item.getDescription().getNode(),
                               request.getRequestContext().getResolvedMount().getMount()))
                          .title(item.getTitle()).icon(item.getIcon()).name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .tab(item.getTab())
                          .author(item.getAuthor())
                          .iconAuthor(item.getIconAuthor())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                          .build());
        }

      } else {
        CallToAtionItemList.add(
                CallToActionItemDTO.builder()
                        .description((new GenericDAOResource()).rewriteHstRichContent(
                                 item.getDescription().getContent(), item.getDescription().getNode(),
                                 request.getRequestContext().getResolvedMount().getMount()))
                        .title(item.getTitle()).icon(item.getIcon())
                        .externalLink(item.getExternalLink())
                        .tab(item.getTab())
                        .author(item.getAuthor())
                        .iconAuthor(item.getIconAuthor())
                        .build());
      }

    } catch (RepositoryException | QueryException e) {
      logger.error("",e);
    }
  }

  private String getParentProductId(HstRequest request, CallToActionButton ctaButton, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiProductRelatedDocuments(ctaButton.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error("Error executing query: ", e);
    }
    return parentProductId;
  }

  private String getApiContainerId(HstRequest request, CallToActionButton ctaButton, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiItemRelatedDocuments(ctaButton.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error("Error executing query: ", e);
    }
    return parentProductId;
  }

  private String getParentProductIdCTAItem(HstRequest request, CallToActionItem item, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiProductRelatedDocuments(item.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error("Error executing query: ", e);
    }
    return parentProductId;
  }

  private String getApiContainerIdCTAItem(HstRequest request, CallToActionItem item, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiItemRelatedDocuments(item.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error("Error executing query: ", e);
    }
    return parentProductId;
  }

}
