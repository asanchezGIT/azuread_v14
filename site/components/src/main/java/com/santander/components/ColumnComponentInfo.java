package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface ColumnComponentInfo {

  @Parameter(name = "document", displayName = "Column Component Document")
  @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:ColumnComponent"})
  String getResourceDocument();

}
