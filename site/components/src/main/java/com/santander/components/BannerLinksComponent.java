package com.santander.components;

import com.santander.beans.*;
import com.santander.components.model.LinksAndDownloadsDTO;
import com.santander.utils.Constants;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.hippoecm.repository.util.JcrUtils;
import org.onehippo.cms7.essentials.components.CommonComponent;
import com.google.common.base.Strings;
import com.santander.dto.GenericDAOResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.santander.utils.Constants.COMPONENT_LINK_DOWNLOADS;

@ParametersInfo(type = BannerLinksComponentInfo.class)
public class BannerLinksComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(BannerLinksComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {

        try {
            super.doBeforeRender(request, response);
            InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
            GenericDAOResource genericDAOResource = new GenericDAOResource();
            BannerLinksComponentInfo bannerLinksComponentInfo = getComponentParametersInfo(request);
            String bannerLinksDocument = bannerLinksComponentInfo.getBannerLinks();
            if (!Strings.isNullOrEmpty(bannerLinksDocument)) {
                BannerLinks object1 = (BannerLinks) request.getRequestContext().getObjectBeanManager().getObject(bannerLinksDocument);

                //necesario para preview
                request.setModel("document", object1);

                request.setModel("title", object1.getTitle());
                request.setModel("description", object1.getDescription());

                List<LinksAndDownloadsDTO> linksLista= new ArrayList<LinksAndDownloadsDTO>();
                for (LinksAndDownloads item : object1.getLinksAndDownloads()) {
                    processLinksAndDownloads(linksLista, item, internalLinkProcessor, genericDAOResource);

                }
                request.setModel("links", linksLista);
            }
        } catch (ObjectBeanManagerException obme) {
            logger.error("", obme);
        }
    }


    private void processLinksAndDownloads(List<LinksAndDownloadsDTO> linksLista,
                                         LinksAndDownloads item, InternalLinkProcessor internalLinkProcessor,GenericDAOResource genericDAOResource) {
        try {

            if (item.getInternalLink() != null) {

                NodeType primaryType = JcrUtils.getPrimaryNodeType(item.getInternalLink().getNode());

                        linksLista.add(
                        LinksAndDownloadsDTO.builder()
                                .externalLink(item.getExternalLink())
                                .title(item.getTitle())
                                .tab(item.getTab())
                                .internalLink(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                                .build());



            } else {

                        linksLista.add(
                                LinksAndDownloadsDTO.builder()
                                        .externalLink(item.getExternalLink())
                                        .title(item.getTitle())
                                        .tab(item.getTab())
                                        .internalLink(null)
                                        .build());
            }

        } catch (RepositoryException | QueryException e) {
            logger.error("",e);
        }
    }
}
