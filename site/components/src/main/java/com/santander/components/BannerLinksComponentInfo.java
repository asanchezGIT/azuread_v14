package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface BannerLinksComponentInfo {

    @Parameter(name = "bannerLinksComponentDocument", displayName = "Banner Links Component Document")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:BannerLinks"})
    String getBannerLinks();

}
