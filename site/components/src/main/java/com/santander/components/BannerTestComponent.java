package com.santander.components;

import com.google.common.base.Strings;
import com.santander.beans.ApiItem;
import com.santander.beans.BannerDocument;
import com.santander.beans.BannerDocumentTest;
import com.santander.beans.CallToActionButton;
import com.santander.components.model.CallToActionButtonDTO;
import com.santander.dto.GenericDAOResource;
import com.santander.utils.Constants;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.standard.HippoDocumentBean;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.repository.util.JcrUtils;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import java.util.Map;
import java.util.Objects;

import static com.santander.utils.Constants.COMPONENT_CTA_BUTTON;

@ParametersInfo(type = BannerTestComponentInfo.class)
public class BannerTestComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(BannerTestComponentInfo.class);

  public void doBeforeRender(HstRequest request, HstResponse response) {
      super.doBeforeRender(request,response);
      try {
          
    	  super.doBeforeRender(request, response);
    	  BannerTestComponentInfo bannerComponentInfo = getComponentParametersInfo(request);
          String bannerComponentInfoDocument = bannerComponentInfo.getBannerTest();

          if (!Strings.isNullOrEmpty(bannerComponentInfoDocument)) {
              Object object = request.getRequestContext().getObjectBeanManager().getObject(bannerComponentInfoDocument);
              request.setModel("document", object);
          }
    	  
      } catch (ObjectBeanManagerException obme) {
          obme.printStackTrace();
      }
  }
 

 
}
