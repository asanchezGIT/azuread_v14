package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;


public interface BannerTestComponentInfo {

	 @Parameter(name="nammerTest", displayName = "BannerTest")
	    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:bannerdocumentTest"})
	    String getBannerTest();
	
}


