package com.santander.components;

import com.google.common.base.Strings;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = InformationComponentInfo.class)
public class InformationComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(InformationComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {
        try {
            super.doBeforeRender(request, response);
            InformationComponentInfo informationComponentInfo = getComponentParametersInfo(request);
            String getInformation1path = informationComponentInfo.getInformation1();
            String getInformation2path = informationComponentInfo.getInformation2();
            String getInformation3path = informationComponentInfo.getInformation3();
            if (!Strings.isNullOrEmpty(getInformation1path)) {
                Object object1 = request.getRequestContext().getObjectBeanManager().getObject(getInformation1path);
                request.setModel("document1", object1);
            }
            if (!Strings.isNullOrEmpty(getInformation2path)) {
                Object object2 = request.getRequestContext().getObjectBeanManager().getObject(getInformation2path);
                request.setModel("document2", object2);
            }
            if (!Strings.isNullOrEmpty(getInformation3path)) {
                Object object3 = request.getRequestContext().getObjectBeanManager().getObject(getInformation3path);
                request.setModel("document3", object3);
            }
        } catch (ObjectBeanManagerException e) {
            logger.error("Error: {}", e.getMessage());
        }
    }

}