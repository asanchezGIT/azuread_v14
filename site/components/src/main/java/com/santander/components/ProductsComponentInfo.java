package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

public interface ProductsComponentInfo {


    @Parameter(name = "productBannerDescription", displayName = "Product Banner Description")
    String getProductBannerDescription();

    @Parameter(name = "product1", displayName = "Product Document 1")
    @JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:ProductBannerItem"})
    String getProduct1();

    @Parameter(name = "product2", displayName = "Product Document 2")
    @JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:ProductBannerItem"})
    String getProduct2();

    @Parameter(name = "product3", displayName = "Product Document 3")
    @JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:ProductBannerItem"})
    String getProduct3();

}
