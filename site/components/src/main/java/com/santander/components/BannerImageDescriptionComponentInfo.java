package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface BannerImageDescriptionComponentInfo {

  @Parameter(name = "bannerImageDescriptionDocument", displayName = "Banner Image Description Document")
  @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:BannerImageDescription"})
  String getBannerImageDescription();

}
