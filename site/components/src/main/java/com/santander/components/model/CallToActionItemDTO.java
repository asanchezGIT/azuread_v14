package com.santander.components.model;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Produces({MediaType.APPLICATION_JSON})
public class CallToActionItemDTO {
  String name;
  String internalLinkId;
  String channel;
  String locale;
  String externalLink;
  String title;
  String description;
  HippoGalleryImageSet icon;
  String tab;
  String parentProductId;
  String version;
  String author;
  String url;
  HippoGalleryImageSet iconAuthor;
  HippoGalleryImageSet imageBackground;
}
