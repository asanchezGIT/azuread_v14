package com.santander.components;

import com.santander.beans.CallToActionButton;
import com.santander.components.model.CallToActionButtonDTO;
import com.santander.utils.Constants;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.hippoecm.repository.util.JcrUtils;
import org.onehippo.cms7.essentials.components.CommonComponent;
import com.google.common.base.Strings;
import com.santander.beans.BannerImageDescription;
import com.santander.dto.GenericDAOResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import java.util.Map;

import static com.santander.utils.Constants.COMPONENT_CTA_BUTTON;

@ParametersInfo(type = BannerImageDescriptionComponentInfo.class)
public class BannerImageDescriptionComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(BannerImageDescriptionComponent.class);

  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    
    try {
      super.doBeforeRender(request, response);
      InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
      GenericDAOResource genericDAOResource = new GenericDAOResource();
      BannerImageDescriptionComponentInfo bannerImageDescriptionComponentInfo = getComponentParametersInfo(request);
      String bannerImageDescriptionDocument = bannerImageDescriptionComponentInfo.getBannerImageDescription();
      if (!Strings.isNullOrEmpty(bannerImageDescriptionDocument)) {
        BannerImageDescription object1 = (BannerImageDescription) request.getRequestContext().getObjectBeanManager().getObject(bannerImageDescriptionDocument);

        //necesario para preview
        request.setModel("document", object1);

        request.setModel("bannerImageDescriptionDocument", object1);
        CallToActionButton ctaButton = object1.getCallToActionButton();
        generateComponent(request, ctaButton,  internalLinkProcessor, genericDAOResource);

        /*String fixDescription = (new GenericDAOResource()).rewriteHstRichContent(object2.getDescription().getContent()
        		, object2.getDescription().getNode(), RequestContextProvider.get().getResolvedMount().getMount());
        request.setModel("description", fixDescription);*/
      }
    } catch (ObjectBeanManagerException obme) {
      logger.error("", obme);
    }
  }

  private void generateComponent(HstRequest request, CallToActionButton ctaButton, InternalLinkProcessor internalLinkProcessor, GenericDAOResource genericDAOResource) {
    try {
      if (ctaButton != null && ctaButton.getInternalLink() != null) {
        Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURL(request, ctaButton.getInternalLink());
        NodeType primaryType = JcrUtils.getPrimaryNodeType(ctaButton.getInternalLink().getNode());
        String parentProductId = "";
        String version = "";
        if (primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)) {
          parentProductId = getParentProductId(request, ctaButton, parentProductId);
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());

        } else if(primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)){
          parentProductId = getApiContainerId(request, ctaButton, parentProductId);
          version = InternalLinkProcessor.getVersion(ctaButton.getInternalLink().getCanonicalUUID(), request.getRequestContext());
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .version(version).channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());
        } else {
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());
        }
      } else {
        if(ctaButton != null) {
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder()
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .externalLink(ctaButton.getExternalLink())
                          .tab(ctaButton.getTab())
                          .build());
        }
      }
    } catch (RepositoryException | QueryException e) {
      logger.error("Error: ", e);
    }
  }
  private String getParentProductId(HstRequest request, CallToActionButton ctaButton, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiProductRelatedDocuments(ctaButton.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error("Error executing query: ", e);
    }
    return parentProductId;
  }
  private String getApiContainerId(HstRequest request, CallToActionButton ctaButton, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiItemRelatedDocuments(ctaButton.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error("Error executing query: ", e);
    }
    return parentProductId;
  }
}
