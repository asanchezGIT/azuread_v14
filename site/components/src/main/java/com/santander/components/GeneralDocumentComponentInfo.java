package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface GeneralDocumentComponentInfo {

    @Parameter(name = "generalDocument", displayName = "General Document")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:GeneralDocument"})
    String getGeneralDocument();
}

