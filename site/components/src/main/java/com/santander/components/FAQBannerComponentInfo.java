package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface FAQBannerComponentInfo {
	
    @Parameter(name = "resourceDocument", displayName = "Document FAQBanner")
    @JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:FAQBanner"})
    String getFAQBanner();
}
