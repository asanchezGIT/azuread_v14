package com.santander.components;


import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import com.google.common.base.Strings;
import com.santander.beans.ListDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = ListDescriptionComponentInfo.class)
public class ListDescriptionComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(ListDescriptionComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {
    	  try {
    	      super.doBeforeRender(request, response);
    	      ListDescriptionComponentInfo listDescriptionInfo = getComponentParametersInfo(request);
    	      String listDescriptionDocument = listDescriptionInfo.getListDescription();
    	      if (!Strings.isNullOrEmpty(listDescriptionDocument)) {
    	        ListDescription object1 = (ListDescription) request.getRequestContext().getObjectBeanManager()
    	        		.getObject(listDescriptionDocument);
    	        
    	        
    	        //necesario para preview
    	        request.setModel("document", object1);
    	        
    	      
    	      }
    	    } catch (ObjectBeanManagerException e) {
    	      logger.error("exception ListDescriptionComponent {}", e.getMessage());
    	    }
    }


}








