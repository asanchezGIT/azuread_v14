package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface CallToActionCollectionProductsComponentInfo {

    @Parameter(name = "callToActionCollectionProducts", displayName = "Call to Action Collection Products")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:CallToActionCollectionProducts"})
    String getCallToActionCollectionProducts();
}

