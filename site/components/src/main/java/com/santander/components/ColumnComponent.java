package com.santander.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;

import com.santander.beans.CallToActionButton;
import com.santander.utils.InternalLinkProcessor;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.hippoecm.repository.util.JcrUtils;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.santander.beans.CallToActionItem;
import com.santander.beans.ColumnComponentBean;
import com.santander.components.model.CallToActionItemDTO;
import com.santander.dto.GenericDAOResource;
import com.santander.utils.Constants;
import com.santander.utils.HippoUtils;

@ParametersInfo(type = ColumnComponentInfo.class)
public class ColumnComponent extends CommonComponent {

	public static final Logger logger = LoggerFactory.getLogger(ColumnComponent.class);

@Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
   
    try {

      super.doBeforeRender(request, response);
		InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
		GenericDAOResource genericDAOResource = new GenericDAOResource();
      ColumnComponentInfo columnComponentInfo = getComponentParametersInfo(request);
      String columnComponent = columnComponentInfo.getResourceDocument();

      if (!Strings.isNullOrEmpty(columnComponent)) {

        ColumnComponentBean object1 = (ColumnComponentBean) request.getRequestContext()
            .getObjectBeanManager().getObject(columnComponent);
        
        //necesario para preview
        request.setModel("document", object1);
        

        request.setModel("title", object1.getTitle());
        /*request.setModel("description",
            (new GenericDAOResource()).rewriteHstRichContent(object1.getDescription().getContent(),
                object1.getDescription().getNode(),
                RequestContextProvider.get().getResolvedMount().getMount()));*/
        request.setModel("imageBackground", object1.getImageBackground());

        ColumnComponentBean columnComponentBean = (ColumnComponentBean) request.getRequestContext()
            .getObjectBeanManager().getObject(columnComponent);

        //updated 15/01/2021--> updated 14/04/2021
        List<CallToActionItemDTO> CallToAtionItemList= new ArrayList<CallToActionItemDTO>();
        
        for (CallToActionItem item : columnComponentBean.getCallToActionItem()) {
          processCallToActionItem(request, CallToAtionItemList, item, internalLinkProcessor, genericDAOResource);

        }
        request.setModel("documentItemList",CallToAtionItemList);
        
      }
    } catch (ObjectBeanManagerException obme) {
      logger.error("",obme);
    }
  }

	private void processCallToActionItem(HstRequest request, List<CallToActionItemDTO> CallToAtionItemList,
										 CallToActionItem item, InternalLinkProcessor internalLinkProcessor,GenericDAOResource genericDAOResource) {
		try {

			if (item.getInternalLink() != null) {

				Map<String, String> urlAttrs =
						HippoUtils.getDocumentAttrsByURL(request, item.getInternalLink());

				NodeType primaryType = JcrUtils.getPrimaryNodeType(item.getInternalLink().getNode());

				// NUEVO.APIS
				String parentProductId = "";
				String version = "";
				if (primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)) {

					parentProductId = getParentProductIdCTAItem(request, item, parentProductId);

					CallToAtionItemList.add(
							CallToActionItemDTO.builder()
									.description((new GenericDAOResource()).rewriteHstRichContent(
                                           item.getDescription().getContent(), item.getDescription().getNode(),
                                           request.getRequestContext().getResolvedMount().getMount()))
									.title(item.getTitle()).icon(item.getIcon()).name(primaryType.getName())
									.internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
									.parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
									.channel(urlAttrs.get("channel"))
									.locale(urlAttrs.get("locale"))
									.tab(item.getTab())
									.author(item.getAuthor())
									.iconAuthor(item.getIconAuthor())
									.url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
									.build());

				} else if(primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)) {

					parentProductId = getApiContainerIdCTAItem(request, item, parentProductId);
					version = InternalLinkProcessor.getVersion(item.getInternalLink().getCanonicalUUID(), request.getRequestContext());
					CallToAtionItemList.add(
							CallToActionItemDTO.builder()
									.description((new GenericDAOResource()).rewriteHstRichContent(
                                           item.getDescription().getContent(), item.getDescription().getNode(),
                                           request.getRequestContext().getResolvedMount().getMount()))
									.title(item.getTitle()).icon(item.getIcon()).name(primaryType.getName())
									.internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
									.parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
									.version(version)
									.channel(urlAttrs.get("channel"))
									.locale(urlAttrs.get("locale"))
									.tab(item.getTab())
									.author(item.getAuthor())
									.iconAuthor(item.getIconAuthor())
									.url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
									.build());

				}else{
					CallToAtionItemList.add(
							CallToActionItemDTO.builder()
									.description((new GenericDAOResource()).rewriteHstRichContent(
                                         item.getDescription().getContent(), item.getDescription().getNode(),
                                         request.getRequestContext().getResolvedMount().getMount()))
									.title(item.getTitle()).icon(item.getIcon()).name(primaryType.getName())
									.internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
									.channel(urlAttrs.get("channel"))
									.locale(urlAttrs.get("locale"))
									.tab(item.getTab())
									.author(item.getAuthor())
									.iconAuthor(item.getIconAuthor())
									.url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
									.build());
				}

			} else {
				CallToAtionItemList.add(
						CallToActionItemDTO.builder()
								.description((new GenericDAOResource()).rewriteHstRichContent(
                                         item.getDescription().getContent(), item.getDescription().getNode(),
                                         request.getRequestContext().getResolvedMount().getMount()))
								.title(item.getTitle()).icon(item.getIcon())
								.externalLink(item.getExternalLink())
								.tab(item.getTab())
								.author(item.getAuthor())
								.iconAuthor(item.getIconAuthor())
								.build());
			}

		} catch (RepositoryException | QueryException e) {
			logger.error("",e);
		}
	}

	private String getParentProductIdCTAItem(HstRequest request, CallToActionItem item, String parentProductId) {
		try {
			parentProductId = HippoUtils.getApiProductRelatedDocuments(item.getInternalLink()
					.getCanonicalUUID(), request.getRequestContext());
		} catch (QueryException e) {
			logger.error("Error executing query: ", e);
		}
		return parentProductId;
	}

	private String getApiContainerIdCTAItem(HstRequest request, CallToActionItem item, String parentProductId) {
		try {
			parentProductId = HippoUtils.getApiItemRelatedDocuments(item.getInternalLink()
					.getCanonicalUUID(), request.getRequestContext());
		} catch (QueryException e) {
			logger.error("Error executing query: ", e);
		}
		return parentProductId;
	}


}
