package com.santander.components.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.santander.beans.DocumentsGroup;
import com.santander.beans.SubMenuDocumentationLevelOne;
import com.santander.dto.GenericDAOResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubMenuDocumentationDTO extends GenericDAOResource{
 
  private String title;	
  private List<DocumentsGroupDTO> documentsGroups;
 
  
  private static Logger logger = LoggerFactory.getLogger(SubMenuDocumentationDTO.class);
  
  public SubMenuDocumentationDTO(SubMenuDocumentationLevelOne subMenuDocumentation)  {
	  
	  //title
	  if(Objects.nonNull(subMenuDocumentation.getTitle())){
		  this.title=subMenuDocumentation.getTitle();
	  }
	  else {
		  this.title=StringUtils.EMPTY;
	  }
	 
	  //documentsGroups
	  List<DocumentsGroupDTO> documentsGroupsDTO = new ArrayList<>(); 
	  /*
	  if (Objects.nonNull(subMenuDocumentation.getDocumentsGroups())) { 
		 
		 for (DocumentsGroup documentGroup :subMenuDocumentation.getDocumentsGroups()) {

	    	  if(Objects.nonNull(documentGroup.getDocuments())){
	    		  DocumentsGroupDTO documentsGroupDTO=new DocumentsGroupDTO(documentGroup);
	    		  documentsGroupsDTO.add(documentsGroupDTO);
	    	  
	    	  }
	          
	    }
	 }
	 */
	
       
	
	 this.documentsGroups=documentsGroupsDTO;
	 
		 
  }

}
	 
	 
