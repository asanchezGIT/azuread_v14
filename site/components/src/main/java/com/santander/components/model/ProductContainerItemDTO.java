package com.santander.components.model;

import java.util.ArrayList;
import java.util.List;

import java.util.Objects;
import java.util.regex.Pattern;


import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoBean;

import org.hippoecm.hst.core.request.HstRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.santander.beans.Documentation;
import com.santander.beans.ProductContainerItem;
import com.santander.dto.GenericDAOResource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductContainerItemDTO extends GenericDAOResource{
  private String title;
  private String description;
  //private List<DocumentationItemDTO> docList;
  
 // private HippoHtml descOverwiew;
  //private List <HippoBean> docList;
  
  private static Logger logger = LoggerFactory.getLogger(ProductContainerItemDTO.class);
  
  public ProductContainerItemDTO(ProductContainerItem productContainerItem)  {
	  
	 
	  HstRequestContext requestContext = RequestContextProvider.get();
	  
	  this.title=productContainerItem.getTitleCMS();
	  
	 if (Objects.nonNull(productContainerItem.getDescriptionOverview())) {
		this.description = rewriteHstRichContent(productContainerItem.getDescriptionOverview().getContent(),
				  productContainerItem.getDescriptionOverview().getNode(),
                  requestContext.getResolvedMount().getMount());
      }
	  
	 /*
	 //documentos destacados del producto
	 if (Objects.nonNull(productContainerItem.getDocumentationOrder())) { 
		 List <HippoBean> documentations= productContainerItem.getDocumentation();
		 List<DocumentationItemDTO> documentationsDTOList = new ArrayList<>();
		 Documentation documentation=null;
		 DocumentationItemDTO documentationItemDTO =null;
		 String[] documentationOrderList = productContainerItem.getDocumentationOrder().split(Pattern.quote(","));
	     int i=0;
	     while ( i<documentationOrderList.length) {
	   
	    	 //Elemento a coger
	    	 try {
		    	 int order=Integer.parseInt(documentationOrderList[i]);
		
		    	 if (order<=documentations.size()) {
		    		 documentation= (Documentation)documentations.get(order-1);
			    	 documentationItemDTO= new DocumentationItemDTO(documentation);
			    	 documentationsDTOList.add(documentationItemDTO);
		    	 }	 
	    	 } 
	    	 catch (Exception e) {
	    		 
	    	 }
	    	 i++;
	     }
	     
	     this.docList=documentationsDTOList;
		 
	 }
	 
	 */
  }
}
	 
	 