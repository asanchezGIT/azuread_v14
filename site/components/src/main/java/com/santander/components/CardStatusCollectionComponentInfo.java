package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface CardStatusCollectionComponentInfo {

  @Parameter(name = "cardStatusCollection", displayName = "Card Status Collection")
  @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:CardStatusCollection"})
  String getCardStatusCollection();

}

