package com.santander.dto;

import com.santander.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.rewriter.impl.SimpleContentRewriter;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.repository.util.JcrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.beans.LinksAndDownloads;
import com.santander.beans.CallToActionItemSimple;
import com.santander.beans.IntegrationProduct;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import io.jsonwebtoken.lang.Strings;

public class GenericDAOResource {

  private static final int INDEX = 3;

private static Logger logger = LoggerFactory.getLogger(GenericDAOResource.class);

  HstRequestContext hstRequestContext = RequestContextProvider.get();

  InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();

  public final String generateHstLink(HippoBean bean) {
    if (Objects.isNull(bean)) {
      return null;
    }
    return hstRequestContext.getHstLinkCreator().create(bean, hstRequestContext)
            .toUrlForm(hstRequestContext, true);
  }

  public final String generateHstImageLink(HippoGalleryImageSet hippoGalleryImageSet) {
    if (Objects.isNull(hippoGalleryImageSet)) {
      return null;
    }
    return convertToExternalURL(hstRequestContext.getHstLinkCreator()
            .create(hippoGalleryImageSet, hstRequestContext).toUrlForm(hstRequestContext, true));
  }

  public String getDocumentLinkWithUUID(HippoBean bean) {
    String uuid = "";
    String url = hstRequestContext.getHstLinkCreator().create(bean, hstRequestContext)
            .toUrlForm(hstRequestContext, true);
    int index = url.lastIndexOf('/');
    String result;
    try {
      Node node = hstRequestContext.getSession().getNode(bean.getPath());
      hstRequestContext.getSession();
      uuid = node.getIdentifier();
    } catch (Exception e) {
    	logger.error("error: ",e);
    }
    result = url.substring(0, index) + "/" + uuid;
    return StringUtils.replace(result, "/content/", "/").toLowerCase();
  }

  public String getDocumentUUID(HippoBean bean) {
    String uuid = "";

    try {
      Node node = hstRequestContext.getSession().getNode(bean.getPath());
      hstRequestContext.getSession();
      uuid = node.getIdentifier();
    } catch (Exception e) {
    	logger.error("",e);
    }
    return uuid;
  }

  public final String rewriteHstRichContent(String content, Node node, Mount mount) {
    
	  logger.debug("******METODO rewriteHstRichContent");
	  if (Objects.isNull(content)) {
      return StringUtils.EMPTY;
    }
    SimpleContentRewriter contentRewriter = new SimpleContentRewriter();
    String html = contentRewriter.rewrite(content, node, hstRequestContext, mount);
    html = internalLinkProcessor.processExternalLink(content, html);
    html = processInternalLinks(html);
    
    if (logger.isDebugEnabled())
    {
    	logger.debug("fixSrcPath(html): {}", fixSrcPath(html));
    }
    //return fixSrcPath(html);
    return html;
  }

  private String fixSrcPath(String html) {

	//COMPROBAR QUE NO HAY QUE USARLA
    
    String url = getRequestURL(hstRequestContext.getServletRequest().getRequestURL().toString());
    
    if (StringUtils.contains(url, "localhost") || StringUtils.contains(url, "dev.")) {
      logger.debug("Es localhost o dev");
      String path = "/site/binaries/content/";
      String fullPath = url + path;
      logger.debug("fullPath: {}" , fullPath);
      return Strings.replace(html, path, fullPath);
    } else {
      logger.debug("NO es localhost");
      return html;
    }

  }

  private String getRequestURL(String url) {
    String aux = url;
    int index = 0;
    int cont = 0;
    for (int i = 0; i < INDEX; i++) {
      index = aux.indexOf('/');
      aux = aux.substring(index + 1, aux.length());
      cont = cont + index + 1;
    }
    return url.substring(0, cont - 1);
  }

  /**
   * Converts the internal Bloomreach hostname to an external URL.
   *
   * @param url
   * @return
   */
  private String convertToExternalURL(String url) {
    logger.debug("Pre formatted Bloomreach internal asset URL: {}" , url);
    logger.debug("BRX_HOSTNAME: {}" , Constants.BRX_HOSTNAME);
    logger.debug("ASSET_HOSTNAME: {}" , Constants.ASSET_HOSTNAME);
    String externalURL = url;
    
    if (StringUtils.contains(url, Constants.BRX_HOSTNAME) && StringUtils.contains(Constants.BRX_HOSTNAME, "dev")) {
      logger.debug("DEV environment hostname found.");
      
    }
    if (StringUtils.contains(url, Constants.BRX_HOSTNAME)) {
      externalURL = StringUtils.replace(url, Constants.BRX_HOSTNAME, Constants.ASSET_HOSTNAME);
    }
    logger.debug("Post formatted external URL: {}" , externalURL);
    return externalURL;

  }

  private String processInternalLinks(String html) {
    try {
      String url;
      logger.debug("Pre processed HTML: {}" , html);
      if (html.contains("<a href")) 
      {
        html = setHtml(html);
      }
    } catch (Exception e) {
      logger.error("Error: ", e);
    }
    logger.debug("Post processed HTML: {}" , html);
    return html;
  }

private String setHtml(String html) throws QueryException {
	String url;
	logger.debug("Href found in internal link");
	Pattern p = Pattern.compile(Constants.PATTERN_INTERNAL_LINK);
	Matcher m = p.matcher(html);
	while (m.find()) {
	  if (!m.group(1).contains(Constants.BINARIES)) {
		// this variable should contain the link URL
		  url = m.group(1); 
	    logger.info("Extracted URL is: {}" , url);
	    if ((url.contains(Constants.RESOURCEAPI) || url.contains(Constants.CUSTOMAPI)) && !url.contains("pagenotfound")) {
	      String contentUUID = internalLinkProcessor.getInternalLinkUUID(internalLinkProcessor.translateInternalLinkURLToContentPath(url));
	      String contentType = internalLinkProcessor.getInternalLinkDocumentType(contentUUID);
	      html = html.replace(url,internalLinkProcessor.generateInternalLink(url, contentType, contentUUID));
	    }
	  }
	}
	return html;
}

public final List<IntegrationProductDTO> processIntegrationsProduct(HstRequestContext requestContext,List<IntegrationProduct> integrationProducts) {

    logger.debug("*****INICIO METODO  processIntegrationsProduct");
    List<IntegrationProductDTO> integrationProductsList = new ArrayList<>();
    
    
    if (integrationProducts.size()!=0) {
	    for (IntegrationProduct integrationProduct : integrationProducts) {
	    
	      try {
		    	
		        processIntegrationProduct(requestContext,integrationProductsList, integrationProduct);
	      } catch (RepositoryException | QueryException e) {
	    	  logger.error("Error proccessing processIntegrationsProduct.", e);
	      } catch (Exception e) {
		        logger.error("Error General proccessing processIntegrationsProduct.", e);
		      }
	    }
    }
   
    logger.debug("*****FIN METODO  processIntegrationsProduct");
    return integrationProductsList;
  }


private void processIntegrationProduct(HstRequestContext requestContext,List<IntegrationProductDTO> IntegrationProductList,
		IntegrationProduct integrationProduct) throws RepositoryException, QueryException {
	
	
	    logger.debug("*****INICIO processIntegrationProduct");
	    
	    List<CallToActionItemSimple> items=integrationProduct.getItems();
	    List<CallToActionItemSimpleDTO> itemsDTO=new ArrayList<>();
	    if (items.size()!=0) {
		    for (CallToActionItemSimple item : items) {
		    	
		    	CallToActionItemSimpleDTO itemDTO=new CallToActionItemSimpleDTO(requestContext,item);
		    	itemsDTO.add(itemDTO);
			    
		      }
		}
	  
	    String iconPath=Objects.nonNull(integrationProduct.getIcon())? generateHstImageLink(integrationProduct.getIcon()) : StringUtils.EMPTY;
	   
	    IntegrationProductList.add(IntegrationProductDTO.builder().title(integrationProduct.getTitle()).icon(iconPath).items(itemsDTO).build());
	    logger.debug("*****FIN processIntegrationProduct");
	 

}


public final List<LinkDTO> processlinksAndDownloads(List<LinksAndDownloads> links) {

    logger.debug("*****INICIO METODO  processlinksAndDownloads");
    List<LinkDTO> linksAndDownloads = new ArrayList<>();
    InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();

    for (LinksAndDownloads link : links) {
      logger.debug("*****Entro en el blucle de Links");
      try 
      {
        processInternalLinks(linksAndDownloads, internalLinkProcessor, link);
      } catch (RepositoryException | QueryException e) {
        logger.error("Error proccessing Link&Downloads.", e);
      }
    }
    return linksAndDownloads;
  }


private void processInternalLinks(List<LinkDTO> linksAndDownloads, InternalLinkProcessor internalLinkProcessor,
	LinksAndDownloads link) throws RepositoryException, QueryException {
if (Objects.nonNull(link.getInternalLink())) {

  logger.debug("*****Nombre del Link: {}" , link.getInternalLink());
  // Manage Internal link
  NodeType primaryType = JcrUtils.getPrimaryNodeType(link.getInternalLink().getNode());
  String version = InternalLinkProcessor.getVersion(link.getInternalLink().getCanonicalUUID(), hstRequestContext);
  if (primaryType.getName().contains(Constants.DOC_TYPE_NEWS_AND_ARTICLES)
          || primaryType.getName().contains(Constants.DOC_TYPE_GETTINGSTARTED)
          || primaryType.getName().contains(Constants.DOC_TYPE_PRODUCT)
          || primaryType.getName().contains(Constants.DOC_TYPE_DOCUMENTATION)
          || primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)
          || primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)) 
  {
    addLinksAndDownloads(version, linksAndDownloads, internalLinkProcessor, link, primaryType);
  } else {
    logger.debug("*****Es un Link a otro tipo de contenido interno");
    // Generic internal link
    linksAndDownloads
            .add(LinkDTO.builder().title(link.getTitle()).name(primaryType.getName())
                    .url(generateHstLink(link.getInternalLink())).version(version).tab(link.getTab()).build());
  }
} else {
  // Manage external link
  manageExternalLink(linksAndDownloads, link);
}
}


private void manageExternalLink(List<LinkDTO> linksAndDownloads, LinksAndDownloads link) {
	if (Objects.nonNull(link.getTitle()) && !link.getTitle().isEmpty()
	          && Objects.nonNull(link.getExternalLink()) && !link.getExternalLink().isEmpty()) {
	    linksAndDownloads.add(LinkDTO.builder().title(link.getTitle())
	            .url(link.getExternalLink()).tab(link.getTab()).build());
	  }
}

private void addLinksAndDownloads(String version, List<LinkDTO> linksAndDownloads, InternalLinkProcessor internalLinkProcessor,
		LinksAndDownloads link, NodeType primaryType) throws QueryException {
	logger.debug(
	        "*****Es un Link a los tipos que tenemos: Articles,getStarted,product"
	        + ",documentation,api product o apiItem");
	//Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURLCustom(hstRequestContext, link.getInternalLink());
	Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURLCustom(link.getInternalLink());
	
	
	String parentProductId = "";

    if (primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)) {
        logger.debug("*****Es un Link ApiContainer");
        parentProductId = setProductContainerParentId(link, parentProductId);
    }

	if (primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)) {
        logger.debug("*****Es un Link ApiItem");
        parentProductId = setApiContainerParentId(link, parentProductId);
	}


	logger.debug("*****Empiezo a rellenar el linksAndDownload con el campo de ese Link ");


	linksAndDownloads
	        .add(LinkDTO.builder().title(link.getTitle()).name(primaryType.getName())
                    .internalLinkId(InternalLinkProcessor.getAliasByUuid(link.getInternalLink().getCanonicalUUID()))
	                .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                    .version(version)
	                .url(internalLinkProcessor.generateInternalLink(
	                        generateHstLink(link.getInternalLink()), primaryType.getName(),
	                        link.getInternalLink().getCanonicalUUID()))
	                .channel(urlAttrs.get("channel")).locale(urlAttrs.get("locale"))
	                .tab(link.getTab()).build());

	logger.debug("*****DESPUES DE rellenar el linksAndDownload con el campo de ese Link ");
}

private String setApiContainerParentId(LinksAndDownloads link, String parentProductId) {
	try {
	    parentProductId = HippoUtils.getApiItemRelatedDocuments(link.getInternalLink().getCanonicalUUID(), hstRequestContext);
	  } catch (QueryException e) {
	    logger.error("Error retriving parentProductId.", e);
	  }
	return parentProductId;
}

    private String setProductContainerParentId(LinksAndDownloads link, String parentProductId) {
        try {
            parentProductId = HippoUtils.getApiProductRelatedDocuments(link.getInternalLink().getCanonicalUUID(),hstRequestContext);
        } catch (QueryException e) {
            logger.error("Error executing query: ", e);
        }
        return parentProductId;
    }


}

