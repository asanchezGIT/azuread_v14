package com.santander.dto;

import java.util.Map;
import com.santander.beans.GettingStartingPage;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import lombok.Data;


@Data
public class GetStartedListDTO extends GenericDAOResource {

  private String id;
  private String title;
  private String littleIcon;
  private DescriptionDTO description;
  private DescriptionDTO shortDescription;
  private String channel;
  private String locale;


  public GetStartedListDTO(GettingStartingPage getStartedPage) {
    this.id = InternalLinkProcessor.getAliasByUuid(getStartedPage.getCanonicalUUID());
    if (getStartedPage.getTitle() != null) {
      this.title = getStartedPage.getTitle();
    }
    if (getStartedPage.getLittleIcon() != null) {
      this.littleIcon = generateHstImageLink(getStartedPage.getLittleIcon());
    }
    if (getStartedPage.getDescription() != null) {
      this.description = DescriptionDTO.builder()
              .content(rewriteHstRichContent(getStartedPage.getDescription().getContent(),
                      getStartedPage.getDescription().getNode(),
                      hstRequestContext.getResolvedMount().getMount()))
              .build();
    }
    if (getStartedPage.getShortDescription() != null) {
     this.shortDescription = DescriptionDTO.builder()
          .content(rewriteHstRichContent(getStartedPage.getShortDescription().getContent(),
              getStartedPage.getShortDescription().getNode(),
              hstRequestContext.getResolvedMount().getMount()))
          .build();
    }
    //Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURLCustom(hstRequestContext, getStartedPage);
    Map<String, String> urlAttrs =
            HippoUtils.getDocumentAttrsByURLCustom(getStartedPage);
    
    this.channel = urlAttrs.get("channel");
    this.locale = urlAttrs.get("locale");
  }

}
