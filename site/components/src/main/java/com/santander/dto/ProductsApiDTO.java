package com.santander.dto;


import com.santander.beans.ProductsApi;
import lombok.Data;

@Data
public class ProductsApiDTO {
  
  private String id;
  private String exposure;
  private String type;
  private String mode;
  private String deprecated;
  
  public   ProductsApiDTO(ProductsApi productApi) {
  this.id=productApi.getId();
  this.exposure=productApi.getExposure();
  this.mode=productApi.getMode();
  this.type=productApi.getType();
  this.deprecated=productApi.getDeprecated();
  }

}
