package com.santander.dto;

import java.util.List;

import org.onehippo.cms7.essentials.components.rest.BaseRestResource;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetStartedsDTO extends BaseRestResource {

    List <HALLinkDTO> _links;
    int _count;
    List <GetStartedListDTO> _embedded;
    
}