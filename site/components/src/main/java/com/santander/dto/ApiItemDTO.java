package com.santander.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.core.request.HstRequestContext;
import com.santander.beans.ApiItem;
import com.santander.beans.Documentation;
import com.santander.beans.ProductsApi;
import com.santander.beans.SectionLevelOne;
import com.santander.beans.SectionLevelThree;
import com.santander.beans.SectionLevelTwo;
import com.santander.beans.LocalCountry;
import com.santander.beans.ModeVersionApiItem;
import com.santander.utils.HippoUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
  
public class ApiItemDTO extends GenericDAOResource implements Comparator<ApiItemDTO>{

    private static Logger logger = LoggerFactory.getLogger(ApiItemDTO.class);


    String id;
    String idIL;
    String name;
    String title;
    String version;
    DescriptionDTO description;
    String status;
    String icon;
    String yaml;
    List<LinkDTO> links;
    LinkDTO postmanCollection;
    List<DocumentationDTO> documentation;
    String featuresOverview;
    String globalExposure;
    List<ProductsApiDTO> products;
    boolean deprecated;
    List<LocalCountryDTO> localCountries;
    String channel;
    String language;
    String updateDate;
    String basePath;
    String author;
    String authorProfile;
    String authorIcon;


    public ApiItemDTO(ApiItem apiItem) {

        HstRequestContext requestContext = RequestContextProvider.get();
        this.id = apiItem.getCanonicalUUID();

        if (Objects.nonNull(apiItem.getIdIL())) {
            this.idIL = apiItem.getIdIL();
        }


        this.name = Objects.nonNull(apiItem.getNameCMS()) && !apiItem.getNameCMS().isEmpty() 
        		? apiItem.getNameCMS() : apiItem.getNameIL();
        this.title = Objects.nonNull(apiItem.getTitleCMS()) && !apiItem.getTitleCMS().isEmpty() 
        		? apiItem.getTitleCMS() : apiItem.getTitleIL();
        this.version = Objects.nonNull(apiItem.getVersionIL()) && !apiItem.getVersionIL().isEmpty() 
        		? apiItem.getVersionIL() : apiItem.getVersionCMS();


        if (Objects.nonNull(apiItem.getStatus())) {
            this.status = apiItem.getStatus();
        }

        this.deprecated = Objects.nonNull(apiItem.getDeprecated()) && !apiItem.getDeprecated().isEmpty() 
        		&& "No".contentEquals(apiItem.getDeprecated()) ? false : true;


        //description
        //Map<String, String> urlAttrs =HippoUtils.getDocumentAttrsByURLCustom(hstRequestContext, apiItem);
        Map<String, String> urlAttrs =
                        HippoUtils.getDocumentAttrsByURLCustom(apiItem);



        if (Objects.nonNull(apiItem.getDescriptionCMS())) {
            this.description = DescriptionDTO.builder()
                    .content(rewriteHstRichContent(apiItem.getDescriptionCMS().getContent(),
                            apiItem.getDescriptionCMS().getNode(),
                            hstRequestContext.getResolvedMount().getMount()))
                    .build();
        } else if (Objects.nonNull(apiItem.getDescriptionIL())) {
           this.description = DescriptionDTO.builder()
                    .content(rewriteHstRichContent(apiItem.getDescriptionIL().getContent(),
                            apiItem.getDescriptionIL().getNode(),
                            hstRequestContext.getResolvedMount().getMount()))
                    .build();

        } else
        {
        	//Do nothing
        }


        if (apiItem.getIcon() != null) {
            this.icon = generateHstImageLink(apiItem.getIcon());
        }

        if (apiItem.getLinks() != null) {
            this.links = processlinksAndDownloads(apiItem.getLinks());

        }


        if (Objects.nonNull(apiItem.getYaml())) {
            this.yaml = generateHstLink(apiItem.getYaml());
        }

        if(Objects.nonNull(apiItem.getPostmanCollection())) {
        	 this.postmanCollection = LinkDTO.builder().title("Postman Collection")
        			 .url(generateHstLink(apiItem.getPostmanCollection())).build();
        }
       
        //feature
        if (Objects.nonNull(apiItem.getFeaturesOverview())) {
           this.featuresOverview = rewriteHstRichContent(apiItem.getFeaturesOverview().getContent(),
                    apiItem.getFeaturesOverview().getNode(),
                    requestContext.getResolvedMount().getMount());

        }

        if (Objects.nonNull(apiItem.getGlobalExposure())) {
            this.globalExposure = rewriteHstRichContent(apiItem.getGlobalExposure().getContent(),
                    apiItem.getGlobalExposure().getNode(),
                    requestContext.getResolvedMount().getMount());

        }

        
        List<ProductsApiDTO> productsApi = new ArrayList<>();
	      for (ProductsApi productApiBean : apiItem.getProducts()) {
	    	  
	    	  //Si el producto no está deprecado y tiene relleno alguno de sus campos
	    	  if ((Objects.nonNull( productApiBean.getDeprecated()) && !productApiBean.getDeprecated().isEmpty()&& !productApiBean.getDeprecated().toUpperCase().equals("TRUE"))&&
	    			  ((Objects.nonNull( productApiBean.getId()) && !productApiBean.getId().isEmpty()) ||
	    		        (Objects.nonNull( productApiBean.getExposure()) && !productApiBean.getExposure().isEmpty()) ||
	    		        (Objects.nonNull( productApiBean.getType()) && !productApiBean.getType().isEmpty()) ||
	    		        (Objects.nonNull( productApiBean.getMode()) && !productApiBean.getMode().isEmpty()))  ) {
	    		  
	    		  ProductsApiDTO productApiDTO = new ProductsApiDTO(productApiBean);
		          productsApi.add(productApiDTO);
	    		  
	    	  }
	    	  /*
	    	  
	    	  if((Objects.nonNull( productApiBean.getId()) && !productApiBean.getId().isEmpty()) ||
	    		        (Objects.nonNull( productApiBean.getExposure()) && !productApiBean.getExposure().isEmpty()) ||
	    		        (Objects.nonNull( productApiBean.getType()) && !productApiBean.getType().isEmpty()) ||
	    		        (Objects.nonNull( productApiBean.getMode()) && !productApiBean.getMode().isEmpty())
	    		       
	    		        ) {
	    	  
	          ProductsApiDTO productApiDTO = new ProductsApiDTO(productApiBean);
	          productsApi.add(productApiDTO);
	    	  }
	    	  */
	      }
        
	      this.products=productsApi;
        
        
        //Documentations


        if (Objects.nonNull(apiItem.getDocumentation())) {


            List<DocumentationDTO> documentations = new ArrayList<>();

            for (HippoBean documentationBean : apiItem.getDocumentation()) {

                processDocumentationBean(requestContext, documentations, documentationBean);


            }

            this.documentation = documentations;

        }

        if (Objects.nonNull(apiItem.getBasepath())) {
            this.basePath = apiItem.getBasepath();
        }

        if (Objects.nonNull(apiItem.getAuthor())) {
            this.author = apiItem.getAuthor();
        }

        if (Objects.nonNull(apiItem.getProfile())) {
            this.authorProfile = apiItem.getProfile();
        }

        if (apiItem.getIconAuthor() != null) {
            this.icon = generateHstImageLink(apiItem.getIconAuthor());
        }

        //LocalCountries
        List<LocalCountryDTO> localCountryList = new ArrayList<LocalCountryDTO>();
        for (LocalCountry localCountry : apiItem.getLocalCountry()) {
        	if (Objects.nonNull( localCountry.getText()) && !localCountry.getText().isEmpty()){
	            
        		LocalCountryDTO localCountryDTO = new LocalCountryDTO();
	            String name = localCountry.getText();
	
	            List<ModeVersionApiItemDTO> modeVersionList = new ArrayList<ModeVersionApiItemDTO>();
	            ModeVersionApiItemDTO modeVersionApiItemDTO = new ModeVersionApiItemDTO();
	            
	            for (ModeVersionApiItem modeVersion : localCountry.getModeVersion()) {
	                modeVersionApiItemDTO.setMode(modeVersion.getMode());
	                modeVersionApiItemDTO.setVersion(modeVersion.getVersion());
	                modeVersionApiItemDTO.setExposure(modeVersion.getExposureLocalCountry());
	                modeVersionList.add(modeVersionApiItemDTO);
	            }
	
	            localCountryDTO.setName(name);
	            localCountryDTO.setModeVersionList(modeVersionList);
	            localCountryList.add(localCountryDTO);

        	}
        }

        this.localCountries = localCountryList;

        //UpdateDate
        if (apiItem.getUpdateDate() != null) {
            Calendar cal = apiItem.getUpdateDate();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = format1.format(cal.getTime());
            this.updateDate = formattedDate;
        }


        this.channel = urlAttrs.get("channel");
        this.language = urlAttrs.get("locale");


    }


	private void processDocumentationBean(HstRequestContext requestContext,
			List<DocumentationDTO> documentations, HippoBean documentationBean) {
		
		DocumentationDTO documentationDTO = new DocumentationDTO();
		Documentation documentation = (Documentation) documentationBean;
		documentationDTO.setTitle(documentation.getTitle());
		documentationDTO.setDescription(
				Objects.nonNull(documentation.getDescription()) 
				? rewriteHstRichContent(documentation.getDescription()
						.getContent(), documentation.getDescription()
						.getNode(), requestContext.getResolvedMount().getMount())
		        : StringUtils.EMPTY);

		documentationDTO.setLinks(processlinksAndDownloads(documentation.getLinks()));


		if (Objects.nonNull(documentation.getPage())) {
		    List<PageDTO> pages = new ArrayList<>();
		    for (SectionLevelOne level1 : documentation.getPage()) {
		        processSectionLevels(requestContext, pages, level1);
		    }
		    documentationDTO.setPages(pages);
		}

		documentations.add(documentationDTO);
	}


	private void processSectionLevels(HstRequestContext requestContext, List<PageDTO> pages, SectionLevelOne level1) {
		PageDTO page = new PageDTO();
		page.setTitle(level1.getTitle());
		page.setRichContent(
		        Objects.nonNull(level1.getDescription()) ? rewriteHstRichContent(level1
		        		.getDescription().getContent(), level1.getDescription().getNode(), 
		        		requestContext.getResolvedMount().getMount()) : StringUtils.EMPTY);
		List<SectionDTO> sections = new ArrayList<>();
		for (SectionLevelTwo level2 : level1.getSections()) {
		    SectionDTO section = new SectionDTO();
		    section.setTitle(level2.getTitle());
		    section.setRichContent(
		            Objects.nonNull(level2.getDescription()) 
		            ? rewriteHstRichContent(level2.getDescription()
		            		.getContent(), level2.getDescription()
		            		.getNode(), requestContext.getResolvedMount()
		            		.getMount()) : StringUtils.EMPTY);
		    List<SectionDTO> sections2 = new ArrayList<>();
		    for (SectionLevelThree level3 : level2.getSections()) {
		        SectionDTO section2 = new SectionDTO();
		        section2.setTitle(level3.getText());
		        section2.setRichContent(
		                Objects.nonNull(level3.getHtml()) ? rewriteHstRichContent(level3.getHtml()
		                		.getContent(), level3.getHtml().getNode(), requestContext
		                		.getResolvedMount().getMount()) : StringUtils.EMPTY);
		        sections2.add(section2);
		    }
		    section.setSections(sections2);
		    sections.add(section);
		}
		page.setSections(sections);
		pages.add(page);
	}


    @Override
    public int compare(ApiItemDTO apiItem1, ApiItemDTO apiItem2) {
        // Ordenacion de las versiones de APIItem

        String version1 = apiItem1.getVersion();
        String version2 = apiItem2.getVersion();

        if(version1 == null || version1.isEmpty())
        {
        	version1 = "0.0";
        }
        if(version2 == null || version2.isEmpty()) 
        {	
        	version2 = "0.0";
        }
        String separador = Pattern.quote(".");
        String[] groups1 = version1.split(separador);
        String[] groups2 = version2.split(separador);
        String apiMayor = "";

        try {
            if (groups1.length > 0 && groups2.length > 0) {
                apiMayor = findApiMayor(groups1, groups2, apiMayor);
            }
            if ("apiItem1".equals(apiMayor)) {
                return -1;
            } else if ("apiItem2".equals(apiMayor)) {
                return 1;
            } else {
                return 0;
            }
        } catch (NumberFormatException nfe) {
            logger.error("Version contains an invalid character.", nfe);
            return 0;
        }
    }


	private String findApiMayor(String[] groups1, String[] groups2, String apiMayor) {
		int i = 0;
		boolean encontrado = false;
		while (i <= groups1.length && i <= groups2.length && !encontrado) {
		    if (Integer.parseInt(groups1[i]) > Integer.parseInt(groups2[i])) {
		        encontrado = true;
		        apiMayor = "apiItem1";
		    } else if (Integer.parseInt(groups1[i]) < Integer.parseInt(groups2[i])) {
		        encontrado = true;
		        apiMayor = "apiItem2";
		    } else
		    {
		    	//do nothing
		    }
		    i++;
		}
		return apiMayor;
	}

}


