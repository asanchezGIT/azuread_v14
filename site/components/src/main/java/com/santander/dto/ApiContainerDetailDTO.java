package com.santander.dto;

import lombok.Data;

@Data
public class ApiContainerDetailDTO {

    ApiContainerExpandDTO apiContainer;
    String securityApis;
}