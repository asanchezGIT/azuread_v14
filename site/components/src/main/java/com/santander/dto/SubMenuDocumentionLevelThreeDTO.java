package com.santander.dto;

import java.util.List;
import lombok.Data;

@Data
public class SubMenuDocumentionLevelThreeDTO {

    String title;
    String description;
    List<DocumentationLinkDTO> documents;
}
