package com.santander.dto;

import org.onehippo.cms7.essentials.components.rest.BaseRestResource;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiContainersExpandDTO extends BaseRestResource {

    ApiContainerExpandDTO apiContainer;
}
