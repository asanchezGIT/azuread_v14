package com.santander.dto;

import org.hippoecm.hst.container.RequestContextProvider;

import com.santander.beans.FaqItem;

import lombok.Data;

@Data
public class FaqItemDTO extends GenericDAOResource {
    private String id;
    private String title;
    private DescriptionDTO description;


    public FaqItemDTO(final FaqItem faqitem) {
        this.setId((faqitem.getIdentifier()));
        this.setTitle(faqitem.getTitle());
        this.setDescription(DescriptionDTO.builder().content(rewriteHstRichContent(faqitem.getDescription()
        		.getContent(), faqitem.getDescription().getNode(), RequestContextProvider.get().getResolvedMount()
        		.getMount())).build());
    }

}