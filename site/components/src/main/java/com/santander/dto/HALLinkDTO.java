package com.santander.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HALLinkDTO {

    String rel;
    String href;

}
