package com.santander.dto;

import java.util.List;

import org.onehippo.cms7.essentials.components.rest.BaseRestResource;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InformationsDTO extends BaseRestResource {

    List <HALLinkDTO> links;
    int count;
    int articlesCount;
    int newsCount;
    int webinarsCount;
    List <InformationDTO> informations;
}
