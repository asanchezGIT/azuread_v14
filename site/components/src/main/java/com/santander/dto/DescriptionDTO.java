package com.santander.dto;

import lombok.Builder;
import org.apache.commons.lang.StringUtils;

@Builder
public class DescriptionDTO {

	
	
    private String content;

    public String getContent() {
        return StringUtils.chomp(content);
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override public String toString() {
        return "DescriptionDTO [content=" + content + "]";
    }
}
