package com.santander.dto;

import java.util.List;

import org.onehippo.cms7.essentials.components.rest.BaseRestResource;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductContainersInfoBasicDTO extends BaseRestResource {
    List <ProductContainerInfoBasicDTO> productContainers;
    
    //Lista de documentations de tipo GetStarted
    List <DocumentationProductDTO> guides;
    
    
}
