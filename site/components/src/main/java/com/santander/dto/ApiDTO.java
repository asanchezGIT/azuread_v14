package com.santander.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiDTO {

  String id;
  String title;
  String version;
  String yaml;
  List<String> hostNames;
  String[]	authMethods;
  String[] regionsSandbox;
  String[] regionsLive;
  
}
