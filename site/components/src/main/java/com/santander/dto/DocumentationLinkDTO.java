package com.santander.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class DocumentationLinkDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String internalLink;
    String title;

    @Override public String toString() {
        return "DocumentationLinkDTO [internalLink=" + internalLink +",title=" + title + "]";
    }
}
