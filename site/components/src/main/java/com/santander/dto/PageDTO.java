package com.santander.dto;

import java.util.List;
import lombok.Data;

@Data
public class PageDTO {

    String title;
    String richContent;
    List<DocumentationLinkDTO> documentsRelation;
    List <SectionDTO> sections;

}
