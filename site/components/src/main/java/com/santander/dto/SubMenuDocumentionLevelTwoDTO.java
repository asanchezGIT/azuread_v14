package com.santander.dto;

import java.util.List;
import lombok.Data;

@Data
public class SubMenuDocumentionLevelTwoDTO {

    String title;
    String description;
    List<DocumentationLinkDTO> documents;
    List <SubMenuDocumentionLevelThreeDTO> documentsGroups;

}
