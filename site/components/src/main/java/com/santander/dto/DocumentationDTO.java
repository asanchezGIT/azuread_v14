package com.santander.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class DocumentationDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String internalLinkId;
    String id; //alias or UUID
    String title;
    String description;
    String type;
    List<PageDTO> pages;
    List<LinkDTO> links;
    MenuDocumentationDTO menuDocumentation;

    @Override public String toString() {
        return "DocumentationDTO [internalLinkId=" + internalLinkId +",title=" + title + ",description=" + description +
                ",type="+type+",pages=" + pages + "links=" + links + "menuDocumentation=" + menuDocumentation + "]";
    }

}
