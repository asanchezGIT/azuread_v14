package com.santander.dto;

import java.util.List;

import org.onehippo.cms7.essentials.components.rest.BaseRestResource;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiContainersDTO extends BaseRestResource {

    List <HALLinkDTO> _links;
    int _count;
    List <ApiContainerDTO> _embedded;
    
}
