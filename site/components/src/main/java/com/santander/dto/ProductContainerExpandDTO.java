package com.santander.dto;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
							 
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.Node;

import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.standard.HippoBean;
																 
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.util.PathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.santander.beans.ApiContainerItem;
								   
import com.santander.beans.Documentation;
import com.santander.beans.IntegrationProduct;
import com.santander.beans.MenuDocumentation;
import com.santander.beans.ProductContainerItem;
import com.santander.beans.SectionLevelOne;
import com.santander.beans.SectionLevelThree;
import com.santander.beans.SectionLevelTwo;
import com.santander.beans.SubMenuDocumentationLevelOne;
import com.santander.beans.SubMenuDocumentationLevelThree;
import com.santander.beans.SubMenuDocumentationLevelTwo;
import com.santander.components.model.DocumentsGroupDTO;
import com.santander.utils.Constants;
import com.santander.utils.GlobalConfiguration;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import com.santander.utils.Locale;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductContainerExpandDTO extends GenericDAOResource{


    private static Logger logger = LoggerFactory.getLogger(ProductContainerExpandDTO.class);
    private String id;
    private String idIL;
    private String title;
    private String version;
    private DescriptionDTO description;
    private String icon;
    private String[] tags;
    List<LinkDTO> links;
    LinkDTO postmanCollection;
    List<IntegrationProductDTO> integrationsProduct;
    String featuresOverview;
    List<DocumentationDTO> documentation;
    List <ApiContainerDTO> apisContainers;
    MenuDocumentationDTO menuDocumentation;
    String author;
    String authorProfile;
    String authorIcon;
    String updateDate;
    private boolean deprecated;
    private String status;
    private String channel;
    private String language;
    private String region;
  
    public ProductContainerExpandDTO(ProductContainerItem productContainer, String siteName){
    	
    	//logger.info("*****INICIO ProductContainerExpandDTO");
    	HstRequestContext requestContext = RequestContextProvider.get();
    	
    	//locale del Context (url de llamada)
    	Locale locale=new Locale();
    	String localeRequest=locale.getLocaleContext(requestContext,"product_containers");
     	
        this.id = InternalLinkProcessor.getAliasByUuid(productContainer.getCanonicalUUID());
        logger.debug("*****id");
        this.idIL = Objects.nonNull(productContainer.getIdIL())? productContainer.getIdIL() : StringUtils.EMPTY;
        logger.debug("*****idIL");
        this.title = Objects.nonNull( productContainer.getTitleCMS()) && ! productContainer.getTitleCMS().isEmpty() 
        		?  productContainer.getTitleCMS() : productContainer.getNameIL();
        logger.debug("*****title");
        this.version = Objects.nonNull( productContainer.getVersionIL()) && ! productContainer.getVersionIL()
        		.isEmpty() ?  productContainer.getVersionIL() : productContainer.getVersionCMS();
        logger.debug("*****version");
        
        Mount mountPoint=hstRequestContext.getResolvedMount().getMount();
        
         try {
		      
		        String localeContent=productContainer.getLocaleString().replace("-", "_");
		      
		        String mountRequestPath = requestContext.getResolvedMount().getMount().getContentPath();
		       
		        String localeMountRequestPath=mountRequestPath.substring(mountRequestPath.lastIndexOf("/")+1);
		        
		       
		        if (!localeContent.equals(localeMountRequestPath)){ //Si el lenguaje del contenido no se corresponde con el de la petición, busco el Mount del contenido
		          	 mountPoint=requestContext.getMount(siteName+"-"+localeContent); //coge el Mount con el atributo alias=superdiginal-es_es
		         	
		         	 
		         }
		        
		        //Description
		        if (Objects.nonNull(productContainer.getDescriptionCMS())) {
		        	
		        		        	
		          this.description = DescriptionDTO.builder()
		                    .content(rewriteHstRichContent(productContainer.getDescriptionCMS().getContent(),
		                    		productContainer.getDescriptionCMS().getNode(),
		                    		mountPoint))
		                    .build();
		          
		        }
		        else {
		        	this.description = DescriptionDTO.builder().content("").build();
		        }
		        
         }
         catch(Exception e) {
        	 logger.error("*****error",e);
        	 
        	 this.description = DescriptionDTO.builder().content("").build();
         }
        
        
  
        logger.debug("*****description");
        
        this.icon = Objects.nonNull(productContainer.getIcon())? generateHstImageLink(productContainer.getIcon()) : StringUtils.EMPTY;
        logger.debug("*****icon");
        
        //Tags   
        try {
            String[] tagsDTO=productContainer.getTags();
	        this.tags = tagsDTO;
        }
        catch(Exception e) {
        	logger.error("*****error",e);
       	 	this.tags = new String[0];
        }
        logger.debug("*****tags");
        
        //Links
        
        try {
	        if (productContainer.getLinks() != null) {
	            this.links = processlinksAndDownloads(productContainer.getLinks());
	        }
	        else {
	        	this.links=new ArrayList();
	        }
        }
        catch(Exception e) {
         	this.links=new ArrayList();
         	logger.error("*****error",e);
        }
        logger.debug("*****links");
        
        
        //IntegrationsProduct
        
        try {
        	 if(Objects.nonNull(productContainer.getIntegrationsProduct())) {
	            this.integrationsProduct = processIntegrationsProduct(requestContext,productContainer.getIntegrationsProduct());
	        }
	        else {
	        	this.integrationsProduct=new ArrayList();
	        }
        }
        catch(Exception e) {
        	this.integrationsProduct=new ArrayList();
         	logger.error("*****error",e);
        }
        logger.debug("*****IntegrationsProduct");
        
        
        //PostmanCollection
        
        try {
	        if(Objects.nonNull(productContainer.getPostmanCollection())) {
	         logger.debug("*****HAY POSTMAN COLLECTION");	
	       	 this.postmanCollection = LinkDTO.builder().title("Postman Collection")
	       			 .url(generateHstLink(productContainer.getPostmanCollection())).build();
	       	 }
             else {
	        	 logger.debug("*****NO HAY POSTMAN COLLECTION");	
	        	 this.postmanCollection =null;
             }
        }
        catch (Exception e) {
        	this.postmanCollection =null;
       		logger.error("*****error",e);
       	 }
        
        logger.debug("*****postmanCollection");
     
        
       //feature
        
       try {
             
        
	       if (Objects.nonNull(productContainer.getFeaturesOverview())) {
	         this.featuresOverview = rewriteHstRichContent(productContainer.getFeaturesOverview().getContent(),
	        		   productContainer.getFeaturesOverview().getNode(),
	        		   mountPoint);
	       }
	       else {
	       	this.featuresOverview =StringUtils.EMPTY;
	       }
			       
            
            
		 }
		 catch( Exception e) {
		 	this.featuresOverview = StringUtils.EMPTY;
		 	logger.error("*****ERROR ProductContainerExpandDTO",e);
		 	
		             
		
		 }	       
        logger.debug("*****featuresOverview");
       /*
       //Documentations
       try { 
	       List<DocumentationDTO> documentations = new ArrayList<>();
	       if (Objects.nonNull(productContainer.getDocumentation())) {
	
	           documentations = new ArrayList<>();
	
	           for (HippoBean documentationBean : productContainer.getDocumentation()) {
	               processDocumentationBean(requestContext,documentations, documentationBean,mountPoint);
	           }
	       }    
	       this.documentation = documentations;
       }
       catch( Exception e) {
		 	this.documentation = new ArrayList();
		 	logger.error("*****ERROR",e);
       }
       
       logger.debug("*****documentation");
       
      */
        
        
        
      
       //MenuDocumentation
       MenuDocumentationDTO menuDocumentationDTO = new MenuDocumentationDTO();
       try { 
	      
	       if (Objects.nonNull(productContainer.getMenuDocumentation())) {
	    	     logger.debug("*****EL PRODUCTO SI TIENE MENU DOCUMENTACION ASOCIADO");	
	    	     processMenuDocumentationBean(requestContext,menuDocumentationDTO, productContainer.getMenuDocumentation(),mountPoint);
	    	     logger.debug("*****DESPUES processMenuDocumentationBean:"+menuDocumentationDTO.getDocumentsGroups().toString());	
	    	     this.menuDocumentation = menuDocumentationDTO;
	         
	       }    
	       else {
	        	 logger.debug("*****EL PRODUCTO NO TIENE MENU DOCUMENTACION ASOCIADO");	
	        	 this.menuDocumentation =menuDocumentationDTO;
          }
       }
       catch (Exception e) {
          	this.menuDocumentation =menuDocumentationDTO;
         		logger.error("*****error",e);
         	 }
       
       logger.debug("*****menuDocumentation");
       
       
       //author
       this.author = Objects.nonNull(productContainer.getAuthor())? productContainer.getAuthor() : StringUtils.EMPTY;
       this.authorProfile = Objects.nonNull(productContainer.getProfile())? productContainer.getProfile() : StringUtils.EMPTY;
       this.authorIcon = Objects.nonNull(productContainer.getIconAuthor())? generateHstImageLink(productContainer.getIconAuthor()) : StringUtils.EMPTY;
       
       //UpdateDate
       if (productContainer.getUpdateDate() != null) {
           Calendar cal = productContainer.getUpdateDate();
           SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
           String formattedDate = format1.format(cal.getTime());
           this.updateDate = formattedDate;
       }
       else {
          	
          	this.updateDate =StringUtils.EMPTY;
       }
        
       logger.debug("*****updateDate");
       
       
       try
       {
	        // Recoge los ApiContainers del productContainerItem 
	        ApiContainerItem apiContainerItem=new ApiContainerItem();
	        ApiContainerDTO apiContainerDTO=new ApiContainerDTO();
	        List<ApiContainerDTO> apiContainersList=new ArrayList<>();
	        
	        //logger.info("*****numApisContainers: {}",productContainer.getApiContainers().size());       
	        Iterator<HippoBean> apisContainersIterator=productContainer.getApiContainers().iterator();
	        
	        while (apisContainersIterator.hasNext()) {
	        	
	            apiContainerItem = (ApiContainerItem) apisContainersIterator.next();
	            //logger.info("*****pasado a objeto apiContainerItem");;  
	            //Si no esta deprecada
	            //logger.info("*****esta deprecado?:"+ apiContainerItem.getDeprecated());
	            
	            if ("No".equals(apiContainerItem.getDeprecated())) {
	             	apiContainerDTO=new ApiContainerDTO(apiContainerItem,siteName);
	             	 //logger.info("*****pasado a objeto apiContainerDTO");;  
	            	apiContainersList.add(apiContainerDTO);
	            }
	              
	        }
	        this.apisContainers=apiContainersList; 
       }
        catch( Exception e) {
		 	this.apisContainers = new ArrayList();
		 	logger.error("*****ERROR API CONTAINERS",e);
       }
	   logger.debug("*****apisContainers");
	   
       this.deprecated=Objects.nonNull(productContainer.getDeprecated()) && !productContainer.getDeprecated()
        		.isEmpty() && "No".contentEquals(productContainer.getDeprecated()) ?  false: true;
       logger.debug("*****deprecated");
       
       this.status = Objects.nonNull(productContainer.getStatus())? productContainer.getStatus() : StringUtils.EMPTY;
       logger.debug("*****status");
       
       this.region = Objects.nonNull(productContainer.getRegion())? productContainer.getRegion() : StringUtils.EMPTY;
       logger.debug("*****region");
       
     
       try {
    	   Map<String, String> urlAttrs=null;
	       urlAttrs =HippoUtils.getDocumentAttrsByURLCustom(productContainer);
	       
	       logger.debug("*****channel:"+urlAttrs.get("channel"));
	       logger.debug("*****locale:"+urlAttrs.get("locale"));
	       this.channel = urlAttrs.get("channel");
	       this.language = urlAttrs.get("locale");
	       
       
       }
       catch (Exception e) {
       	
	       	logger.error("*****error en getDocumentAttrsByURLCustom",e);
	       	this.channel = "";
		    this.language = "";
	       	
       	
       }
       
    	
    }
    
    
    private void processMenuDocumentationBean(HstRequestContext requestContext,
			MenuDocumentationDTO menuDocumentationDTO, HippoBean menuDocumentationBean, Mount mountPoint) {
    	
    	
    	//logger.info("menuDocumentationBean path: "+menuDocumentationBean.getPath());
		MenuDocumentation menuDocumentation = (MenuDocumentation) menuDocumentationBean;
			
		
		//menuDocumentationDTO.setId(menuDocumentationBean.getCanonicalUUID());
		 
		
		//SubMenusDocumentationLevelOne
		if (Objects.nonNull(menuDocumentation.getDocumentsGroups())) {
			    	
		    List<SubMenuDocumentionLevelOneDTO> subMenusDocumentationLevelOne = new ArrayList<>();
		    for (SubMenuDocumentationLevelOne level1 : menuDocumentation.getDocumentsGroups()) {
		    	    
		        processMenuLevelOne(requestContext, subMenusDocumentationLevelOne, level1, mountPoint);
		        
		    }
		    	    
		    
		    menuDocumentationDTO.setDocumentsGroups(subMenusDocumentationLevelOne);
		    menuDocumentationDTO.setId(menuDocumentation.getCanonicalUUID()); 
		    
		}
		
		
	}
    
    /*
    private void processDocumentationBean(HstRequestContext requestContext,
			List<DocumentationDTO> documentations, HippoBean documentationBean, Mount mountPoint) {
    	
    	DocumentationDTO documentationDTO = new DocumentationDTO();
    	
		Documentation documentation = (Documentation) documentationBean;
		
		//alias or UUID
		if (Objects.nonNull(documentation.getAlias()) && !documentation.getAlias().isEmpty()) {
			
			documentationDTO.setId(documentation.getAlias());
		}
		else {
			documentationDTO.setId(documentation.getCanonicalUUID());
		}
		
		//title
		if (Objects.nonNull(documentation.getTitle()) && !documentation.getTitle().isEmpty()) {
			documentationDTO.setTitle(documentation.getTitle());
		}		
				
		//description
		try {
			if (Objects.nonNull(documentation.getTitle()) && !documentation.getTitle().isEmpty()) { 
			documentationDTO.setDescription(
					Objects.nonNull(documentation.getDescription()) 
					? rewriteHstRichContent(documentation.getDescription()
							.getContent(), documentation.getDescription()
							.getNode(), mountPoint)
			        : StringUtils.EMPTY);
			}
		}
   
	    catch( Exception e) {
	    	documentationDTO.setDescription(StringUtils.EMPTY);
	    	logger.error("*****ERROR documentationDTO:"+e.getMessage());
	    	e.printStackTrace();
	                
	
	    }
		
		
		//Links
		if (Objects.nonNull(documentation.getLinks())) {
			documentationDTO.setLinks(processlinksAndDownloads(documentation.getLinks()));
		}

		//Pages
		if (Objects.nonNull(documentation.getPage())) {
		    List<PageDTO> pages = new ArrayList<>();
		    for (SectionLevelOne level1 : documentation.getPage()) {
		        processSectionLevels(requestContext, pages, level1, mountPoint);
		    }
		    documentationDTO.setPages(pages);
		}
		

		documentations.add(documentationDTO);
		
	}
    */
    
    
    private void processMenuLevelOne(HstRequestContext requestContext,  List<SubMenuDocumentionLevelOneDTO> SubMenusLevelOne, SubMenuDocumentationLevelOne level1, Mount mountPoint) {
    	
    	//METODO QUE PROCESA EL CONTENIDO DE UN SubMenuDocumentationLevelOne y lo añade a una Lista de Menus de Nivel1
    	
    	logger.debug("INICIO processMenuLevelOne");    
    	
    	SubMenuDocumentionLevelOneDTO subMenuDocumentionLevelOneDTO = new SubMenuDocumentionLevelOneDTO();
    	//Level1.title
    	subMenuDocumentionLevelOneDTO.setTitle(level1.getTitle());
    	logger.debug("despues titulo");    
    	//Level1.description
		try {
			logger.debug("****DESCRIPTION MENU****:"+level1.getDescription());
			subMenuDocumentionLevelOneDTO.setDescription(Objects.nonNull(level1.getDescription()) ? rewriteHstRichContent(level1
	        		.getDescription().getContent(), level1.getDescription().getNode(), 
	        		requestContext.getResolvedMount().getMount()) : StringUtils.EMPTY);
		}
		catch( Exception e) {
			subMenuDocumentionLevelOneDTO.setDescription(StringUtils.EMPTY);
        	logger.error("*****ERROR processMenuLevels:"+e.getMessage());
        	e.printStackTrace();
                    
   
        }
		logger.debug("despues descripcion"); 
		
		
	 	//Level1.DocumentsGroups
		List<SubMenuDocumentionLevelTwoDTO> subMenusDocumentionLevelTwoDTO = new ArrayList<>();
		for (SubMenuDocumentationLevelTwo level2 : level1.getDocumentsGroups()) {
			SubMenuDocumentionLevelTwoDTO section2 = new SubMenuDocumentionLevelTwoDTO();
			//Level2.Title
			section2.setTitle(level2.getTitle());
			logger.debug("despues Title leveltwo"); 
			
			//Level2.Description
		    try {
		    section2.setDescription(
		            Objects.nonNull(level2.getDescription()) 
		            ? rewriteHstRichContent(level2.getDescription()
		            		.getContent(), level2.getDescription()
		            		.getNode(),mountPoint): StringUtils.EMPTY);
		    }
		    catch(Exception e) {
		    	section2.setDescription(StringUtils.EMPTY);
		    	
		    }
		    logger.debug("despues description leveltwo"); 
		    
		    //Level2.documents
		  
		       List<DocumentationLinkDTO> documentations2 = new ArrayList<DocumentationLinkDTO>();
		       try { 
			       
			       if (Objects.nonNull(level2.getDocuments())) {
			
			    	   setMenuDocuments(documentations2, level2.getDocuments(),requestContext);
			    	   
			       }    
			      
		       }
		       catch( Exception e) {
				 	
				 	logger.error("*****ERROR",e);
		       }
		       section2.setDocuments(documentations2);
		       logger.debug("despues documents leveltwo"); 
		    
		    //Level2.DocumentsGroup
		   
		    List<SubMenuDocumentionLevelThreeDTO> subMenusDocumentionLevelThreeDTO = new ArrayList<>();
		    for (SubMenuDocumentationLevelThree level3 : level2.getDocumentsGroups()) {
		    	SubMenuDocumentionLevelThreeDTO section3 = new SubMenuDocumentionLevelThreeDTO();
		    	//Level3.Title
		    	section3.setTitle(level3.getTitle());
		    	//Level3.description
		        try {
		        section3.setDescription(
		                Objects.nonNull(level3.getDescription()) ? rewriteHstRichContent(level3.getDescription()
		                		.getContent(), level3.getDescription().getNode(), requestContext
		                		.getResolvedMount().getMount()) : StringUtils.EMPTY);
		        }
		        catch(Exception e) {
			    	section3.setDescription(StringUtils.EMPTY);
			    	
			    }
		        //Level3.Documents
		        List<DocumentationLinkDTO> documentations3 = new ArrayList<DocumentationLinkDTO>();
		        try { 
			       
			       if (Objects.nonNull(level3.getDocuments())) {
			
			    	   setMenuDocuments(documentations3, level3.getDocuments(),requestContext);
			    	   
			       }    
			      
		        }
		        catch( Exception e) {
				 	
				 	logger.error("*****ERROR",e);
		        }
		        section3.setDocuments(documentations3);
			    
			        
		        subMenusDocumentionLevelThreeDTO.add(section3);
		    }
		    logger.debug("despues documentsgroup leveltwo"); 
		    section2.setDocumentsGroups(subMenusDocumentionLevelThreeDTO);
		    subMenusDocumentionLevelTwoDTO.add(section2);
		}
		logger.debug("despues documentsgroup levelone"); 
		subMenuDocumentionLevelOneDTO.setDocumentsGroups(subMenusDocumentionLevelTwoDTO);
		
		
		
		//Level1.documents
    	List<DocumentationLinkDTO> documentations = new ArrayList<DocumentationLinkDTO>();
       
       try { 
	       
	       if (Objects.nonNull(level1.getDocuments())) {
	
	    	   setMenuDocuments(documentations, level1.getDocuments(),requestContext);
	    	   
	       }    
	      
       }
       catch( Exception e) {
		 	
		 	logger.error("*****ERROR",e);
       }
       subMenuDocumentionLevelOneDTO.setDocuments(documentations);
       logger.debug("despues documents levelone"); 
	      
       
	   SubMenusLevelOne.add(subMenuDocumentionLevelOneDTO);
	  
	}

   
    private void setMenuDocuments (List<DocumentationLinkDTO> documentations,List<HippoBean> documents,HstRequestContext requestContext)
	{
	
		
		try
		{
			for (HippoBean document : documents) {
	        	   String title=null;
	   				if (document.getNode()!=null && document.getNode().getProperty("santanderbrxm:title")!=null)
	   				{
	   					title=document.getNode().getProperty("santanderbrxm:title").getString();
	   				
	   				} else if (document.getNode()!=null && document.getNode().getProperty("santanderbrxm:tutle")!=null) 
	   				{
	   					title=document.getNode().getProperty("santanderbrxm:tutle").getString();
	   				} 
	   				
	   				String url =requestContext.getHstLinkCreator().create(document, requestContext)
	   		                .toUrlForm(requestContext, true);
	   				String uuid=HippoUtils.getNodeId(document.getNode().getPath(), requestContext);
	   				
	   				String type = document.getNode().getPrimaryNodeType().getName();

					DocumentationLinkDTO documentationLinkDTO = new DocumentationLinkDTO();
					documentationLinkDTO.setTitle(title);
					documentationLinkDTO.setInternalLink(internalLinkProcessor.generateInternalLink(url, type, uuid));

					documentations.add(documentationLinkDTO);
	           }
		} catch (Exception e)
		{
			logger.error("Exception retrieving documents",e);
		}
	}
    
    
   private void processSectionLevels(HstRequestContext requestContext, List<PageDTO> pages, SectionLevelOne level1, Mount mountPoint) {
		PageDTO page = new PageDTO();
		page.setTitle(level1.getTitle());
		
		try {
		page.setRichContent(
		        Objects.nonNull(level1.getDescription()) ? rewriteHstRichContent(level1
		        		.getDescription().getContent(), level1.getDescription().getNode(), 
		        		requestContext.getResolvedMount().getMount()) : StringUtils.EMPTY);
		}
		catch( Exception e) {
			page.setRichContent(StringUtils.EMPTY);
        	logger.error("*****ERROR processSectionLevels:"+e.getMessage());
        	e.printStackTrace();
                    
   
        }
		
		List<SectionDTO> sections = new ArrayList<>();
		for (SectionLevelTwo level2 : level1.getSections()) {
		    SectionDTO section = new SectionDTO();
		    section.setTitle(level2.getTitle());
		    try {
		    section.setRichContent(
		            Objects.nonNull(level2.getDescription()) 
		            ? rewriteHstRichContent(level2.getDescription()
		            		.getContent(), level2.getDescription()
		            		.getNode(),mountPoint): StringUtils.EMPTY);
		    }
		    catch(Exception e) {
		    	section.setRichContent(StringUtils.EMPTY);
		    	
		    }
		    
		    
		    List<SectionDTO> sections2 = new ArrayList<>();
		    for (SectionLevelThree level3 : level2.getSections()) {
		        SectionDTO section2 = new SectionDTO();
		        section2.setTitle(level3.getText());
		        try {
		        section2.setRichContent(
		                Objects.nonNull(level3.getHtml()) ? rewriteHstRichContent(level3.getHtml()
		                		.getContent(), level3.getHtml().getNode(), requestContext
		                		.getResolvedMount().getMount()) : StringUtils.EMPTY);
		        }
		        catch(Exception e) {
			    	section2.setRichContent(StringUtils.EMPTY);
			    	
			    }
		        sections2.add(section2);
		    }
		    section.setSections(sections2);
		    sections.add(section);
		}
		page.setSections(sections);
		pages.add(page);
	}



}
 
