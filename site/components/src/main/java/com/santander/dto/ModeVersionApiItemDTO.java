package com.santander.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Data
@NoArgsConstructor
@Produces({MediaType.APPLICATION_JSON})

public class ModeVersionApiItemDTO  extends GenericDAOResource {

    private String mode;
    private String version;
    private String exposure;
    
}

