package com.santander.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IntegrationProductDTO {
	
    private String title;
    private String icon;
    private List<CallToActionItemSimpleDTO> items = new ArrayList<>();
   
}
