package com.santander.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;
import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDTO {
  
  String id;
  String title;
  String team;
  Boolean sandbox;
  Boolean live;
  Boolean deprecated;
  String newVersion;
  String description;
  String featuresOverview;
  List<LinkDTO> linksAndDownloads;
  LinkDTO postmanCollection;
  List<DocumentationDTO> documentation;
  List<ApiDTO> apis; 
  Calendar createdDate;
  Calendar modifiedDate;
  String author;
  List<String> tags;
  String[] createdBy;
  Calendar lastUpdate;

}
