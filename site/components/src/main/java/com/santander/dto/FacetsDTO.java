package com.santander.dto;

import static com.santander.utils.Constants.FACE_TITLE;
import static com.santander.utils.Constants.FACE_TITLE_2;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.onehippo.taxonomy.api.Category;
import org.onehippo.taxonomy.api.Taxonomy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.Data;

@Data
public class FacetsDTO extends GenericDAOResource {
  private static Logger logger = LoggerFactory.getLogger(FacetsDTO.class);
  private List<FacetItemDTO> facets = new ArrayList<>();



  public FacetsDTO(final Taxonomy taxonomy) {
    try {

      List<FacetItemDTO> facetItemList = new ArrayList<FacetItemDTO>();
      for (Category category : taxonomy.getCategories()) {
        FacetItemDTO facetItem = new FacetItemDTO();

        String faceTitle = category.getInfo(new Locale("es")).getName();
        logger.debug("***********faceTitle: {}" , faceTitle);

        if (faceTitle.contains(FACE_TITLE_2)) {
          logger.debug("**********identificada /");
          faceTitle = faceTitle.replace(FACE_TITLE_2, "/");
          logger.debug("*********faceTitle1 despues del remplazo: {}" , faceTitle);
        } else if (faceTitle.contains(FACE_TITLE)) {
          logger.debug("********identificada /");
          faceTitle = faceTitle.replace(FACE_TITLE, "/");
          logger.debug("******faceTitle2 despues del remplazo: {}" , faceTitle);
        } else
        {
        	//do nothing
        }


        facetItem.setId(category.getKey());
        facetItem.setTitle(faceTitle);



        setFacets(category, facetItem);
        
        facetItemList.add(facetItem);


      }
      this.facets = facetItemList;
    } catch (Exception e) {
      logger.error("Error: ", e);
    }
  }

private void setFacets(Category category, FacetItemDTO facetItem) {
	if (category.getChildren() != null && category.getChildren().size() > 0) 
	{
	  List<FacetItemDTO> facetChildrenList = new ArrayList<FacetItemDTO>();
	  for (Category childCategory : category.getChildren()) {
	    facetChildrenList.add(getChildrensInfo(childCategory));
	  }
	  facetItem.setFacets(facetChildrenList);
	}
}

  private FacetItemDTO getChildrensInfo(Category category) {
    FacetItemDTO facetItem = new FacetItemDTO();
    try {



      String faceTitle = category.getInfo(new Locale("es")).getName();
      logger.debug("***********faceTitle: {}" , faceTitle);

      if (faceTitle.contains(FACE_TITLE_2)) {
        logger.debug("***********identificada /");
        faceTitle = faceTitle.replace(FACE_TITLE_2, "/");
        logger.debug("***********faceTitle1 despues del remplazo: {}" , faceTitle);
      } else if (faceTitle.contains(FACE_TITLE)) {
        logger.debug("***********identificada /");
        faceTitle = faceTitle.replace(FACE_TITLE, "/");
        logger.debug("***********faceTitle2 despues del remplazo: {}" , faceTitle);
      } else
      {
    	  //do nothing
      }

      facetItem.setId(category.getKey());
      facetItem.setTitle(faceTitle);



      List<FacetItemDTO> facetChildrenList = new ArrayList<FacetItemDTO>();
      for (Category childCategory : category.getChildren()) {
        facetChildrenList.add(getChildrensInfo(childCategory));
      }
      facetItem.setFacets(facetChildrenList);
    } catch (Exception e) {
      logger.error("Error: ", e);
    }
    return facetItem;
  }
}
