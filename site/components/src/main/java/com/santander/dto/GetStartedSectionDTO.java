package com.santander.dto;

import java.util.List;
import lombok.Data;

@Data
public class GetStartedSectionDTO {

    String title;
    DescriptionDTO description;
    List<DocumentationLinkDTO> documentsRelation;
    
    List<GetStartedSectionDTO> sections;
}
