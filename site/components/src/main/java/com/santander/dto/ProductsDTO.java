package com.santander.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;
@Data
@JsonRootName(value = "products")
public class ProductsDTO {
  
  List<ProductDTO> products;

}
