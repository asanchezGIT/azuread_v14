package com.santander.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

public final class CertificateUtils {

    public static final String PUBLIC_KEY_CERTIFICATE = "publicKeyCertificate";

    private CertificateUtils() {
    }

    private static Logger logger = LoggerFactory.getLogger(CertificateUtils.class);

    public static KeyStore importCertificateToLocalKeystore(String certificatePath, String keystorePath
    		, String keyStorePassword) {
        logger.debug("START importCertificateToLocalKeystore()");

        logger.debug("certificatePath {}", certificatePath);
        logger.debug("keystorePath {}", keystorePath);

        try {

            KeyStore keystore = getOrCreateKeyStore(keystorePath, keyStorePassword);

            char[] password = keyStorePassword.toCharArray();

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream certstream = fullStream(certificatePath);
            Certificate certs = cf.generateCertificate(certstream);

            // Continue if the alias already exists in the keystore
            if (keystore.getCertificate(PUBLIC_KEY_CERTIFICATE) != null) {
                logger.debug("Alias already exists in the keystore");
                return keystore;
            }

            File keystoreFile = new File(keystorePath);
            // Load the keystore contents

            logger.debug("Load the keystore contents");

            FileInputStream in = new FileInputStream(keystoreFile);
            keystore.load(in, password);
            in.close();

            keystore.setCertificateEntry(PUBLIC_KEY_CERTIFICATE, certs);

            // Save the new keystore contents
            logger.debug("Save the new keystore contents");
            FileOutputStream out = new FileOutputStream(keystoreFile);
            keystore.store(out, password);
            out.close();

            logger.debug("END importCertificateToLocalKeystore()");
            return keystore;

        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            logger.error("Keystore error", e);
        } catch (IOException e) {
            logger.error("File IO Exception", e);
        } catch (Exception e) {
            logger.error("Error loading certificate trust store", e);
        }
        return null;
    }

    public static KeyStore getOrCreateKeyStore(String keystorePath, String password) 
    		throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
        logger.debug("START getOrCreateKeyStore()");
        File file = new File(keystorePath);
        KeyStore keyStore = KeyStore.getInstance("JKS");
        if (file.exists()) {
            // if exists, load
            keyStore.load(new FileInputStream(file), password.toCharArray());
        } else {
            // if not exists, create
            keyStore.load(null, null);
            keyStore.store(new FileOutputStream(file), password.toCharArray());
        }
        logger.debug("END getOrCreateKeyStore()");
        return keyStore;
    }

    private static InputStream fullStream(String fname) throws IOException {
        FileInputStream fis = new FileInputStream(fname);
        DataInputStream dis = null;
        byte[] bytes = null;
        try
        {
        	dis = new DataInputStream(fis);
        	bytes = new byte[dis.available()];
        	dis.readFully(bytes);
        } catch (IOException e)
        {
        	logger.error("",e);
        } finally
        {
        	dis.close();
        }
        return new ByteArrayInputStream(bytes);
    }

}