package com.santander.security;

import io.jsonwebtoken.JwtException;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class PublicKeyService {
    private static final int CACHE_SIZE = 100;
	private static final Logger logger = LoggerFactory.getLogger(PublicKeyService.class);
    public static final String SANTANDER_PUBLIC_KEY_CACHE = "santanderPublicKeyCache";
    private static CacheManager manager = CacheManager.create();

    private static final Integer TIMEOUT=3000;
    
    public void init() {
        logger.info("(Re-)Initializing Public key cache");
        // (re)create cache
        manager.removeCache(SANTANDER_PUBLIC_KEY_CACHE);
        Cache publicKeyCache = new Cache(new CacheConfiguration(SANTANDER_PUBLIC_KEY_CACHE, CACHE_SIZE));
        publicKeyCache.getCacheConfiguration().setTimeToLiveSeconds(600);
        manager.addCache(publicKeyCache);

    }

    public PublicKeyService() {
    	//Empty Constructor
    }

    public static String getPublicKeyFromServer(String serverUrl, KeyStore keyStore, String kid) {
        try {
            final Cache publicKeyCache = manager.getCache(SANTANDER_PUBLIC_KEY_CACHE);
            // try to retrieve element from cache
            final Element element = publicKeyCache.get(kid);
            if (element != null) {
                return (String) element.getObjectValue();
            } else {
                // if cache element does not exist retrieve object and place in cache
                logger.info("Getting public key from the server");
                logger.info("Connecting to: {}" , serverUrl+kid);

                TrustManagerFactory tmf = TrustManagerFactory
                		.getInstance(TrustManagerFactory.getDefaultAlgorithm()); // PKIX
                tmf.init(keyStore);

                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                kmf.init(keyStore, null);

                SSLContext sslCtx = SSLContext.getInstance("TLSv1.2");
                sslCtx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
                SSLSocketFactory sslSF = sslCtx.getSocketFactory();

                final URLConnection urlConnection = new URL(serverUrl+kid).openConnection();
                if (urlConnection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) urlConnection).setSSLSocketFactory(sslSF);
                }
                urlConnection.setConnectTimeout(TIMEOUT);
                String publicKey = IOUtils.toString(urlConnection.getInputStream(), StandardCharsets.UTF_8.name());
                JSONObject jsonObject = new JSONObject(publicKey);
                logger.info("Key received from the server: {}" , publicKey);
                String publicKeyValue = jsonObject.getString("key");
                logger.info("Public key extracted: {}" , publicKeyValue);

                publicKeyCache.put(new Element(kid, publicKeyValue));
                return publicKeyValue;
            }

        } catch (Exception e) {
        	logger.error("",e);
            throw new JwtException("Error retrieving public key from server", e);
        }
    }


}
