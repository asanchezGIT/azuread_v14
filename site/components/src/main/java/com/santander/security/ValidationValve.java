package com.santander.security;

import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.IncorrectClaimException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MissingClaimException;

import org.apache.commons.lang3.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.container.valves.AbstractOrderableValve;
import org.hippoecm.hst.core.container.ContainerException;
import org.hippoecm.hst.core.container.ValveContext;
import org.onehippo.forge.selection.hst.contentbean.ValueList;
import org.onehippo.forge.selection.hst.util.SelectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static com.santander.utils.Constants.BEARER_AUTH_HEADER_PROPERTY;

public class ValidationValve extends AbstractOrderableValve {

    public static final String SKIP_TOKEN_VALIDATION = "skipTokenValidation";

    public static final String AUD_CMS = "CMS";
    private static Logger logger = LoggerFactory.getLogger(ValidationValve.class);

    @Override
    public void invoke(ValveContext context) throws ContainerException {
        try {
            logger.debug("Start validation valve");

            Mount mount = context.getRequestContext().getResolvedMount().getMount();
            String skipValidation = mount.getProperty(SKIP_TOKEN_VALIDATION);

            if (!skipValidation.equals("true")) {
                logger.debug("Validation active");

                Jws<Claims> claimsJws = validateTokenSignature(getAndValidateHeader(context), mount);
                validateTokenClaims(claimsJws);
            }
        } catch (Exception e) {
            try {
                    context.getServletResponse().sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
            } catch (IOException ex) {
            	logger.error("",e);
                throw new JwtException("Error validating JWT", ex);
            }
            logger.error("",e);
            throw new JwtException("Error validating JWT", e);
        } finally {
            context.invokeNext();
        }
    }

    protected Jws<Claims> validateTokenSignature(String authToken, Mount mount) {
        try {
            logger.info("Validating token");

            CustomSigningKeyResolver customSigningKeyResolver = new CustomSigningKeyResolver(mount);
            Jws<Claims> claims = Jwts.parserBuilder()
            		.setSigningKeyResolver(customSigningKeyResolver).build().parseClaimsJws(authToken);
            logger.info("Token valid. Subject: {}", claims.getBody().getSubject());

            return claims;
        } catch (MissingClaimException mce) {
        	logger.error("",mce);
        	throw new JwtException("Error validating JWT - Audience (aud) field missing. ", mce);
        } catch (IncorrectClaimException ice) {
        	logger.error("",ice);
        	throw new JwtException("Error validating JWT - Audience (aud) field not equal to " + AUD_CMS + ". ", ice);
        } catch (Exception e) {
        	logger.error("",e);
        	throw new JwtException("Error validating JWT with public key", e);
        }
    }

    protected void validateTokenClaims(Jws<Claims> claimsJws) {

        checksPart1(claimsJws);
        if (claimsJws.getBody().get("exp") == null) {
            throw new JwtException("exp is null or empty - JWT Token is not valid");
        }
        if (claimsJws.getBody().get("jti") == null || claimsJws.getBody().get("jti").equals("")) {
            throw new JwtException("jti is null or empty - JWT Token is not valid");
        }

        Date currentDate = new Date(System.currentTimeMillis());
        if (currentDate.before(claimsJws.getBody().getNotBefore())) {
            throw new JwtException("not before date exception - JWT Token is not valid");
        }

        if (currentDate.after(claimsJws.getBody().getExpiration())) {
            throw new JwtException("expiration date exception - JWT Token is not valid");
        }
    }

	private void checksPart1(Jws<Claims> claimsJws) {
		if (claimsJws.getBody().get("iss") == null || claimsJws.getBody().get("iss").equals("")) {
            throw new JwtException("iss is null or empty - JWT Token is not valid");
        }
        if (claimsJws.getBody().get("sub") == null || claimsJws.getBody().get("sub").equals("")) {
            throw new JwtException("sub is null or empty - JWT Token is not valid");
        }
        if (claimsJws.getBody().get("nbf") == null) {
            throw new JwtException("nbf is null or empty - JWT Token is not valid");
        }
	}

    protected String getAndValidateHeader(ValveContext context) {
        try {
            String header = context.getServletRequest().getHeader("Authorization");
            if (header == null) {
                throw new JwtException("No JWT token found in request headers");
            }
            logger.info("Header: {}", header);
            return StringUtils.remove(header, BEARER_AUTH_HEADER_PROPERTY);
        } catch (Exception e) {
        	logger.error("",e);
            throw new JwtException("Error validating Authorization header", e);
        }
    }


}