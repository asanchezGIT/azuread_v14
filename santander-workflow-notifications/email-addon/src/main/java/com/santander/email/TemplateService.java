package com.santander.email;

public interface TemplateService {
    String getTemplateByName(String name);

    String getPropertyByName(String name, String propertyName);
}
