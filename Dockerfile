FROM tomcat:9-jdk8-openjdk-slim

LABEL PROJECT=santanderbrxm

# Default JVM heap size variables
ENV JAVA_MINHEAP 256m
ENV JAVA_MAXHEAP 512m

# Default tomcat http max threads variable
ENV TOMCAT_MAXTHREADS 200

# Default repository settings
ENV REPO_PATH /brxm/project/target/storage
ENV REPO_CONFIG ""
ENV REPO_BOOTSTRAP true
ENV REPO_AUTOEXPORT_ALLOWED false
ENV REPO_WORKSPACE_BUNDLE_CACHE 256
ENV REPO_VERSIONING_BUNDLE_CACHE 64

# Default database profile
ENV profile mysql

# Default mysql variables
ENV MYSQL_DB_HOST ${MYSQL_DB_HOST}
ENV MYSQL_DB_PORT ${MYSQL_DB_PORT}
ENV MYSQL_DB_USER ${MYSQL_DB_USER}
ENV MYSQL_DB_PASSWORD ${MYSQL_DB_PASSWORD}
ENV MYSQL_DB_NAME ${MYSQL_DB_NAME}
ENV MYSQL_DB_DRIVER com.mysql.cj.jdbc.Driver

# Default postgres variables
ENV POSTGRES_DB_HOST postgres
ENV POSTGRES_DB_PORT 5432
ENV POSTGRES_DB_USER ${MYSQL_DB_USER}
ENV POSTGRES_DB_PASSWORD ${MYSQL_DB_PASSWORD}
ENV POSTGRES_DB_NAME ${MYSQL_DB_NAME}
ENV POSTGRES_DB_DRIVER org.postgresql.Driver

# Prepare dirs
# Delete default & unused war files
# Define a non-root user with limited permissions
# Non-root user should own tomcat & /brxm dirs
RUN mkdir -p \
        /brxm/bin \
        /usr/local/tomcat/common/classes \
        /usr/local/tomcat/shared/classes \
    && rm -rf \
        /usr/local/tomcat/webapps/docs \
        /usr/local/tomcat/webapps/examples \
        /usr/local/tomcat/webapps/host-manager \
        /usr/local/tomcat/webapps/manager \
        /usr/local/tomcat/webapps/ROOT \
    && addgroup --gid 1001 brxmuser \
    && adduser --gid 1001 --uid 1001 brxmuser \
    && chown -R brxmuser /usr/local/tomcat /brxm

# In maven/ the files as specified in the <assembly> section are stored and need to be added manually
# COPY in reverse order of expected change frequency, for optimal docker build caching
COPY --chown=brxmuser:brxmuser maven/common /usr/local/tomcat/common/
COPY --chown=brxmuser:brxmuser maven/db-drivers /brxm/db-drivers
COPY --chown=brxmuser:brxmuser maven/scripts /brxm/bin
RUN chmod +x /brxm/bin/docker-entrypoint.sh

# Entrypoint script applies env-vars to config, then runs tomcat
ENTRYPOINT ["/brxm/bin/docker-entrypoint.sh"]

COPY --chown=brxmuser:brxmuser maven/conf /usr/local/tomcat/conf/
COPY --chown=brxmuser:brxmuser maven/shared /usr/local/tomcat/shared/
COPY --chown=brxmuser:brxmuser maven/webapps /usr/local/tomcat/webapps/

RUN chmod -R 777 /usr
RUN chmod -R 777 /brxm

USER brxmuser

# Entrypoint script applies env-vars to config, then runs tomcat
ENTRYPOINT ["/brxm/bin/docker-entrypoint.sh"]
