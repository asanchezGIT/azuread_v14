#!/usr/bin/env bash

echo "========================================================================"
echo "                          Building project                              "
echo "========================================================================"

mvn clean install

echo "========================================================================"
echo "                        Project successful built                        "
echo "========================================================================"

echo "========================================================================"
echo "                        Building docker image                           "
echo "========================================================================"

mvn -Pdocker.build,mysql

echo "========================================================================"
echo "                     Docker image successful built                      "
echo "========================================================================"

echo "========================================================================"
echo "                     Extracting docker build                            "
echo "========================================================================"
tar -xvf ./target/docker/com.santander/santanderbrxm/0.1.0-SNAPSHOT/tmp/docker-build.tar -C ./target/docker/com.santander/santanderbrxm/0.1.0-SNAPSHOT/tmp/

echo "========================================================================"
echo "                     Moving build to root directory                     "
echo "========================================================================"

rm -rf assembly
cp -r ./target/docker/com.santander/santanderbrxm/0.1.0-SNAPSHOT/tmp/assembly .
echo "assembly folder DONE"

rm -rf maven
cp -r ./target/docker/com.santander/santanderbrxm/0.1.0-SNAPSHOT/tmp/maven .
echo "maven folder DONE"

rm -rf scripts
cp -r ./target/docker/com.santander/santanderbrxm/0.1.0-SNAPSHOT/tmp/scripts .
echo "scripts folder DONE"

echo "========================================================================"
echo "                               FINISHED                                 "
echo "========================================================================"