package com.santander.utils;

import java.util.HashMap;
import java.util.Map;

public final class Constants {

	private Constants() {
		// Auto-generated constructor stub
	}
	
    public static final String LOCAL_ENV  = "localhost";
    public static final String BRX_HOSTNAME = System.getenv("BRX_HOSTNAME");
    public static final String ASSET_HOSTNAME = System.getenv("ASSET_HOSTNAME");
    public static final String EVENTHUB_APP_SECRET = System.getenv("EVENTHUB_APP_SECRET");
    public static final String EVENTHUB_APP_ID = System.getenv("EVENTHUB_APP_ID");
    public static final String EVENTHUB_TENANT_ID = System.getenv("EVENTHUB_TENANT_ID");

    public static final String PRODUCER_ORG_TOPIC = "PRODUCER_ORG_TOPIC";
    public static final String PRODUCT_TOPIC = "PRODUCT_TOPIC";
    public static final String API_TOPIC = "API_TOPIC";
    public static final String MOCK_TOPIC = "MOCK_TOPIC";
    public static final String MOCK_MESSAGE = "MOCK_MESSAGE";
    public static final String AUTO_OFFSET_RESET = "AUTO_OFFSET_RESET";
    
    
    public static final String MODE = "mode";
    public static final String VIEW = "view";
    public static final String EDIT = "edit";
    public static final String HIPPO_MIRROR = "hippo:mirror";
    public static final String PUBLISHED = "published";
    public static final String HIPPOSTD_STATE = "hippostd:state";
    public static final String PROCESSED = "processed";

    public static final String SANTANDERBRXM_ID_IL = "santanderbrxm:idIL";
    public static final String SANTANDERBRXM_TITLE_IL = "santanderbrxm:titleIL";
    public static final String SANTANDERBRXM_TITLE= "santanderbrxm:title";
    public static final String SANTANDERBRXM_NAME_IL = "santanderbrxm:nameIL";
    public static final String SANTANDERBRXM_NAME = "santanderbrxm:name";
    public static final String SANTANDERBRXM_DESCRIPTION_IL = "santanderbrxm:descriptionIL";
    public static final String SANTANDERBRXM_DESCRIPTION_CMS = "santanderbrxm:descriptionCMS";
    public static final String SANTANDERBRXM_DESCRIPTION_OVERVIEW = "santanderbrxm:descriptionOverview";
    public static final String SANTANDERBRXM_DESCRIPTION = "santanderbrxm:description";
    public static final String SANTANDERBRXM_STATUS = "santanderbrxm:status";
    public static final String SANTANDERBRXM_APICONTAINERITEM = "santanderbrxm:ApiContainerItem";
    public static final String SANTANDERBRXM_VERSION_IL = "santanderbrxm:versionIL";
    public static final String SANTANDERBRXM_PK ="santanderbrxm:PK";
    public static final String SANTANDERBRXM_DEPRECATED="santanderbrxm:deprecated";
    public static final String SANTANDERBRXM_MODE="santanderbrxm:mode";
    public static final String SANTANDERBRXM_LOCALCOUNTRY="santanderbrxm:localCountry";
    public static final String SANTANDERBRXM_LOCALCOUNTRY_TYPE="santanderbrxm:LocalCountry";
    public static final String SANTANDERBRXM_YAML="santanderbrxm:yaml";
    public static final String STATUS_REAL ="Real";
    public static final String SANTANDERBRXM_ICON="santanderbrxm:icon";
    public static final String SANTANDERBRXM_POSTMAN="santanderbrxm:postmanCollectionlink";
    public static final String SANTANDERBRXM_LINKS="santanderbrxm:links";
    public static final String SANTANDERBRXM_ICON_AUTHOR="santanderbrxm:iconAuthor";
    public static final String SANTANDERBRXM_GLOBALEXPOSURE ="santanderbrxm:globalExposure";
    public static final String SANTANDERBRXM_TYPE ="santanderbrxm:type";
    public static final String SANTANDERBRXM_DOCUMENTATION="santanderbrxm:documentation";
    public static final String SANTANDERBRXM_DOCUMENTATION_API="santanderbrxm:document";
    public static final String SANTANDERBRXM_MENU_DOCUMENTATION="santanderbrxm:menuDoc";
    public static final String SANTANDERBRXM_UPDATEDATE ="santanderbrxm:updateDate";

    public static final String HIPPO_DOCBASE = "hippo:docbase";
    public static final String HIPPO_CONTENT ="hippostd:content";
    public static final String HIPPO_VALUES = "hippo:values";
    public static final String HIPPO_MODES ="hippo:modes";
    public static final String HIPPO_FACETS ="hippo:facets";

    //Path where the documents will be stored
    public static final String PRODUCER_ORGANIZATIONS_FOLDER = 
    		"/content/documents/santander/santander-paymentshub/en_gb/content/producercontainers/";
    
    public static final String EN_GB = "en-gb";

    //querys
    public static final String ORDERBY_VERSIONIL_AND_VERSIONCMS ="' ] order by @santanderbrxm:versionIL, @santanderbrxm:versionCMS";


    //Doctypes
    public static final String SANTANDERBRXM_PRODUCER_ORGANIZATION = "santanderbrxm:ProducerOrganizationContainer";
    public static final String SANTANDERBRXM_PRODUCT_CONTAINER = "santanderbrxm:ProductContainerItem";

  //------------------
    //**WORKFLOWS
    //------------------
    public static final String CONTENT_FOLDER = "/content/documents/santander/";
    public static final String TRANSLATOR_PREFIX_GROUP= "translator_";
    
    //Pagonext
    public static final Map<String, String[]> MAPPING = new HashMap<String, String[]>() {{
    	put("author_", new String[]{"translator_", "editor_"});
        put("translator_", new String[]{"author_"});
        put("editor_", new String[]{"author_"});
   									
     }};


}