/**
 * Exception class implementation for Integration Layer
 */
package com.santander.sync;

public class IntegrationLayerException extends Exception{
	
	public IntegrationLayerException(String message) {
		super(message);
	}
}
