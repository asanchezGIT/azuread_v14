package com.santander.sync.kafka;
//Copyright (c) Microsoft Corporation. All rights reserved.
//Licensed under the MIT License.


import com.microsoft.aad.msal4j.ClientCredentialFactory;
import com.microsoft.aad.msal4j.ClientCredentialParameters;
import com.microsoft.aad.msal4j.ConfidentialClientApplication;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.aad.msal4j.IClientCredential;
import com.santander.utils.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.security.auth.AuthenticateCallbackHandler;
import org.apache.kafka.common.security.oauthbearer.OAuthBearerToken;
import org.apache.kafka.common.security.oauthbearer.OAuthBearerTokenCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.AppConfigurationEntry;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeoutException;

public class CustomAuthenticateCallbackHandler implements AuthenticateCallbackHandler {

    private static final String EMPTY_STRING = "";

	private static final String PATTERNBRACKETS = "\\[|\\]";

	private static Logger logger = LoggerFactory.getLogger(CustomAuthenticateCallbackHandler.class);

public CustomAuthenticateCallbackHandler() {
	// Auto-generated constructor stub
}
    
    static final ScheduledExecutorService EXECUTOR_SERVICE = Executors.newScheduledThreadPool(1);

    private String authority;
    private String appId;
    private String appSecret;
    private ConfidentialClientApplication aadClient;
    private ClientCredentialParameters aadParameters;

    @Override
    public void configure(Map<String, ?> configs, String mechanism, List<AppConfigurationEntry> jaasConfigEntries) {
        String bootstrapServer = Arrays.asList(configs.get(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG)).get(0).toString();
        bootstrapServer = RegExUtils.replaceAll(bootstrapServer, PATTERNBRACKETS, EMPTY_STRING);
     
        URI uri = URI.create("https://" + bootstrapServer);
        String sbUri = uri.getScheme() + "://" + uri.getHost();
        this.aadParameters =
                ClientCredentialParameters.builder(Collections.singleton(sbUri + "/.default"))
                        .build();

        this.authority = "https://login.microsoftonline.com/" + Constants.EVENTHUB_TENANT_ID + "/";
        this.appId = Constants.EVENTHUB_APP_ID;
        this.appSecret = Constants.EVENTHUB_APP_SECRET;
    }

    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException 
    {
    	logger.info("Creating custom OAuth Bearer Token");
        for (Callback callback : callbacks) 
        {
            if (callback instanceof OAuthBearerTokenCallback) {
                try {
                    OAuthBearerToken token = getOAuthBearerToken();
                    OAuthBearerTokenCallback oauthCallback = (OAuthBearerTokenCallback) callback;
                    oauthCallback.token(token);
                } catch (InterruptedException| ExecutionException e) {
                    logger.error("Error getting OAuthBearerToken",e);
                }
            } else {
                throw new UnsupportedCallbackException(callback);
            }
        }
    }

    synchronized OAuthBearerToken getOAuthBearerToken() throws MalformedURLException, InterruptedException, 
    ExecutionException {
        if (this.aadClient == null) 
        {
           IClientCredential credential = ClientCredentialFactory.createFromSecret(this.appSecret);
           this.aadClient = ConfidentialClientApplication.builder(this.appId, credential)
                      .authority(this.authority)
                      .build(); 
        }

        IAuthenticationResult authResult = this.aadClient.acquireToken(this.aadParameters).get();
        logger.info("TOKEN ACQUIRED");

        return new OAuthBearerTokenImp(authResult.accessToken(), authResult.expiresOnDate());
    }

    public void close() throws KafkaException {
        // NOOP
    }
}