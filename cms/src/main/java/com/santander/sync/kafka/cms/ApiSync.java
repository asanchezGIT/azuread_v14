package com.santander.sync.kafka.cms;

import java.rmi.RemoteException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.repository.api.HippoWorkspace;
import org.hippoecm.repository.api.WorkflowException;
import org.hippoecm.repository.api.WorkflowManager;
import org.hippoecm.repository.standardworkflow.FolderWorkflow;
import org.onehippo.repository.documentworkflow.DocumentWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.santander.sync.model.ApiPayload;

public class ApiSync {
	
	private static Logger logger = LoggerFactory.getLogger(ApiSync.class);

	
public void createApi(Session session) {
		
        
        String articlesFolderPath = "/content/documents/santander/common/products/apis";

		
        try {
	        WorkflowManager wflManager = ((HippoWorkspace) session.getWorkspace()).getWorkflowManager();
	        final Node folder = session.getNode(articlesFolderPath);
	        final FolderWorkflow folderWorkflow = (FolderWorkflow)wflManager.getWorkflow("internal", folder);
	        folderWorkflow.add("new-document", "santanderbrxm:ApiProduct", "miNombre");
	        final Node document = folder.getNode("miNombre");
	        document.setProperty("santanderbrxm:title", "title1");
	        final DocumentWorkflow documentWorkflow = (DocumentWorkflow) wflManager.getWorkflow("default", document);
	        documentWorkflow.commitEditableInstance();
	        documentWorkflow.publish();
        }catch(RepositoryException | RemoteException | WorkflowException e) {
        	
        	logger.error("{}",e.getMessage());
	
        }   
        
		logger.debug("end of createApi");


	}
	
	
}
