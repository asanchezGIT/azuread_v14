/**
 * Payload class for Products bounds to  API containers
 * 
 */
package com.santander.sync.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IdFrameworkPayload {
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	protected String id;

}
