/**
 * Payload class for Product Containers
 * 
 */
package com.santander.sync.model;

import java.util.List;

public class ProductContainerPayload {
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getContainerProducerOrg() {
		return containerProducerOrg;
	}
	public void setContainerProducerOrg(String containerProducerOrg) {
		this.containerProducerOrg = containerProducerOrg;
	}
	/*
	public Integer[] getApis() {
		return apis;
	}*/
	/*
	public void setApis(Integer[] apis) {
		this.apis = apis;
	}
	*/
	protected String id;
    protected String name;
    protected String version;
    protected String containerProducerOrg;
    protected String deprecated;
    public String getDeprecated() {
		return deprecated;
	}
	public void setDeprecated(String deprecated) {
		this.deprecated = deprecated;
	}
	//protected Integer[] apis;
    protected List<String> apisContainers;
	public List<String> getApisContainers() {
		return apisContainers;
	}
	public void setApisContainers(List<String> apisContainers) {
		this.apisContainers = apisContainers;
	}

}