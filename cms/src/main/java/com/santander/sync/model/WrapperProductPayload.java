/*
 * Wrapper class for kafka product payload
 * author: VASS
 * copyright: N/A
 * 
 */
package com.santander.sync.model;

public class WrapperProductPayload {
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public ProductContainerPayload getPayload() {
		return payload;
	}
	public void setPayload(ProductContainerPayload payload) {
		this.payload = payload;
	}
	protected String id;
    protected String timestamp;
    protected ProductContainerPayload payload;
}
