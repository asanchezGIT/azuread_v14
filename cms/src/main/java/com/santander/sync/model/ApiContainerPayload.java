package com.santander.sync.model;

import java.util.List;

public class ApiContainerPayload {

	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDeprecated() {
		return deprecated;
	}
	public void setDeprecated(String deprecated) {
		this.deprecated = deprecated;
	}
	public String getContainerProducerOrg() {
		return containerProducerOrg;
	}
	public void setContainerProducerOrg(String containerProducerOrg) {
		this.containerProducerOrg = containerProducerOrg;
	}
	public List<ApiPayload> getApis() {
		return apis;
	}
	public void setApis(List<ApiPayload> apis) {
		this.apis = apis;
	}
	public String getIdBusinessArea() {
		return idBusinessArea;
	}
	public void setIdBusinessArea(String idBusinessArea) {
		this.idBusinessArea = idBusinessArea;
	}
	public String getIdBusinessDomain() {
		return idBusinessDomain;
	}
	public void setIdBusinessDomain(String idBusinessDomain) {
		this.idBusinessDomain = idBusinessDomain;
	}
	public String getIdServiceDomain() {
		return idServiceDomain;
	}
	public void setIdServiceDomain(String idServiceDomain) {
		this.idServiceDomain = idServiceDomain;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	protected String version;
	protected String id;
	protected String name;
	protected String title;
	protected String type;
	protected String deprecated;
	protected String containerProducerOrg;
	protected List<ApiPayload> apis;
	protected String idBusinessArea;
	protected String idBusinessDomain;
	protected String idServiceDomain;
}