/**
 * POJO to store the configuration for the integration layer cronjob
 */

package com.santander.sync.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IntegrationLayerConfigurationModel
{


	protected String bootstrapServers;
	protected String groupId;
	protected String requestTimeoutMS;
	protected String securityProtocol;
	protected String saslMechanism;
	protected String saslJaasConfig;
	protected String saslLoginCallbackHandlerClass;
	protected String localTesting;
	protected String topic;
	protected String productTopic;
	protected String producerOrgTopic;
	protected String apiTopic;
	protected String mockTopic;
	protected String mockMessage;
	protected String autoOffsetReset;
	protected String versionBlacklist;
	protected String environment;
}
