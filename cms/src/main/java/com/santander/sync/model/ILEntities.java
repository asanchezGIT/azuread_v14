package com.santander.sync.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ILEntities {

	protected String id;
	protected String name;
}
