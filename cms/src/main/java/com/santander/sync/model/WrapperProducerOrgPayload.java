/**
 * Wrapper class for Producer organizations from kafka
 */
package com.santander.sync.model;

import com.santander.sync.model.ProducerOrgPayload;

public class WrapperProducerOrgPayload {
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public ProducerOrgPayload getPayload() {
		return payload;
	}
	public void setPayload(ProducerOrgPayload payload) {
		this.payload = payload;
	}
	protected String id;
    protected String timestamp;
    protected ProducerOrgPayload payload;

    

}
