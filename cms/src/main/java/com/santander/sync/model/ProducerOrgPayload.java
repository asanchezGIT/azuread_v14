/**
 * Payload class for Producer organizations
 */
package com.santander.sync.model;


public class ProducerOrgPayload {

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isDeprecated() {
		return deprecated;
	}
	public void setDeprecated(boolean deprecated) {
		this.deprecated = deprecated;
	}
	protected String id;
    protected String name;
    protected boolean deprecated;
}
