/**
 * Payload class for products
 */
package com.santander.sync.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;


public class ProductPayload {
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getContainerProducerOrg() {
		return containerProducerOrg;
	}
	public void setContainerProducerOrg(String containerProducerOrg) {
		this.containerProducerOrg = containerProducerOrg;
	}
	public List<String> getApis() {
		return apis;
	}
	public void setApis(List<String> apis) {
		this.apis = apis;
	}
	protected String id;
    protected String name;
    protected String version;
    protected String containerProducerOrg;
    protected List<String> apis;


}
