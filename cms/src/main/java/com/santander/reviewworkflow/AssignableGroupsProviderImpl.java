package com.santander.reviewworkflow;

import org.bloomreach.forge.reviewworkflow.cms.reviewedactions.AssignableGroupsProvider;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.repository.impl.SessionDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.regex.Pattern;

import com.santander.utils.Constants;

public class AssignableGroupsProviderImpl implements AssignableGroupsProvider {

	 private static final int INDEX = 2;
	public static final String CONTENT_FOLDER = Constants.CONTENT_FOLDER;
	 public static final String TRANSLATOR= Constants.TRANSLATOR_PREFIX_GROUP;
	 public static final Map<String, String[]> mapping= Constants.MAPPING;
	  

    private static final Logger log = LoggerFactory.getLogger(AssignableGroupsProviderImpl.class);

 
    @Override
    public Set<String> provideGroups(String currentUserId, String docAbsolutePath) {
        try {
        	log.info("******INICIO AssignableGroupsProviderImpl.java*************");
           	Set<String> assignableGroups = new HashSet<>();
            HstRequestContext requestContext = RequestContextProvider.get();
            Session session = requestContext.getSession();

            for (String myGroup : ((SessionDecorator) session).getUser().getMemberships()) {
                log.debug("******My group is: {}" , myGroup);
                for (String key : mapping.keySet()) 
                {
                    processMyGroups(assignableGroups, session, myGroup, key);
                }
            }
            return assignableGroups;

        } catch (RepositoryException e) {
            log.error("Error getting groups: ", e);
        }
        return Collections.emptySet();
    }


	private void processMyGroups(Set<String> assignableGroups, Session session, String myGroup, String key)
			throws RepositoryException, PathNotFoundException, ValueFormatException {
		if (myGroup.contains(key)) {
		    String groupToAppend;
		    if (myGroup.indexOf('_') != -1) {
		        groupToAppend = myGroup.substring(myGroup.indexOf('_') + 1);
		    } else {
		        groupToAppend = myGroup;
		        
		    }
		    String[] groups = (mapping.get(key));
		                            
		    for (String group : groups) 
		    {
		        extractAssignableGroups(assignableGroups, session, groupToAppend, group);
		    }
		}
	}


	private void extractAssignableGroups(Set<String> assignableGroups, Session session, String groupToAppend,
			String group) throws RepositoryException, PathNotFoundException, ValueFormatException {
		
		
		//En Pagonxt ya no existen los traductores de todos los locales. Sólo existe el trnaslator_superdigital y no es translator-es_Superdigital y el translator-en_Superdigital.
		//Por esta razón se trata como un autor_ y un editor_
		/*
		if (group.equals(TRANSLATOR)) {
		    NodeIterator nodeIterator = session.getNode(CONTENT_FOLDER).getNodes();
		    while (nodeIterator.hasNext()) {
		        processNode(assignableGroups, groupToAppend, group, nodeIterator);
		    }
		} else {*/
			
			if (groupToAppend.equals("everybody") || groupToAppend.equals("editor")||groupToAppend
					.equals("webmaster")) { 
				//author,editor y admin. Default users Bloomreach
				if (!groupToAppend.equals(group)){
					assignableGroups.add(group);
					log.debug("******añadido a la lista de asignables: {}",group);
				}
			}
			else { 
				// para cuando tienes los grupos de seguridad
				//de un site establecidos "author_", "editor_", etc.
				assignableGroups.add(group + groupToAppend);
				log.info("******añadido a la lista de asignables: {} {}",group,  groupToAppend);
			}
		    
		//}
	}


	private void processNode(Set<String> assignableGroups, String groupToAppend, String group,
			NodeIterator nodeIterator) throws RepositoryException, ValueFormatException, PathNotFoundException {
		Node node = nodeIterator.nextNode();
		log.error(node.getPath());
		NodeIterator languageNodeIterator = node.getNodes();
		while (languageNodeIterator.hasNext()) {
		    Node languageNode = languageNodeIterator.nextNode();
		    log.error(languageNode.getPath());
		    if (languageNode.hasProperty("hippotranslation:locale")) {
		        String locale = languageNode
		        		.getProperty("hippotranslation:locale").getString();
		        assignableGroups.add(group + locale.substring(0, INDEX) + "_" + groupToAppend);
		    }
		}
	}
}